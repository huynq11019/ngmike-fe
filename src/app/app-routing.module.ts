import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicLayoutComponent } from './layout/app-layout/public-layout/public-layout.component';
import { MainLayoutComponent } from './layout/app-layout/main-layout/main-layout.component';
import { Page404Component } from './authentication/page404/page404.component';
import { FullLayoutComponent } from './layout/app-layout/full-layout/full-layout.component';
import { GuardGuard } from './core/guard/guard.guard';

const routes: Routes = [
  // {path: '', pathMatch: 'full', redirectTo: '/page/page404'},
  // {path: 'welcome', loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule)},
  { // đây là public layout
    path: '',
    component: PublicLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./entity/public/public.module').then(m => m.PublicModule)
      }
    ]
  }, {
    path: '',
    component: FullLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./authentication/authentication.module').then((m) => m.AuthenticationModule)

      }
    ]
  },
  {// đây là main layout

    path: 'admin',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./entity/private/private.module').then((m) => m.PrivateModule)
      }],
    canActivate: [GuardGuard],
    data: {
      authorities: ['EMPLOYEE']
    }
  },
  {
    path: '**',
    component: Page404Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
