import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CusomterCreateComponent } from './cusomter-create.component';

describe('CusomterCreateComponent', () => {
  let component: CusomterCreateComponent;
  let fixture: ComponentFixture<CusomterCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CusomterCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CusomterCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
