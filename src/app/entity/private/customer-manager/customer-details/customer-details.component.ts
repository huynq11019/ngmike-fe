import { IOrder } from './../../../../shared/models/order-customer';
import { MyOrderService } from 'src/app/shared/service/my-order.service';
import { CUSTOMER_GENDER } from './../../../../shared/contants/customerGender';
import { catchError } from 'rxjs/operators';
import { ICustomer } from './../../../../shared/models/customer';
import { CustomerService } from 'src/app/shared/service/customer.service';
import { Component, OnInit } from '@angular/core';
import { NzTabsCanDeactivateFn } from 'ng-zorro-antd/tabs';
import { ActivatedRoute } from '@angular/router';
import { CUSTOMER } from 'src/app/shared/contants/customerStatus';
import { TranslateService } from '@ngx-translate/core';
import { IMyAddress } from 'src/app/shared/models/my-address';
import { MyAddressService } from 'src/app/shared/service/my-address.service';
import { WishlistService } from 'src/app/shared/service/wishlist.service';
import { FileUploadService } from 'src/app/shared/service/file-upload.service';
import { MyCartService } from 'src/app/shared/service/my-cart.service';
import { ToastrService } from 'ngx-toastr';
import { DEFAULT_QUERY, ENTITY_TYPE } from 'src/app/shared/contants/Constants';
import { Wishlist } from 'src/app/shared/models/wishlist';
import { IAddToCart } from 'src/app/shared/models/addToCart';
import { cartItem } from 'src/app/entity/public/mycart/mycart.component';
import { ICart } from 'src/app/shared/models/my-cart';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss'],
})
export class CustomerDetailsComponent implements OnInit {
  tabs = ['Tổng quan ', 'Lịch sử mua hàng'];
  dataCustomerDetail?: ICustomer;
  listOfMyAddress?: IMyAddress[] = [];
  listOfOrder?: IOrder[];
  private customerId?: string;
  totalMoney: number = 0;

  public PRODUCT_TYPE = ENTITY_TYPE.PRODUCT;
  public totalItem = 0;
  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
  };
  listOfProfile?: Wishlist[] = [];
  carts: readonly cartItem[] = [];
  stars: number[] = [1, 2, 3, 4, 5];
  selectedValue: number;
  addToCart?: IAddToCart;
  intoMoneyData?: number;
  listOfCurrentPageData: readonly cartItem[] = [];
  constructor(
    private customerDetailService: CustomerService,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private addressService: MyAddressService,
    private myOrderService: MyOrderService,
    private wishlistService: WishlistService,
    public fileUpload: FileUploadService,
    private myCartService: MyCartService,
    private toastr: ToastrService
  ) {}
  ngOnInit() {
    this.loadInfo();
    this.onLoadHistoryOrder(this.customerId);
    this.onLoadWishlist(this.customerId);
    this.onLoadListCart();
  }
  canDeactivate: NzTabsCanDeactivateFn = (fromIndex: number, toIndex: number) => {
    switch (fromIndex) {
      case 0:
        return toIndex === 1;
      case 1:
        return true;
      case 2:
        return true;
      default:
        return true;
    }
  };
  loadInfo() {
    this.route.params.subscribe((param) => {
      this.customerId = param['customerId'];
      if (!!this.customerId) {
        this.findCustomerById();
        this.getAddressByCustomerId(this.customerId);
      }
    });
  }
  private getAddressByCustomerId(customerId: any) {
    this.addressService.findByCustomerId(customerId).subscribe((data: any) => {
      this.listOfMyAddress = data?.body.addressDTOS;
      // console.log('info address: ', data?.body.addressDTOS);
    });
  }
  private findCustomerById(): void {
    // console.log('Id param: ' + this.customerId);

    this.customerDetailService.getInforCustomer(this.customerId).subscribe((res: any) => {
      this.dataCustomerDetail = res?.customerDTO;
      // console.log('====================================');
      // console.log('Welcome to detail');
      // console.log(this.dataCustomerDetail);
      // console.log('====================================');
      // console.log(res.customerDTO);
      // console.log('====================================');

      // console.log('====================================');
    });
  }
  onLoadHistoryOrder(customerId: string) {
    this.myOrderService.getListOrderByUserId(customerId).subscribe((data) => {
      this.listOfOrder = data.body?.orderList;
      // console.log('order: ', data);
    });
  }
  countTotalMoney(orderId: string): number {
    this.totalMoney = 0;
    const order = this.listOfOrder.find((id) => id.id == orderId);
    if (this.listOfOrder) {
      order.orderDetails.forEach((data) => {
        this.totalMoney += data.quantity * data.productPrice;
      });
    }
    return this.totalMoney;
  }

  onLoadWishlist(userId: string) {
    this.wishlistService.getFavoriteByUserId(userId).subscribe((data) => {
      this.listOfProfile = data?.body.favouritesDTOS as Array<Wishlist>;
      this.totalItem = data.body?.totalItem;
      // console.log('fav: ', data);
    });
  }
  onLoadListCart() {
    this.myCartService.getListCartByUserId(this.customerId).subscribe((data) => {
      this.carts = data.body?.cartDTOs as Array<ICart>;
      // console.log('Cart: ', data);
    });
  }
  // Into money when the quantity is updated
  total(cart: ICart): number {
    return (this.intoMoneyData = cart.quantity * cart.productPrice);
  }
  pageChange(type: string, event: number): void {
    // console.log(type, event);
    if (type === 'page') {
      this.querySearch[type] = event - 1;
    } else {
      this.querySearch[type] = event;
    }
    this.onLoadWishlist(this.customerId);
  }
  countStar(star: number) {
    this.selectedValue = star;
    // console.log('Value of star', star);
  }
  // Status
  onStatusCustomer(active: number): string {
    if (active === CUSTOMER.BLOCKED) {
      return this.translateService.instant('customer.detail.blocked');
    } else if (active == CUSTOMER.VERIFY) {
      return this.translateService.instant('customer.detail.verify');
    }
    return this.translateService.instant('customer.detail.active');
  }
  // Gender
  onGenderCustomer(active: number): string {
    if (active === CUSTOMER_GENDER.MALE) {
      return this.translateService.instant('customer.detail.male');
    }
    return this.translateService.instant('customer.detail.female');
  }
}
