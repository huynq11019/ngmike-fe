import { CUSTOMER_GENDER } from './../../../shared/contants/customerGender';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

import { CUSTOMER, dateToMs } from './../../../shared/contants/customerStatus';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DEFAULT_QUERY } from 'src/app/shared/contants/Constants';
import { CustomerService } from 'src/app/shared/service/customer.service';
import { ICustomer } from '../../../shared/models/customer';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';

export interface Data {
  id: string;
  name: string;
  age: number;
  address: string;
  disabled: boolean;
}

@Component({
  selector: 'app-customer-manager',
  templateUrl: './customer-manager.component.html',
  styleUrls: ['./customer-manager.component.scss'],
})
export class CustomerManagerComponent implements OnInit, AfterViewInit {
  checked = false;
  loading = false;
  indeterminate = false;
  listOfData: ICustomer[] = [];
  listOfCurrentPageData: readonly Data[] = [];
  setOfCheckedId = new Set<string>();
  confirmModal?: NzModalRef;
  //filter date picker
  from?: any;
  toDate?: any;
  searchValue = '';
  sizePage = 0;

  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
  };
  public querySearchFilterDate = {
    ...DEFAULT_QUERY,
    status: '',
    startDate: 0,
    endDate: 0,
  };
  listOfSelection = [
    {
      text: 'Select All Row',
      onSelect: () => {
        this.onAllChecked(true);
      },
    },
    {
      text: 'Select Odd Row',
      onSelect: () => {
        this.listOfCurrentPageData.forEach((data, index) => this.updateCheckedSet(data.id, index % 2 !== 0));
        this.refreshCheckedStatus();
      },
    },
    {
      text: 'Select Even Row',
      onSelect: () => {
        this.listOfCurrentPageData.forEach((data, index) => this.updateCheckedSet(data.id, index % 2 === 0));
        this.refreshCheckedStatus();
      },
    },
  ];

  updateCheckedSet(id: string, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: readonly Data[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onItemChecked(id: string, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(checked: boolean): void {
    this.listOfCurrentPageData
      .filter(({ disabled }) => !disabled)
      .forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  sendRequest(): void {
    this.loading = true;
    const requestData = this.listOfData.filter((data) => this.setOfCheckedId.has(data.id));
    // console.log(requestData);
    setTimeout(() => {
      this.setOfCheckedId.clear();
      this.refreshCheckedStatus();
      this.loading = false;
    }, 1000);
  }
  constructor(
    private customerService: CustomerService,
    private modal: NzModalService,
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.loadData(this.querySearch);
    // console.log('this is search value: ' + this.searchValue);
  }
  ngAfterViewInit() {
     this.customerService.searchCustomer(this.querySearch).subscribe((data) => {
       this.sizePage = data.body.totalItem;
     });
  }
  loadData(param: any) {
    this.customerService.searchCustomer(param).subscribe((data) => {
      this.listOfData = data.body.customerDTOS;
      // console.log('Data customer: ', data.body.customerDTOS);
    });
  }
  onNavCreate(): void {}

  onChangeDatePicker(event: any): void {
    if (dateToMs(this.from) != null && dateToMs(this.toDate) != null) {
      this.querySearchFilterDate.startDate = dateToMs(this.from);
      this.querySearchFilterDate.endDate = dateToMs(this.toDate);

      this.loadData(this.querySearchFilterDate);
    } else {
      this.loadData(this.querySearch);
    }
  }

  // Status
  onStatusCustomer(active: number): string {
    if (active === CUSTOMER.BLOCKED) {
      return this.translateService.instant('customer.detail.blocked');
    } else if (active == CUSTOMER.VERIFY) {
      return this.translateService.instant('customer.detail.verify');
    }
    return this.translateService.instant('customer.detail.active');
  }

  // Gender
  onGenderCustomer(active: number): string {
    if (active === CUSTOMER_GENDER.MALE) {
      return this.translateService.instant('customer.detail.male');
    }
    return this.translateService.instant('customer.detail.female');
  }

  // Check customer status
  onCheckCustomerStatus(idCustomer: string, status: number) {
    if (status === CUSTOMER.BLOCKED) {
      status = CUSTOMER.ACTIVE;
    } else if (status == CUSTOMER.ACTIVE) {
      status = CUSTOMER.BLOCKED;
    }
    // console.log('Id: ', idCustomer, ' Status: ', status);
    this.showConfirm(idCustomer, status);
  }
  // Block or Unblock customer
  showConfirm(idCustomer: string, status: number): void {
    this.confirmModal = this.modal.confirm({
      nzTitle:
        this.translateService.instant('customer.manage.messageConfirmBlock') +
        (status == CUSTOMER.BLOCKED
          ? this.translateService.instant('customer.detail.blocked')
          : this.translateService.instant('customer.detail.unblock')) +
        ' ?',
      nzOnOk: () =>
        this.customerService.lockUser(idCustomer, status).subscribe((data) => {
          this.toastrService.success(this.translateService.instant('customer.manage.messageSuccess'));
          this.loadData(this.querySearch);
        }),
    });
  }
  onFilter(keyword: string) {
    this.querySearch.keyword = keyword;
    this.loadData(this.querySearch);
  }
  // Filter by Status
  onOptionsSelected(value: any) {
    // console.log('the selected value is .' + value);
    this.querySearch.status = value;
    this.loadData(this.querySearch);
  }
  // Fiter by date
  onFilterByDate() {
    if(this.from){
      // console.log(this.from);
      // console.log(this.toDate);
      dateToMs(this.from);
    }
  }
  //Pagination
  onPaginate(size: number, page: number) {
    this.querySearch.page = page;
    this.querySearch.size = size;
    this.loadData(this.querySearch);
  }
  pageChange(event: any): void {
    // console.log(event);
    this.querySearch.page = event;
    this.loadData(this.querySearch);
  }
  pageSizeChange(event: any): void {
    // console.log(event);
    this.querySearch.size = event;
    this.loadData(this.querySearch);
  }
}
