import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-customer',
  templateUrl: './order-customer.component.html',
  styleUrls: ['./order-customer.component.scss'],
})
export class OrderCustomerComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
  index = 0;
  disable = false;
  onIndexChange(index: number): void {
    this.index = index;
  }
}
