import { RefundProductsComponent } from './refund-products/refund-products.component';
import { AuthorityComponent } from './authority/authority.component';
import { OrderDetailManagementComponent } from './order-management/order-detail-management/order-detail-management.component';
import { CustomerDetailsComponent } from './customer-manager/customer-details/customer-details.component';
import { OrderCustomerComponent } from './../private/order-customer/order-customer.component';

import { EmployeeDetailComponent } from './employee-manager/employee-detail/employee-detail.component';
import { OrderManagementComponent } from './order-management/order-management.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivateRoutingModule } from './private-routing.module';
import { ProductionComponent } from './production/production.component';
import { CreateProductComponent } from './production/create-product/create-product.component';
import { CategoryComponent } from './category/category.component';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { TranslateModule } from '@ngx-translate/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzBackTopModule } from 'ng-zorro-antd/back-top';
import { EmployeeManagerComponent } from './employee-manager/employee-manager.component';
import { CustomerManagerComponent } from './customer-manager/customer-manager.component';
// import { EmployeeCreateComponent } from './employee-manager/employee-create/employee-create.component';
// import { CusomterCreateComponent } from './customer-manager/cusomter-create/cusomter-create.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NzTableModule } from 'ng-zorro-antd/table';
import { MatDialogModule } from '@angular/material/dialog';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { EmployeeCreateComponent } from './employee-manager/employee-create/employee-create.component';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { AvatarModule } from 'ngx-avatar';
import { ShareModuleModule } from '../../shared/share-module.module';
import {NzTreeSelectModule} from "ng-zorro-antd/tree-select";
import {NzDividerModule} from "ng-zorro-antd/divider";
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { RecaptchaModule } from 'ng-recaptcha';
import { NgxEchartsModule } from 'ngx-echarts';
import { NzSwitchModule } from 'ng-zorro-antd/switch';

 @NgModule({
  declarations: [
    ProductionComponent,
    CreateProductComponent,
    CategoryComponent,
    EmployeeManagerComponent,
    CustomerManagerComponent,
    DashboardComponent,
    OrderManagementComponent,
    EmployeeCreateComponent,
    EmployeeDetailComponent,
    OrderCustomerComponent,
    CustomerDetailsComponent,
    OrderDetailManagementComponent,
    AuthorityComponent,
    RefundProductsComponent
  ],
   imports: [
     CommonModule,
     PrivateRoutingModule,
     NzTabsModule,
     TranslateModule,
     NzIconModule,
     NzButtonModule,
     NzPaginationModule,
     NzTypographyModule,
     NzToolTipModule,
     NzFormModule,
     NzSelectModule,
     NzInputModule,
     NzImageModule,
     NzUploadModule,
     NzCheckboxModule,
     NzBreadCrumbModule,
     NzBackTopModule,
     MatButtonModule,
     MatIconModule,
     NzTableModule,
     NzStepsModule,
     MatDialogModule,
     NzTreeModule,
     NzTimelineModule,
     FormsModule,
     MatCardModule,
     MatSlideToggleModule,
     NzDropDownModule,
     NzModalModule,
     ReactiveFormsModule,
     NzRadioModule,
     NzDatePickerModule,
     AvatarModule,
     ShareModuleModule,
     NzTreeSelectModule,
     NzDividerModule,
     NzDividerModule,
     NzSliderModule,
     RecaptchaModule,
     NgxEchartsModule.forRoot({
       echarts: () => import('echarts'), // or import('./path-to-my-custom-echarts')
     }),
     NzSwitchModule,
   ]
 })
export class PrivateModule {}
