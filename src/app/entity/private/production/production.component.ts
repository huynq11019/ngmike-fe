import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DEFAULT_QUERY, ENTITY_TYPE, PRODUCT_STATUS } from '../../../shared/contants/Constants';
import { ProductService } from '../../../shared/service/product.service';
import { Iproduct } from '../../../shared/models/product';
import { NzTabsCanDeactivateFn } from 'ng-zorro-antd/tabs';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmRequestComponent } from '../../../shared/component/confirm-request/confirm-request.component';
import { TranslateService } from '@ngx-translate/core';
import { FileUploadService } from '../../../shared/service/file-upload.service';
import { IOptionProduct } from '../../../shared/models/OptionProduct';
import CommonUtil from '../../../shared/untils/common-utils';
import { AttributeService } from '../../../shared/service/attribute.service';

@Component({
  selector: 'app-production',
  templateUrl: './production.component.html',
  styleUrls: ['./production.component.scss']
})
export class ProductionComponent implements OnInit {
  public TYPE_PRODUCT = ENTITY_TYPE.PRODUCT;
  constructor(private route: Router,
              private productService: ProductService,
              private toastr: ToastrService,
              private ngModalService: NgbModal,
              private translateService: TranslateService, public fileService: FileUploadService,
  ) {
  }

  tabs = ["Tất cả ", "Sản phẩm đang bán", "Sản phẩm đã xóa"];

  public totalItem = 0;
  public listProduct?: Iproduct[];
  public listStatus = PRODUCT_STATUS;
  public querySearch = {
    ...DEFAULT_QUERY,
    status: ''
  };
  ngOnInit(): void {
    this.loadAll();
  }

  pageChange(type: string, event: number): void {
    // console.log(type, event);
    if (type === 'page') {
      this.querySearch[type] = event - 1;
    } else {
      this.querySearch[type] = event;
    }
    this.loadAll();
  }

  // chuyển qua tạo sản phẩm
  onNavCreate(): void {
    this.route.navigate(['/admin/product/create'], {
      queryParams: {
        idtem: 1
      }
    });
  }

  public loadAll() {
    this.productService.loadProduct(this.querySearch).subscribe(respon => {
      this.listProduct = respon.body?.listProductDTO;
      this.totalItem = respon.body?.totalItem;
      // console.table(this.listProduct);
    }, error => {
      console.error(error);
    });
  }

  updateQuery(typeQuery: string, value: any): void {
    // @ts-ignore
    this.querySearch[typeQuery] = value;
    // console.log(typeQuery, value);
  }

  canDeactivate: NzTabsCanDeactivateFn = (fromIndex: number, toIndex: number) => {
    // console.log(fromIndex, toIndex);
    switch (toIndex) {
      case 0:
        this.querySearch.status = '';
        this.loadAll();
        return true;
      case 1:
        this.querySearch.status = '1';
        this.loadAll();
        return true;
      case 2:
        // console.log('toIndex', toIndex);
        this.querySearch.status = '-1';
        this.loadAll();
        return true;
      default:
        return true;
    }
  };

  productStatus(status: number) {
    const key = Object.keys(this.listStatus);
    const res = key?.find(value => {
      // console.log(this.listStatus[value]);
      return this.listStatus[value]?.value === status;
    });
    if (!!this.listStatus[res]) {
      return this.listStatus[res];
    }
    return "-";
  }

  /**
   * @author: huynq
   * @since: 10/31/2021 4:05 PM
   * @description:  Thực hiện xóa sản phẩm theo id
   * @update:
   */
  confirmDelete(item: Iproduct): void {
    const modalDeep = this.ngModalService.open(ConfirmRequestComponent, {
      ariaLabelledBy: 'modal-basic-title',
      size: "md",
      centered: false,
      backdrop: true,
      animation: true,
      keyboard: false,
      windowClass: "true",
      // modalClass: 'mymodal',
      //   hideCloseButton: false,
      backdropClass: "modal-backdrop"
    });
    modalDeep.componentInstance.title = 'Xóa sản phẩm';
    modalDeep.componentInstance.confirmButton = 'Đồng ý'
    modalDeep.componentInstance.body = "Sản phẩm: " + item.productName;
    modalDeep.result.then((result) => {
      // console.log(result);
      if (result === 'confirm') {
        this.productService.changeStatusProduct(item.id, PRODUCT_STATUS.remove.value).subscribe(
          res => {
            console.log(res);
            this.toastr.success("Đẫ xóa sản phẩm");
            this.loadAll();
          }
        );
      }
    });
  }

  getLimitText(text: string, limit = 15): string {
    return CommonUtil.getLimitLength(text, limit);
  }
}
