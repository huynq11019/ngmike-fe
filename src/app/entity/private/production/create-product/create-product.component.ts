import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AngularFireStorage } from '@angular/fire/storage';
import { NzUploadFile, NzUploadXHRArgs } from 'ng-zorro-antd/upload';
import { getBase64 } from '../../../../shared/untils/FunctionUntils';
import { ProductService } from '../../../../shared/service/product.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { ConfirmRequestComponent } from '../../../../shared/component/confirm-request/confirm-request.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Iproduct } from '../../../../shared/models/product';
import { FileUpload } from '../../../../shared/models/file-upload';
import { FileUploadServiceService } from '../../../../shared/service/file-upload-service.service';
import { EventManagerService } from '../../../../shared/service/data-service/event-manager.service';
import { CategoryService } from '../../../../shared/service/category.service';
import { ICategory } from '../../../../shared/models/category';
import { SerriProductService } from '../../../../shared/service/serri-product.service';
import { IOptionProduct, OptionProduct } from '../../../../shared/models/OptionProduct';
import { IserriProduct } from '../../../../shared/models/SerriProduct';
import { IproductAttibute } from '../../../../shared/models/productAttribute';
import { AttributeService } from '../../../../shared/service/attribute.service';
import { Iattibute } from '../../../../shared/models/attribute';
import CommonUtil from '../../../../shared/untils/common-utils';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss'],
})
export class CreateProductComponent implements OnInit {
  fileList = []; // file của ant design
  productFile?: File[] = []; // file khi chọn upload
  fileDefault?: File[] = []; // file mặc định
  fileCache = []; // file cache
  public isUpdate = window.location.href.includes('product/update');
  // default product
  public optionProduct: IOptionProduct[] = [];
  tagValue: [];
  //cage gory
  expandKeys = ['100', '1001'];
  value?: string;
  category = [];
  serrials = [];
  productAttriBute: Iattibute[] = []; // list attribute
  selectAttribute: IproductAttibute[] = []; // attribute đã chọn
  // private Api: ApiService,
  previewImage: string | undefined = '';
  previewVisible = false;

  title = 'cloudsSorage';
  downloadURL?: Observable<string>;
  productForm: any;
  productInfor?: Iproduct;
  createContinue = false;
  fb: any;
  content?: string;
  public isDisableForm: false;
  private productId?: string;

  constructor(
    private storage: AngularFireStorage,
    private productService: ProductService,
    private translateService: TranslateService,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    protected eventManagerService: EventManagerService,
    private router: Router,
    private fileUploadService: FileUploadServiceService,
    private categoryService: CategoryService,
    private serrialSerrial: SerriProductService,
    private attributeService: AttributeService
  ) {

  }

  ngOnInit(): void {

    this.initForm();
    this.loadTreeCategory();
    this.loadSerrial();
    // kh
    if (this.optionProduct.length === 0) {
      this.optionProduct.push(new OptionProduct());
    }

    this.route.params.subscribe((param) => {
      this.productId = param['productId'];
      if (!this.productId) {
        this.isUpdate = false;
        return;
      }
      this.loadProductAttribute(this.productId);
      this.productService.getProductById(this.productId).subscribe((res) => {
        this.productInfor = res.body?.productDTO;
        if (!this.productInfor?.description) {
          this.productInfor.description = '<p></p>';
        }
        if (!!this.productInfor?.metaData) {
          this.productInfor.metaData = JSON.parse(this.productInfor?.metaData);
        }
        if (!!this.productInfor?.categoryId) {
          // this.loadAttributeByCategory(this.productInfor.categoryId);
        }
        this.optionProduct = this.productInfor?.optionProducts;
        this.fileDefault = res.body?.productDTO?.files;
        this.productInfor?.files?.forEach(fileDTO => {
          const file: NzUploadFile = {
            uid: fileDTO.id,
            name: fileDTO.originalName,
            ownerId: fileDTO?.ownerId,
            status: 'done',
            url: 'data:' + fileDTO.fileType + ';base64,' + fileDTO.file,
            file: fileDTO.file, // file byte
          };
          this.fileCache.push(file);
        });
        this.fileList = this.fileCache;
        this.initForm();
      });
    });
  }

  private getParamId() {
    this.route.params.subscribe((param) => {
      this.productId = param['productId'];
      if (!!this.productId) {
        this.findProductById();
      } else {
        this.initForm();
      }
    });
  }

  private initForm(): void {
    this.productForm = this.formBuilder.group({
      productName: [
        {
          value: this.productInfor?.productName || '',
          disabled: this.isDisableForm,
        },
        [Validators.required],
      ],
      subCateName: [
        {value: this.productInfor?.subCateName || '', disabled: this.isDisableForm},
        [Validators.required],
      ],
      description: [
        {
          value: this.productInfor?.description || '',
          disabled: this.isDisableForm,
        },
        [Validators.required],
      ],
      price: [this.productInfor?.price || 0, [Validators.min(0), Validators.required]],
      discount: [this.productInfor?.discount || 0, [Validators.min(0), Validators.max(100)]],
      isOrder: [this.productInfor?.isOrder || false],
      quantity: [this.productInfor?.quantity || 0, [Validators.min(0)]],
      metaData: [{value: this.productInfor?.metaData || this.tagValue, disabled: this.isDisableForm}],
      status: [this.productInfor?.status || 1],
      estimateTime: [this.productInfor?.estimateTime || 0],
      optionProducts: [this.productInfor?.optionProducts || []],
      serial: [this.productInfor?.serial || 0],
      categoryId: [this.productInfor?.categoryId || ''],
    });
  }

  private findProductById(): void {
    this.productService.getProductById(this.productId).subscribe((res) => {
      this.productInfor = res.body?.productDTO;
      this.productInfor.metaData = JSON.parse(this.productInfor.metaData);
      this.productInfor?.files.forEach((fileDTO) => {
        const file: NzUploadFile = {
          uid: fileDTO.id,
          name: fileDTO.originalName,
          ownerId: fileDTO?.ownerId,
          status: 'done',
          url: 'data:' + fileDTO.fileType + ';base64,' + fileDTO.file,
          file: fileDTO.file, // file byte
        };
        this.fileList.push(file);
      });
      this.initForm();
    });
  }

  onFileSelected(event: any): void {
    const n = Date.now();
    const file = event.target.files[0];
    // productFileUpload.fileSize = file.g
    const filePath = `RoomsImages/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`RoomsImages/${n}`, file);
    task
    .snapshotChanges()
    .pipe(
      finalize(() => {
        this.downloadURL = fileRef.getDownloadURL();
        this.downloadURL.subscribe((url) => {
          if (url) {
            this.fb = url;
          }
        });
      })
    )
    .subscribe((url: any) => {
      if (url) {
        console.log(url);
      }
    });
  }

  handlePreview = async (file: NzUploadFile) => {
    if (!file.url && !file.preview) {
      // tslint:disable-next-line:no-non-null-assertion
      file.preview = await getBase64(file?.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  };

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = this.fileList.concat(file);
    return true;
  };
  handleCustomUploadReq = (item: NzUploadXHRArgs) => {
    const productFileUpload = new FileUpload();

    console.log(item.file);
    console.log(this.productFile);
    const n = Date.now();
    const file = item.file;
    productFileUpload.originalName = file?.name;
    productFileUpload.fileSize = file?.size;
    productFileUpload.fileType = file?.type;
    console.log(item);
    return this.fileUploadService.fakeUploadFile(false).subscribe(
      (res) => {
        item.onSuccess(null, item.file, null);
      },
      (error) => item.onError(error, file)
    );
  };

  async handleUploadImg($event) {
    if ($event.type === 'success') {
      const file = $event?.file;
      if (!file?.url) {
        file.url = await getBase64(file.originFileObj);
        this.productFile.push(file);
        // this.fileList.push(file);
      }
    } else if ($event.type === 'removed') {
      this.fileList = this.fileList.filter((item) => item.uid !== $event?.file.uid);
    }
  }

  handleDownload = (file: NzUploadFile) => {
    console.table(this.fileList);
    console.log(file);
    const fileD = this.fileList.find((item) => item.uid === file.uid);
    if (!!fileD) {
      const a = document.createElement('a');
      if (!fileD.url) {
        this.toastr.error('Đường đãn file chưa tồn tại');
        return;
      }
      a.href = fileD.url;
      a.download = fileD.name;
      a.click();
    }
  };

  handleRemoveImg = (file: NzUploadFile): boolean => {
    console.log(file);
    this.fileUploadService.deleteImage(file.uid).subscribe(
      (res) => {
        this.toastr.success('Xóa thành công');
        // this.fileList = this.fileList.filter((item) => item.uid !== file.uid);
      },
      (error) => {
        this.toastr.error('Xóa thất bại');
      }
    );
    return true;
  };

  /**
   * @author: huynq
   * @since: 10/31/2021 10:57 AM
   * @description:  nhân viên thực hiện lưu nháp bản ghi
   * @update:
   */
  onUpdate(): void {
    console.log("optionProduct :", this.optionProduct);

    this.productForm.markAllAsTouched();
    if (!this.validateOptionProduct()) {
      this.toastr.error("option của sản phẩm không hợp lệ");
      return;
    }

    const product: Iproduct = {
      ...this.productForm.value,
      metaData: JSON.stringify(this.productForm.get('metaData').value),
      optionProducts: this.optionProduct,
      files: this.productFile,
      lmao: ''
    };
    console.log(product,this.productFile);
    // this.toastr.success("đã lưu nháp");
    this.productService.updateProduct(this.productId, product, this.productFile).subscribe((res) => {
      console.log(res);
      this.onSaveAttribute(this.productId);
      // this.toastr.success("đã lưu nháp");
      // this.router.navigate(['/admin/product']);
      this.toastr.success('Đã lưu nháp');
    });
  }

  /**
   * @author: huynq
   * @since: 10/31/2021 10:58 AM
   * @description:  nhân viên thực hiện submit bản ghi
   * @update:
   */
  onSubmitForm(): void {
    this.productForm.markAllAsTouched();
    const product: Iproduct = {
      ...this.productForm.value,
      metaData: JSON.stringify(this.productForm.get('metaData').value),
      files: this.productFile,
      optionProducts: this.optionProduct
    };
    console.log(product);
    // check option validate
    if (!this.validateOptionProduct()) {
      this.toastr.error("option của sản phẩm không hợp lệ");
      return;
    }
    if (this.productForm.invalid) {
      this.toastr.error('form điền vào không hợp lệ');
      return;
      }
      console.log(product, this.productFile);
      this.productService.createProduct(product, this.fileList).subscribe((res) => {
        console.log(res);
        if (res.status === 200) {
          console.log(res.body?.productDTO?.id);
          // lưu option sản phẩm
          this.onSaveAttribute(res.body?.productDTO?.id);
          this.toastr.success('Đã lưu sản phẩm');
        }
      });
    }


  // category
  /**
   * @author: huynq
   * @since: 11/25/2021 12:01 AM
   * @description:  laod data cho select category
   * @update:
   */
  loadTreeCategory(keyword?: string) {
    this.categoryService.loadCategoryTree(keyword).subscribe((res) => {
      this.category = this.inputDataToTree(res.body?.categoryDTOS);
    });
  }

  public inputDataToTree(list: ICategory[]): any[] {
    const categoryList = [];
    list?.forEach(category => {
      // @ts-ignore
      const obj = {
        title: category.categoryName,
        key: category.id,
        children: category.child == null ? null : this.inputDataToTree(category.child)
      };

      categoryList.push(obj);

    });

    return categoryList;
  }

  // ============== serrial ===============
  loadSerrial(keyword?: string) {
    this.serrialSerrial.search(keyword).subscribe((res) => {
      this.serrials = res.body?.serriProductDTOList;
      console.log('serrial', res.body);
    });
  }

  addItem(input: HTMLInputElement): void {
    if (!input.value) {
      this.toastr.error('Vui lòng nhập mã serrial');
      return ;
    }

    const value = input.value;

    const serrial: IserriProduct = {
      serriName: value,
      serriDescroption: ''
    };
    this.serrialSerrial.createUpdate(serrial).subscribe((res) => {
      this.loadSerrial();
    });
    input.value = '';
  }

  //=========== option product  =============

  // add new option product
  addOptionProduct() {
    console.log('add option product');
    this.optionProduct = [...this.optionProduct, new OptionProduct()];
    console.log(this.optionProduct);
  }

  public deleteOptionProduct(index: number) {
    this.optionProduct.splice(index, 1);
  }

  // validate option product
  validateOptionProduct() {
    const optionProduct = this.optionProduct;
    let isValid = true;
    if (optionProduct.length == 0){
      return false;
    }
    optionProduct.forEach(option => {
      if (option.name === '' || !option?.price || !option?.stockQuantity) {
        isValid = false;
      }
    });
    return isValid;
  }

  public onChangeOptionProduct(target: any, type: string, index: number): void {
    this.optionProduct[index][type] = target?.value;
    console.log(index, target.value, type);
  }

  // ============== ck ===============
  onChangeData(event) {
    this.productForm.get('description').setValue(event);
    this.content = event;
  }

  loadAttributeByCategory(categoryId: string): void {
    console.log('load attribute by category', categoryId);
    this.attributeService.loadAttrubuteByCategory({}, categoryId).subscribe((res) => {
      if (this.productAttriBute?.length > 0) {
        console.log(" đã có data attribute");
        return ;
      }
      this.productAttriBute = res.body?.attributeList as Array<Iattibute>;
      this.productAttriBute.forEach(attribute => {
        // attribute.value = [];
        const value: IproductAttibute = {
          attributeId: attribute.id,
          attributeName: attribute.name,
          value: ''
        };
        this.selectAttribute.push(value);
      });
    });
  }

  // attribute
  onChangeDataAttribute(event, index: number) {
    // console.log(event.value, index);
    this.selectAttribute[index].value = event.value;
    console.log(this.selectAttribute);
  }

  // check ok
  onSaveAttribute(productId: string) {
    console.log('save attribute', this.selectAttribute);
    console.log(this.selectAttribute);
    this.selectAttribute.forEach(at => {
      at.productId = productId;
      at.attributeName = this.productAttriBute.find(pa => pa.id == at.attributeId)?.name || 'unknow';
    });
    this.attributeService.saveAttributeByProduct(productId, this.selectAttribute).subscribe((res) => {
      console.log(res);
      this.router.navigate(['/admin/product']);
    });
  }

  // load product attribute
  loadProductAttribute(productId: string) {
    this.attributeService.loadAttributeByProduct(productId).subscribe((res) => {
      const productAttribute: IproductAttibute[] = res.body?.productAttributeList as Array<IproductAttibute>;
      this.selectAttribute = productAttribute;
      const showAttribute = productAttribute.map(pa => {
        const attributes: Iattibute = {
          id: pa.attributeId,
          name: pa.attributeName,
          value: pa.value
        };
        return attributes;
      });
      this.productAttriBute = showAttribute;
      console.log('product attribute', this.productAttriBute);
      console.log('select attribute', this.selectAttribute);
    });
  }
}
