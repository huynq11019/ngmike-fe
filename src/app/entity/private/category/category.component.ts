import { Component, OnInit } from '@angular/core';

import { NzFormatEmitEvent } from 'ng-zorro-antd/tree';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { CategoryService } from '../../../shared/service/category.service';
import { DEFAULT_QUERY } from '../../../shared/contants/Constants';
import { ICategory } from '../../../shared/models/category';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AttributeService } from '../../../shared/service/attribute.service';
import { Iattibute } from '../../../shared/models/attribute';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  loading = false;
  categorySelect?: ICategory = null;
  checkAttributeSelect?: boolean = false;
  attributeSelectId?: string = null;
  formGroup?: FormGroup;
  formAttribute?: FormGroup;
  searchValue = '';
  constructor(private route: Router,
              private categoryService: CategoryService,
              private toastrService: ToastrService,
              private ngModalService: NgbModal,
              private translateService: TranslateService,
              private attributeService: AttributeService,
              private fb: FormBuilder) {
    this.initForm();
    this.initAttributeFrom();
  }

  public totalItem = 0;
  public listCategory?: ICategory[];
  public listAttribute: Iattibute[];

  public querySearch = {
    ...DEFAULT_QUERY,
    status: ''
  };
  public nodes = [  ];
  private currentCateId: string;

  ngOnInit(): void {
    this.loadTree();
    this.attributeService.loadAttrubuteByCategory(null, '1').subscribe(
      (response) => {
        console.log(response.body?.attributeList);
      }
    )
  }

  pageChange(type: string, event: number): void {
    if (type === 'page') {
      this.querySearch[type] = event - 1;
    } else {
      this.querySearch[type] = event;
    }
    this.loadAll();
  }

  onNavCreate(): void {
    this.route.navigate(['/admin/category/create']);
  }

  nzEvent(event: NzFormatEmitEvent): void {
    // console.log(event.node.origin);
    // console.log(event.node.origin);
//
    if (event.node?.origin?.selected) {
      this.categorySelect = this.getCategoryFromTree(event.node.origin.key, this.listCategory);
      this.loadAttrubute(this.categorySelect.id);
      this.formGroup.get('categoryName').setValue(this.categorySelect.categoryName);
      this.formGroup.get('status').setValue(this.categorySelect.status)
    }else{
      this.categorySelect = null;
    }
  }

  public loadAll() {
    this.categoryService.loadAllCategory(this.querySearch).subscribe(response => {
      this.listCategory = response.body?.listCategoryDto;
      this.totalItem = response.body?.totalItem;
    }, error => {
      // console.error(error);
    });
  }

  public loadTree() {
    this.querySearch.page = null;
    this.querySearch.size = null;
    this.categoryService.loadCategoryTree(this.querySearch).subscribe(response => {
      this.listCategory = response.body?.categoryDTOS;
      this.totalItem = response.body?.totalItem;
      // console.log(response.body?.categoryDTOS);
      // console.log(this.inputDataToTree(this.listCategory))
      this.nodes=this.inputDataToTree(this.listCategory)
    }, error => {
      console.error(error);
    });
  }

  updateQuery(typeQuery: string, value: any): void {
    // @ts-ignore
    this.querySearch[typeQuery] = value;
  }

  public inputDataToTree(list: ICategory[]): any[] {
    const categoryList = [];
    list?.forEach(category => {
      // @ts-ignore
      const obj = {
        title: category.categoryName,
        key: category.id,
        children: category.child == null ? null : this.inputDataToTree(category.child)
      };
      categoryList.push(obj);
    });

    return categoryList;
  }

  selectAttribute(attribute:Iattibute){
    if (this.attributeSelectId = attribute.id) {
      this.checkAttributeSelect = this.checkAttributeSelect ? false : true;
    }
    this.attributeSelectId = attribute.id;
    if (this.checkAttributeSelect) {
      this.formAttribute.get('name').setValue(attribute.name);
      this.formAttribute.get('type').setValue(attribute.type);
    }else{
      this.formAttribute.get('name').setValue(null);
      this.formAttribute.get('type').setValue(null);
      this.attributeSelectId = null;
    }
  }

  initForm(): void {
    this.formGroup = this.fb.group({
      categoryName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      // categoryDescription: new FormControl(''),
      // categoryParent: new FormControl(''),
      status: new FormControl(''),
      // categoryImage: new FormControl(''),
    });
  }

  initAttributeFrom(): void {
    this.formAttribute = this.fb.group({
      name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      type: new FormControl('', [Validators.required, Validators.maxLength(30)]),
    })
  }

  getCategoryFromTree(key: string, tree: ICategory[]):ICategory{
    for (const item of tree) {
      if (item.id === key) {
        return item;
      }
      let child = item.child === null ? null : this.getCategoryFromTree(key, item.child);
      if (child !== null) {return child}
      // if (item.child != null) {
      //   return this.getCategoryFromTree(key, item.child);
      // }
    }
    return null;
  }

  createCategory(){
    let category: ICategory = this.formGroup.value;
    console.log("tạo mới category", category);
    if (this.categorySelect != null) {
      category.parentCategory = this.categorySelect.id;
    }
    this.categoryService.createCategory(category).subscribe(rp => {
      setTimeout(() => {
        this.nodes = [];
        this.loadTree();
      }, 500);
      this.toastrService.success('Thêm mới danh mục thành công');
    });

  }

  updateCategory() {
    let category: ICategory = this.formGroup.value;
    if (this.categorySelect != null) {
      category.id = this.categorySelect.id;
      // console.log(category);
      // console.log(this.categorySelect);
      this.categoryService.updateProduct(category).subscribe(rp => {
        this.loadTree();

        this.toastrService.success('Cập nhập thành công');
      });
    }else{
      this.toastrService.error('Bạn phải chọn danh mục cần cập nhập');
    }
  }

  public createAttribute(){
    if (this.categorySelect != null) {
      let attributet: Iattibute = this.formAttribute.value;
      attributet.categoryId = this.categorySelect.id.toString();
      this.attributeService.createAttributeByCategory(attributet).subscribe(rp => {
        this.toastrService.success('Thêm thành công');
        this.loadAttrubute(this.categorySelect.id);
        this.formAttribute.reset();
      });
    }else{
      this.toastrService.error('Bạn phải chọn danh mục');
    }
  }

  public updateAttribute(){
    if (this.categorySelect != null) {
      let attributet: Iattibute = this.formAttribute.value;
      if (this.attributeSelectId != null) {
        attributet.categoryId = this.categorySelect.id.toString();
        // console.log(attributet);
        this.attributeService.updateAttrubuteByCategory(attributet, this.attributeSelectId).subscribe(rp => {
          this.toastrService.success('Cập nhập thành công');
          this.loadAttrubute(this.categorySelect.id);
        });
      }else{
        this.toastrService.error("Bạn chưa chọn thuộc tính");
      }
    }else{
      this.toastrService.error('Bạn phải chọn danh mục');
    }
  }

  public loadAttrubute(categoryId:string){
    this.attributeService.loadAttrubuteByCategory(null, categoryId).subscribe(
      (response) => {
        this.listAttribute = response.body?.attributeList;
        // console.log(response.body?.attributeList);
      }
    )
  }

}
