import { IOrder } from 'src/app/shared/models/order-customer';
import { Component, OnInit } from '@angular/core';
import { MyOrderService } from 'src/app/shared/service/my-order.service';
import { DEFAULT_QUERY, ORDER_STATUS_STRING } from 'src/app/shared/contants/Constants';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  CREATE = ORDER_STATUS_STRING.CREATE;
  WAITING_FOR_PAYMENT = ORDER_STATUS_STRING.WAITING_FOR_PAYMENT;
  WAITING_FOR_CONFIRM = ORDER_STATUS_STRING.WAITING_FOR_CONFIRM;
  CONFIRMED = ORDER_STATUS_STRING.CONFIRMED;
  SHIPPING = ORDER_STATUS_STRING.SHIPPING;
  PAID = ORDER_STATUS_STRING.PAID;
  CANCELED = ORDER_STATUS_STRING.CANCELED;
  totalItem?: number;
  constructor(private myOrderService: MyOrderService) {}
  option = {
    title: {
      // text: 'Thống kê khách hàng',
      // subtext: '纯属虚构'
    },
    tooltip: {
      trigger: 'axis',
    },
    legend: {
      data: ['Sản phẩm được thêm vào giỏ', 'Lượt truy câp'],
    },
    toolbox: {
      show: true,
      feature: {
        dataZoom: {
          yAxisIndex: 'none',
        },
        dataView: { readOnly: false },
        magicType: { type: ['line', 'bar'] },
        restore: {},
        saveAsImage: {},
      },
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: ['Thứ 2', 'Thứ 3', 'thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'Chủ nhật'],
    },
    yAxis: {
      type: 'value',
      axisLabel: {
        formatter: '{value} °',
      },
    },
    series: [
      {
        name: 'Sản phẩm được thêm vào giỏ',
        type: 'line',
        data: [11, 11, 15, 13, 12, 13, 10],
        markPoint: {
          data: [
            { type: 'max', name: '最大值' },
            { type: 'min', name: '最小值' },
          ],
        },
        markLine: {
          data: [{ type: 'average', name: 'trung bình' }],
        },
      },
      {
        name: 'Lượt truy câp',
        type: 'line',
        data: [1, -2, 2, 5, 3, 2, 0],
        markPoint: {
          data: [{ name: '周最低', value: -2, xAxis: 1, yAxis: -1.5 }],
        },
        markLine: {
          data: [
            { type: 'average', name: 'Tổng đơn hàng trung bình' },
            [
              {
                symbol: 'none',
                x: '90%',
                yAxis: 'max',
              },
              {
                symbol: 'circle',
                label: {
                  position: 'start',
                  formatter: 'Số đơn hàng \n cao nhất',
                },
                type: 'max',
                name: 'Số đơn hàng cao nhất',
              },
            ],
          ],
        },
      },
    ],
  };

  barchart = {
    xAxis: {
      type: 'category',
      data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
    },
    yAxis: {
      type: 'value',
    },
    series: [
      {
        data: [
          120,
          {
            value: 200,
            itemStyle: {
              color: '#a90000',
            },
          },
          150,
          80,
          70,
          110,
          130,
        ],
        type: 'bar',
      },
    ],
  };

  listOfColumn = [
    {
      title: 'Số Id đơn hàng',
      compare: null,
      priority: false,
    },
    {
      title: 'Khách hàng',
      compare: null,
      priority: 3,
    },
    {
      title: 'Trạng thái',
      compare: null,
      priority: 2,
    },
    {
      title: 'Ngày được đặt',
      compare: null,
      priority: 1,
    },
  ];
  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
    size: 5,
    // startDate: 0,
    // endDate: 0,
  };
  listOfData: IOrder[];
  onLoadOrder() {
    this.myOrderService.getAllOrder(this.querySearch).subscribe((data) => {
      this.listOfData = data.body.orderList;
      this.totalItem = data.body.totalItem;
      console.log(data);
    });
  }
  statusString(status: string): string {
    if (status === this.CREATE) {
      return "Chờ xử lí";
    } else if (status === this.WAITING_FOR_PAYMENT) {
      return "Chờ thanh toán";
    } else if (status === this.WAITING_FOR_CONFIRM) {
      return "Chờ xác nhận";
    } else if (status === this.CONFIRMED) {
      return "Đã xác nhận";
    } else if (status === this.SHIPPING) {
      return "Đang giao hàng";
    } else if (status === this.PAID) {
      return "Đã thanh toán";
    } else if (status === this.CANCELED) {
      return "Đã hủy";
    }
    return "-";
  }
  pageChange(type: string, event: number): void {
    // console.log(type, event);
    if (type === 'page') {
      this.querySearch[type] = event - 1;
    } else {
      this.querySearch[type] = event;
    }
    this.onLoadOrder();
  }
  ngOnInit(): void {
    this.onLoadOrder();
  }
}
