import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { CUSTOMER } from 'src/app/shared/contants/customerStatus';
import { EmployeeService } from 'src/app/shared/service/employee.service';

@Component({
  selector: 'app-authority',
  templateUrl: './authority.component.html',
  styleUrls: ['./authority.component.scss'],
})
export class AuthorityComponent implements OnInit {
  constructor(
    private route: Router,
    private employeeService: EmployeeService,
    private modal: NzModalService,
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) {}
  searchValue = '';
  confirmModal?: NzModalRef;
  isVisible = false;
  switchValue = false;
  onNavCreate(): void {}
  onFilter(keyword: string) {}

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }
  // Block or Unblock Employee
  showConfirm(idEmployee: string, status: number): void {
    this.confirmModal = this.modal.confirm({
      nzTitle:
        this.translateService.instant('customer.manage.messageConfirmBlock') +
        (status == CUSTOMER.BLOCKED
          ? this.translateService.instant('customer.detail.blocked')
          : this.translateService.instant('customer.detail.unblock')) +
        ' ?',
      nzOnOk: () =>
        this.employeeService.lockUser(idEmployee).subscribe((data) => {
          this.toastrService.success(this.translateService.instant('customer.manage.messageSuccess'));
        }),
    });
  }
  ngOnInit() {}
}
