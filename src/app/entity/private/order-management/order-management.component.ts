import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IOrder } from 'src/app/shared/models/order-customer';
import { MyOrderService } from 'src/app/shared/service/my-order.service';
import { DEFAULT_QUERY, ORDER_STATUS } from 'src/app/shared/contants/Constants';
import { ExportcerService } from '../../../shared/service/exportcer.service';

@Component({
  selector: 'app-order-management',
  templateUrl: './order-management.component.html',
  styleUrls: ['./order-management.component.scss'],
})
export class OrderManagementComponent implements OnInit {
  CREATE = ORDER_STATUS.CREATE.label;
  WAITING_FOR_PAYMENT = ORDER_STATUS.WAITING_FOR_PAYMENT.label;
  WAITING_FOR_CONFIRM = ORDER_STATUS.WAITING_FOR_CONFIRM.label;
  CONFIRMED = ORDER_STATUS.CONFIRMED.label;
  SHIPPING = ORDER_STATUS.SHIPPING.label;
  PAID = ORDER_STATUS.PAID.label;
  CANCELED = ORDER_STATUS.CANCELED.label;
  SUCCESS = ORDER_STATUS.SUCCESS.label;

  public listProduct?: Object[];

  constructor(private myOrderService: MyOrderService,
              private route: Router,
              private exportExcel: ExportcerService) {
  }

  inforUserOrder?: IOrder[];
  listOfOrderDetails: IOrder[];
  orderId: string = '';
  intoMoneyData?: number = 0;
  deliveryCharges?: number = 0;
  numberProducts?: number = 0;
  searchValue = '';
  sizePage = 10;
  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
  };

  // Load into money, total discount
  totalCart() {
    this.listOfOrderDetails?.forEach((item) => {
      let total = item.totalQuantity * item.totalPrice;
      this.intoMoneyData += total;
    });
  }
  onLoadCustomerOrder(param?: any) {
    this.myOrderService.getAllOrder(param).subscribe((data) => {
      this.sizePage = data.body?.totalItem;
      this.listOfOrderDetails = data.body?.orderList as IOrder[];
      console.table(data);
    });
  }

  ngOnInit(): void {
    this.onLoadCustomerOrder(this.querySearch);
  }
  onFilter(keyword: string) {
    this.querySearch.keyword = keyword;
    this.onLoadCustomerOrder(this.querySearch);
  }
  // chuyển qua tạo sản phẩm
  onNavCreate(): void {
    this.route.navigate(['/admin/product/create'], {
      queryParams: {
        idtem: 1,
      },
    });
  }
  //Pagination
  onPaginate(size: number, page: number) {
    this.querySearch.page = page;
    this.querySearch.size = size;
    this.onLoadCustomerOrder(this.querySearch);
  }
  pageChange(event: any): void {
    // console.log(event);
    this.querySearch.page = event;
    this.onLoadCustomerOrder(this.querySearch);
  }
  pageSizeChange(event: any): void {
    // console.log(event);
    this.querySearch.size = event;
    this.onLoadCustomerOrder(this.querySearch);
  }
  limitText(text: string, limit: number): string {
    if (text.length > limit) {
      return text.substr(0, limit);
    }
    return text;
  }
  statusString(status: string): string {
    if (status === this.CREATE) {
      return 'Chờ xử lí';
    } else if (status === this.WAITING_FOR_PAYMENT) {
      return 'Chờ thanh toán';
    } else if (status === this.WAITING_FOR_CONFIRM) {
      return 'Chờ xác nhận';
    } else if (status === this.CONFIRMED) {
      return 'Đã xác nhận';
    } else if (status === this.SHIPPING) {
      return 'Đang giao hàng';
    } else if (status === this.PAID) {
      return 'Đã thanh toán';
    } else if (status === this.CANCELED) {
      return 'Đã hủy';
    } else if (status === this.SUCCESS) {
      return 'Đã thanh toán thành công';
    }
    return '-';
  }

  public exportOrder(): void {
    this.exportExcel.exportExcel(this.listOfOrderDetails, 'Danh sách đơn hàng');
  }
}
