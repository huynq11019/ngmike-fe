import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { IOrderDetail } from './../../../../shared/models/order-detail';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MyOrderService } from 'src/app/shared/service/my-order.service';
import { IOrder } from 'src/app/shared/models/order-customer';
import { ORDER_STATUS} from 'src/app/shared/contants/Constants';

@Component({
  selector: 'app-order-detail-management',
  templateUrl: './order-detail-management.component.html',
  styleUrls: ['./order-detail-management.component.scss'],
})
export class OrderDetailManagementComponent implements OnInit {
  WAITING_FOR_PAYMENT = ORDER_STATUS.WAITING_FOR_PAYMENT.label;
  WAIT_FOR_CONFIRM = ORDER_STATUS.WAITING_FOR_CONFIRM.label;
  CONFIRMED = ORDER_STATUS.CONFIRMED.label;
  SHIPPING = ORDER_STATUS.SHIPPING.label;
  PAID = ORDER_STATUS.PAID.label;
  CANCELED = ORDER_STATUS.CANCELED.label;
  SUCCESS = ORDER_STATUS.SUCCESS.label;

  orderDetailForm: any;
  infoOrder: IOrderDetail;
  isDisableForm: boolean = true;

  constructor(
    private myOrderService: MyOrderService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private translate: TranslateService
  ) {
    this.route.params.subscribe((params) => {
      this.orderId = params['orderId'];
    });
  }

  inforUserOrder?: IOrder;
  orderDetail: IOrder;
  orderId: string = '';
  intoMoneyData?: number = 0;
  deliveryCharges?: number = 0;
  numberProducts?: number = 0;
  switchValue = false;
  orderForm?: any;
  orderStatusShow?: string;

  statusString(status: string): string {
    if (status === this.WAITING_FOR_PAYMENT) {
      return 'Chờ thanh toán';
    } else if (status === this.WAIT_FOR_CONFIRM) {
      return 'Chờ xác nhận';
    } else if (status === this.CONFIRMED) {
      return 'Đã xác nhận';
    } else if (status === this.SHIPPING) {
      return 'Đang giao hàng';
    } else if (status === this.PAID) {
      return 'Đã thanh toán';
    } else if (status === this.CANCELED) {
      return 'Đã hủy';
    } else if (status === this.SUCCESS) {
      return 'Thanh toán thành công';
    }
    return '-';
  }
  onChangeStatus() {
    console.log(this.switchValue);
  }

  // Load into money, total discount
  totalCart() {
    this.inforUserOrder?.orderDetails?.forEach((item) => {
      let total = item.quantity * item.productPrice;
      this.intoMoneyData += total;
    });
  }

  onLoadCustomerOrder(): void {
    this.myOrderService.getOrderDetailByOrderId(this.orderId).subscribe((data) => {
      this.inforUserOrder = data.body?.order;
      this.totalCart();
      console.log(' order detail management: ', this.inforUserOrder);
    });
  }

  onLoadOrder(userId: string) {
    this.myOrderService.getOrderDetailByUserId(userId).subscribe((data) => {
      this.orderDetail = data.body?.order;
      this.orderStatusShow = this.orderDetail.orderStatus;
      console.log('alo ', data.body.order);
    });
  }
  onChangeOrderStatus(orderStatus: string) {
    console.log(orderStatus);
    this.myOrderService.changeOrderStatus(this.orderId, orderStatus).subscribe((data) => {
      this.onLoadOrder(this.orderId);
      this.toastr.success(this.translate.instant('order.successStatus'));
    });
  }
  ngOnInit() {
    this.onLoadCustomerOrder();
    this.onLoadOrder(this.orderId);
  }
}
