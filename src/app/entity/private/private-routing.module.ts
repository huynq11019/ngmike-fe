import { RefundProductsComponent } from './refund-products/refund-products.component';
import { AuthorityComponent } from './authority/authority.component';
import { OrderDetailManagementComponent } from './order-management/order-detail-management/order-detail-management.component';
import { EmployeeDetailComponent } from './employee-manager/employee-detail/employee-detail.component';
import { CustomerDetailsComponent } from './customer-manager/customer-details/customer-details.component';
import { OrderManagementComponent } from './order-management/order-management.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductionComponent} from './production/production.component';
import {CreateProductComponent} from './production/create-product/create-product.component';
import {EmployeeManagerComponent} from './employee-manager/employee-manager.component';
import {EmployeeCreateComponent} from './employee-manager/employee-create/employee-create.component';
import {CustomerManagerComponent} from './customer-manager/customer-manager.component';
import {CusomterCreateComponent} from './customer-manager/cusomter-create/cusomter-create.component';
import { CategoryComponent } from './category/category.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OrderCustomerComponent } from './order-customer/order-customer.component';

const routes: Routes = [
  {
    path: 'product/create',
    component: CreateProductComponent,
  },
  {
    path: 'product',
    component: ProductionComponent,
  },
  {
    path: 'product/update/:productId',
    component: CreateProductComponent,
  },
  {
    path: 'employee',
    component: EmployeeManagerComponent,
  },
  {
    path: 'employee/detail/:employeeId',
    component: EmployeeDetailComponent,
  },
  {
    path: 'employee/create',
    component: EmployeeDetailComponent,
  },
  {
    path: 'customer',
    component: CustomerManagerComponent,
  },
  {
    path: 'customer/create',
    component: CusomterCreateComponent,
  },
  {
    path: 'customer/detail/:customerId',
    component: CustomerDetailsComponent,
  },
  {
    path: 'order',
    component: OrderManagementComponent,
  },
  {
    path: 'order-customer',
    component: OrderManagementComponent,
  },
  {
    path: 'order/detail/:orderId',
    component: OrderDetailManagementComponent,
  },
  {
    path: 'category',
    component: CategoryComponent,
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'authority',
    component: AuthorityComponent,
  },
  {
    path: 'refund-product',
    component: RefundProductsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateRoutingModule {
}
