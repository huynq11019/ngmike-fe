import { ResetPasswordService } from './../../../../shared/service/reset-password.service';
import { IEmployee } from './../../../../shared/models/employee';
import { EmployeeService } from './../../../../shared/service/employee.service';
import { Component, OnInit } from '@angular/core';
import { INFO_CUSTOM, REGEX_CUSTOM } from 'src/app/shared/contants/Constants';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { ICustomer } from 'src/app/shared/models/customer';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss'],
})
export class EmployeeDetailComponent implements OnInit {
  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private router: Router,
    private resetPasswordService: ResetPasswordService
  ) {
    console.log('load employee => ');
    this.route.params.subscribe((param) => {
      this.employeeId = param['employeeId'];
      // console.log(this.employeeId);
    });
  }

  employeeId: string;
  employeeForm?: any;
  infoEmployee?: IEmployee;
  employeeFile?: File[] = [];
  isVisible = false;
  validateForm: any;
  inforCustomer?: ICustomer;
  isDisableForm = false;
  genderValue?: number =1;
  isDetail = window.location.href.includes('detail');
  ngOnInit() {
      
    if (this.employeeId) {
      this.isDisableForm = true;
      this.onLoad();
    }
    this.initForm();
  }
  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.onChangePassword();
    this.isVisible = false;
  }

  handleCancel(): void {
    // console.log('Button cancel clicked!');
    this.isVisible = false;
  }
  resolved(captchaResponse: string) {
    // console.log(`Resolved captcha with response: =>> ${captchaResponse}`);
    this.validateForm.get('captcha').setValue(captchaResponse);
  }
  // random password generator
  generatePassword() {
    var length = 8,
      charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      retVal = '';
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }
  onLoad() {
    this.employeeService.getInforEmployee(this.employeeId).subscribe(
      (response: any) => {
        this.infoEmployee = response?.employeeDTO;
        this.genderValue = response?.employeeDTO.gender;
        this.initForm();
        // this.employeeForm.controls.password.clearValidators();
        // this.employeeForm.controls.password.updateValueAndValidity();
        // this.employeeForm.pathValue({ gender: this.infoEmployee.gender });
        // console.log('this is employee info: ', response?.employeeDTO);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  private initForm(): void {
    // console.log(this.infoEmployee?.gender, ' init');
    
    this.employeeForm = this.formBuilder.group({
      fullName: [this.infoEmployee?.fullName || '', [Validators.required, Validators.minLength(5)]],
      // userName: [
      //   {
      //     value: this.infoEmployee?.userName || '',
      //     disabled: this.isDisableForm,
      //   },
      //   [Validators.minLength(5), Validators.pattern(REGEX_CUSTOM.USER_NAME)],
      // ],
      password: [this.infoEmployee?.password || '', [Validators.required, Validators.pattern(REGEX_CUSTOM.PASSWORD)]],
      address: [this.infoEmployee?.address || '', [Validators.required, Validators.maxLength(100)]],
      email: [
        {
          value: this.infoEmployee?.email || '',
          disabled: this.isDisableForm,
        },
        [Validators.required, Validators.pattern(REGEX_CUSTOM.EMAIL)],
      ],
      phoneNumber: [
        this.infoEmployee?.phoneNumber || '',
        [Validators.required, Validators.pattern(REGEX_CUSTOM.PHONE_NUMBER)],
      ],
      dob: [moment(this.infoEmployee?.dob).format('yyyy-MM-DD') || '', [Validators.required]],
      gender: [this.infoEmployee?.gender],
      status: [this.infoEmployee?.status || INFO_CUSTOM.STATUS_DEFAULT],
    });
    //Form change password
    this.validateForm = this.formBuilder.group({
      password: [this.inforCustomer?.password || '', [Validators.required]],
      newPassword: [this.inforCustomer?.newPassword || '', [Validators.required]],
      repeatNewPassword: [this.inforCustomer?.repeatNewPassword || '', [Validators.required]],
      capcha: [''],
    });
  }
  onChangePassword() {
    this.validateForm.markAllAsTouched();
    const changePass = { ...this.validateForm.value };
    // console.log('onSubmit: ', changePass.password);
    if (changePass.newPassword != changePass.repeatNewPassword) {
      this.toastr.error('Password is missmatched!');
      return;
    }
  }
  onSubmitForm(): void {
    this.employeeForm.markAllAsTouched();
    const employee = { ...this.employeeForm.value };
    // console.log(this.employeeForm, '   :submit');
    
       if (this.employeeId) this.employeeForm.removeControl('password');
    if (this.employeeForm.invalid) {
      this.toastr.error(this.translate.instant('employee.message.invalidForm'));
      return;
    }
    // console.log('onSubmit, empoyeeId: ', this.employeeId);
    // console.log('length: ', this.employeeForm.value?.password?.length);

    if (this.employeeId) {
      this.onUpdate(this.employeeId, employee);
    } else {
      if (!this.employeeForm.value?.password || this.employeeForm.value?.password?.length < 8) {
        this.toastr.error(this.translate.instant('employee.message.errorPassword'));
        return;
      }
      // console.log('onSubmit, empoyeeId: ', this.employeeId);
      // console.log('length: ', this.employeeForm.value?.password?.length);

      if (this.employeeId) {
        this.employeeForm.removeControl('password');
      } else {
        if (!this.employeeForm.value?.password || this.employeeForm.value?.password?.length < 8) {
          this.toastr.error('Yêu cầu nhập mật khẩu trên 8 ký tự!');
          return;
        }
        this.onCreate(employee);
      }
    }
  }
  onCreate(employee: IEmployee) {
    this.employeeService.createEmployee(employee).subscribe((res) => {
      this.toastr.success('Bạn đã tạo nhân viên thành công!');
    });
  }

  onUpdate(employeeId: string, employee: IEmployee) {
    this.employeeService.updateEmployee(employeeId, employee).subscribe((data: any) => {
      this.toastr.success(this.translate.instant('employee.message.successUpdate'));
      this.router.navigate(['/admin/employee']);
    });
  }

  onResetpassword(){
    // console.log(this.employeeId);
    this.resetPasswordService.resetPassword(this.employeeId).subscribe(()=>{
      this.toastr.success(this.translate.instant('customer.manage.messageSuccess'));
    });
  }
}
