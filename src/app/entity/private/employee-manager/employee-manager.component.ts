import { EmployeeService } from './../../../shared/service/employee.service';
import { IEmployee } from './../../../shared/models/employee';
import { Data } from './../customer-manager/customer-manager.component';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ToastrService } from 'ngx-toastr';
import { CUSTOMER_GENDER } from 'src/app/shared/contants/customerGender';
import { CUSTOMER, dateToMs } from 'src/app/shared/contants/customerStatus';
import { DEFAULT_QUERY } from 'src/app/shared/contants/Constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-manager',
  templateUrl: './employee-manager.component.html',
  styleUrls: ['./employee-manager.component.scss'],
})
export class EmployeeManagerComponent implements OnInit, AfterViewInit {
  checked = false;
  loading = false;
  indeterminate = false;
  listOfData: IEmployee[] = [];
  listOfCurrentPageData: readonly Data[] = [];
  setOfCheckedId = new Set<string>();
  confirmModal?: NzModalRef;
  //filter date picker
  from?: any;
  toDate?: any;
  searchValue = '';
  sizePage = 0;

  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
    // startDate: 0,
    // endDate: 0,
  };
  // public querySearchFilterDate = {
  //   ...DEFAULT_QUERY,
  //   status: '',
  //   startDate: 0,
  //   endDate: 0,
  // };
  listOfSelection = [
    {
      text: 'Select All Row',
      onSelect: () => {
        this.onAllChecked(true);
      },
    },
    {
      text: 'Select Odd Row',
      onSelect: () => {
        this.listOfCurrentPageData.forEach((data, index) => this.updateCheckedSet(data.id, index % 2 !== 0));
        this.refreshCheckedStatus();
      },
    },
    {
      text: 'Select Even Row',
      onSelect: () => {
        this.listOfCurrentPageData.forEach((data, index) => this.updateCheckedSet(data.id, index % 2 === 0));
        this.refreshCheckedStatus();
      },
    },
  ];

  updateCheckedSet(id: string, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: readonly Data[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onItemChecked(id: string, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(checked: boolean): void {
    this.listOfCurrentPageData
      .filter(({ disabled }) => !disabled)
      .forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  sendRequest(): void {
    this.loading = true;
    const requestData = this.listOfData.filter((data) => this.setOfCheckedId.has(data.id));
    // console.log(requestData);
    setTimeout(() => {
      this.setOfCheckedId.clear();
      this.refreshCheckedStatus();
      this.loading = false;
    }, 1000);
  }
  constructor(
    private route: Router,
    private employeeService: EmployeeService,
    private modal: NzModalService,
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.loadData();
    // console.log('this is search value: ' + this.searchValue);
  }
  ngAfterViewInit() {
    this.employeeService.searchEmployee(this.querySearch).subscribe((data) => {
      this.sizePage = data.body.employeeDTOs.length;
    });
  }
  loadData() {
    this.employeeService.searchEmployee(this.querySearch).subscribe((data) => {
      this.listOfData = data.body.employeeDTOs;
      this.sizePage = data.body.employeeDTOs.length;
      console.log('Data employee: ', data);
    });
  }
  onNavCreate(): void {
    this.route.navigate(['/admin/employee/create'], {
      queryParams: {
        idtem: 1,
      },
    });
  }

  onChangeDatePicker(event: any): void {
    if (dateToMs(this.from) != null && dateToMs(this.toDate) != null) {
      // this.querySearch.startDate = dateToMs(this.from);
      // this.querySearch.endDate = dateToMs(this.toDate);
      this.loadData();
    }
  }

  // Status
  onStatusEmployee(active: number): string {
    if (active === CUSTOMER.BLOCKED) {
      return this.translateService.instant('customer.detail.blocked');
    } else if (active == CUSTOMER.VERIFY) {
      return this.translateService.instant('customer.detail.verify');
    }
    return this.translateService.instant('customer.detail.active');
  }

  // Gender
  onGenderEmployee(active: number): string {
    if (active === CUSTOMER_GENDER.MALE) {
      return this.translateService.instant('customer.detail.male');
    }
    return this.translateService.instant('customer.detail.female');
  }

  // Check customer status
  onCheckEmployeeStatus(idCustomer: string, status: number) {
    if (status === CUSTOMER.BLOCKED) {
      status = CUSTOMER.ACTIVE;
    } else if (status == CUSTOMER.ACTIVE) {
      status = CUSTOMER.BLOCKED;
    }
    // console.log('Id: ', idCustomer, ' Status: ', status);
    this.showConfirm(idCustomer, status);
  }
  // Block or Unblock Employee
  showConfirm(idEmployee: string, status: number): void {
    this.confirmModal = this.modal.confirm({
      nzTitle:
        this.translateService.instant('customer.manage.messageConfirmBlock') +
        (status == CUSTOMER.BLOCKED
          ? this.translateService.instant('customer.detail.blocked')
          : this.translateService.instant('customer.detail.unblock')) +
        ' ?',
      nzOnOk: () =>
        this.employeeService.lockUser(idEmployee).subscribe((data) => {
          this.toastrService.success(this.translateService.instant('customer.manage.messageSuccess'));
          this.loadData();
        }),
    });
  }
  onFilter(keyword: string) {
    this.querySearch.keyword = keyword;
    this.loadData();
  }
  // Filter by Status
  onOptionsSelected(value: any) {
    console.log('the selected value is .' + value);
    this.querySearch.status = value;
    this.loadData();
  }
  // Fiter by date
  onFilterByDate() {
    console.log(this.from);
    console.log(this.toDate);
    dateToMs(this.from);
  }
  //Pagination
  onPaginate(size: number, page: number) {
    this.querySearch.page = page;
    this.querySearch.size = size;
    this.loadData();
  }
  pageChange(event: any): void {
    // console.log('page change:', event);
    this.querySearch.page = event;
    this.loadData();
  }
  pageSizeChange(event: any): void {
    // console.log('size change: ', event);
    this.querySearch.size = event;
    this.loadData();
  }
}
