import { ConfirmRequestComponent } from './../../../../shared/component/confirm-request/confirm-request.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { NzUploadChangeParam, NzUploadFile, NzUploadXHRArgs } from 'ng-zorro-antd/upload';
import { ToastrService } from 'ngx-toastr';
import { getBase64 } from 'src/app/shared/untils/FunctionUntils';
import { FileUpload } from 'src/app/shared/models/file-upload';
import { FileUploadServiceService } from 'src/app/shared/service/file-upload-service.service';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.scss'],
})
export class EmployeeCreateComponent implements OnInit {
  constructor(
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private modalService: NgbModal,
    private fileUploadService: FileUploadServiceService
  ) {}
  fileList = []; // file của ant design
  profileFile?: File[] = []; // fiel khi upload
  // private Api: ApiService,
  previewImage: string | undefined = '';
  previewVisible = false;
  ngOnInit(): void {

  }
  // //Upload image
  handleChange(info: NzUploadChangeParam): void {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      this.toastrService.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      this.toastrService.success(`${info.file.name} file upload failed.`);
    }
  }
  async handleUploadImg($event) {
    if ($event.type === 'success') {
      const file = $event?.file;
      if (!file?.url) {
        file.url = await getBase64(file.originFileObj);
        this.profileFile.push(file);
        // this.imageLoad = file.url;
        console.log('Base64: ', file.url);

        // this.fileList.push(file);
      }
    } else if ($event.type === 'removed') {
      this.fileList = this.fileList.filter((item) => item.uid !== $event?.file.uid);
    }
  }
  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = this.fileList.concat(file);
    return true;
  };
  handleRemoveImg = (file: NzUploadFile): boolean => {
    console.log('remove file', file);
    return true;
    // if (file.scoreCardId) {
    const modal = this.modalService.open(ConfirmRequestComponent, {
      size: 'md',
      backdrop: 'static',
      centered: false,
    });
    modal.componentInstance.title = this.translateService.instant('common.modal.questionDelete');
    modal.componentInstance.confirmButton = this.translateService.instant('common.modal.agree');
    modal.result.then((result) => {
      console.log(result);
      if (result === 'confirm') {
        console.log(file);
        return true;
      } else {
        return false;
      }
    });
  };
  handlePreview = async (file: NzUploadFile) => {
    if (!file.url && !file.preview) {
      // tslint:disable-next-line:no-non-null-assertion
      file.preview = await getBase64(file?.originFileObj!);
    }
    console.log(file.preview);
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  };
  handleCustomUploadReq = (item: NzUploadXHRArgs) => {
    const productFileUpload = new FileUpload();

    console.log(item.file);
    console.log(this.profileFile);
    const n = Date.now();
    const file = item.file;
    // const file = event.target.files[0];
    productFileUpload.originalName = file?.name;
    productFileUpload.fileSize = file?.size;
    productFileUpload.fileType = file?.type;
    console.log(item);
    return this.fileUploadService.fakeUploadFile(false).subscribe(
      (res) => {
        item.onSuccess(null, item.file, null);
      },
      (error) => item.onError(error, file)
    );
  };
}
