import { DateCenterService } from './../../../shared/service/data-service/date-center.service';
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ProductService } from '../../../shared/service/product.service';
import { CategoryService } from '../../../shared/service/category.service';
import { ICategory } from '../../../shared/models/category';
import { Iproduct, Product } from '../../../shared/models/product';
import { SORT_PRODUCT_BY, SHOW_RECORD, ENTITY_TYPE, DEFAULT_QUERY } from '../../../shared/contants/Constants';
import { FileUploadService } from 'src/app/shared/service/file-upload.service';
import { filter } from 'rxjs/operators';
import { WishlistService } from '../../../shared/service/wishlist.service';
import {ToastrService} from "ngx-toastr";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-shopping-page',
  templateUrl: './shopping-page.component.html',
  styleUrls: ['./shopping-page.component.scss']
})
export class ShoppingPageComponent implements OnInit, OnChanges {

  constructor(private productService: ProductService,
              private categoryService: CategoryService,
              public fileService: FileUploadService,
              private favorite: WishlistService,
              private toastrService: ToastrService,
              private router: Router,
              private activeRoute: ActivatedRoute,
              private dataCenter: DateCenterService) { }

  public listCategory: ICategory[];
  public listProduct?: Product[] = [];
  public favoriteProductList: Product[];
  public TYPE_PRODUCT = ENTITY_TYPE.PRODUCT;
  public SORT_PRODUCT_BY = SORT_PRODUCT_BY;
  public SHOW_RECORD = SHOW_RECORD;
  public totalItem = 0;
  public minPrice: number;
  public maxPrice: number;
  public categoryId: string[] = [];
  public querySearch = {
    ...DEFAULT_QUERY,
    size: 9,
    categoryId: [],
    minPrice: '',
    maxPrice: '',
  }

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe(params => {
      console.log(params.categoryId);
      if (params.keyword) {
        this.querySearch.keyword = params.keyword;
      } else {
        this.querySearch.keyword = '';
      }
      if(!!params?.categoryId) {
        this.querySearch.categoryId.push(params?.categoryId ? params.categoryId : '')
      };
      this.loadProduct();
    });
    this.loadCategory();
    // this.loadProduct();
    this.loadFavoriteProductList();
  }
  ngOnChanges(changes: SimpleChanges): void {

  }

  checkIsNewProduct(createDate:string): boolean{
    let dateSent = new Date(createDate);
    let currentDate = new Date();
    let diff = Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(dateSent.getFullYear(), dateSent.getMonth(), dateSent.getDate()) ) /(1000 * 60 * 60 * 24));
    return diff >= 15 ? false : true
  }

  loadCategory(): void{
    this.categoryService.loadAllCategory(null).subscribe(response => {
      this.listCategory = response.body?.categoryDTOS;
    });
  }

  loadProduct(): void{
    this.querySearch.categoryId = this.categoryId

    this.productService.loadProduct(this.querySearch).subscribe(response => {
      console.log(response);
      this.listProduct = response.body?.listProductDTO;
      // this.totalItem = response.body?.totalItem;

      console.log(this.listProduct);
    });
  }

  loadFavoriteProductList(){
    const query = {
      sort: 'likeCount,desc'
    }
    this.productService.loadProduct(query).subscribe(response => {
      let listAll = response.body?.listProductDTO;
      this.favoriteProductList = listAll.slice(0,3);
    })
  }

  categoriesCheckBox(event, categoryId:string){
    console.log(event, categoryId);
    if (event.target.checked && !this.categoryId.includes(categoryId)) {
      this.categoryId.push(categoryId);
    }else if(!event.target.checked){
      this.categoryId = this.categoryId.filter(i=>i != categoryId);
    }
    this.loadProduct();
  }

  priceFilter(){
    if (this.minPrice != null) {this.querySearch.minPrice = this.minPrice.toString()}else{this.querySearch.minPrice = ''}
    if (this.maxPrice != null) {this.querySearch.maxPrice = this.maxPrice.toString()}else{this.querySearch.maxPrice = ''}
    this.loadProduct();
  }

  sortByFilter() {
    this.loadProduct();
  }

  recordShow() {
    this.loadProduct();
  }

  onPageChange(event) {
    this.querySearch.page = (event - 1);
    this.loadProduct();
  }

  generateCategoryIdParam(): string {
    let result: string = '';
    this.categoryId.forEach(item => result += (item + ','));
    return result;
  }

  public likeProduct(product: Iproduct): void {
    this.favorite.addToWishlist(product.id).subscribe(response => {
      // console.log(response);
      // this.onloadTotalFav();
      this.toastrService.success('Sản phẩm đã được thêm vào danh sách yêu thích', 'Success');
    });
  }
  onloadTotalFav(){
    this.favorite.loadWishlist(this.querySearch).subscribe((data) => {
          let totalFav = data.body?.totalItem ? data.body?.totalItem : 0;
          this.dataCenter.setTotalFavoriteItem(totalFav);
        });
  }
  goToDetail(id: string) {
    this.router.navigate(['/product/' + id]);
  }
}
