import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../shared/service/product.service';
import { CategoryService } from '../../../shared/service/category.service';
import { FileUploadService } from '../../../shared/service/file-upload.service';
import { WishlistService } from '../../../shared/service/wishlist.service';
import { CategoryComponentComponent } from '../../../layout/publicComponent/category-component/category-component.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ICategory } from 'src/app/shared/models/category';
import { CATEGORY_FIXER, CATEGORY_TYPE, DEFAULT_QUERY } from '../../../shared/contants/Constants';
import { Iproduct, Product } from '../../../shared/models/product';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public listCategory: ICategory[];
  public listNewProduct: Product[];
  public listTopSellingProduct: Product[];
  public categorySale = CATEGORY_TYPE;
  public productNewQuery: any = {
    ...DEFAULT_QUERY,
    sort: 'createDate,DESC',
    categoryId: '',
  };
  public productTopSellingQuery: any = {
    ...DEFAULT_QUERY,
    sort: 'createDate,ASC',
    categoryId: '1',
  };
  public indexCategoryMOI = '1';
  public indexTopSelling = '1';
  public listProductMoi?: Product[];
  public listProductTopSelling?: Product[];

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private fileService: FileUploadService,
    private favorite: WishlistService,
    private ngModalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.getListNewProduct();
    this.getListTopSellingProduct();
    setTimeout(() => {
      // this.onCreateModel();
      // this.getListCategory();
      // this.getListNewProduct();
      // this.getListTopSellingProduct();
    }, 1000);
  }

  changeCategory(categoryId?: string, type?: string) {
    if (categoryId) {
      if (type === 'moi') {
        this.productNewQuery.categoryId = categoryId;
        this.indexCategoryMOI = categoryId;
        this.getListNewProduct();
      } else {
        this.productTopSellingQuery.categoryId = categoryId;
        this.getListTopSellingProduct();
      }
    }
  }

  getListNewProduct() {
    this.productService.loadProduct(this.productNewQuery).subscribe((res) => {
      this.listProductMoi = res.body.listProductDTO as Array<Iproduct>;
    });
  }

  getListTopSellingProduct() {
    this.productService.loadProduct(this.productTopSellingQuery).subscribe((res) => {
      this.listProductTopSelling = res.body.listProductDTO as Array<Iproduct>;
    });
  }
  onCreateModel(): void {
    const modalRef = this.ngModalService.open(CategoryComponentComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false,
    });
    modalRef.componentInstance.modalRef = modalRef;
    modalRef.componentInstance.isCreate = true;
    modalRef.result.then((result) => {
      if (result) {
        console.log(result);
      }
    });
  }
}
