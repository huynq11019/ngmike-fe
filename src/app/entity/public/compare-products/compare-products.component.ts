import { Component, OnInit } from '@angular/core';
import { CompareServiceService } from '../../../shared/service/compare-service.service';
import { File } from '@angular/compiler-cli/src/ngtsc/file_system/testing/src/mock_file_system';
import { FileUploadService } from '../../../shared/service/file-upload.service';
import { Iproduct } from '../../../shared/models/product';
import { AttributeService } from '../../../shared/service/attribute.service';
import { ToastrService } from 'ngx-toastr';
import { Iattibute } from '../../../shared/models/attribute';
import { IproductAttibute } from '../../../shared/models/productAttribute';

interface extentIattibute extends Iattibute {
  productAttribute: IproductAttibute[];
}

@Component({
  selector: 'app-compare-products',
  templateUrl: './compare-products.component.html',
  styleUrls: ['./compare-products.component.scss'],
})
export class CompareProductsComponent implements OnInit {
  productList: Iproduct[];
  // attributeList: IproductAttibute[];
  attributeList: extentIattibute[] = [];

  constructor(public compareProduct: CompareServiceService,
              public fileService: FileUploadService,
              private attributeService: AttributeService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    const iproducts = this.compareProduct.getCompareList();
    console.log(iproducts);
    this.productList = iproducts;
    if (this.productList?.length > 0 && !!this.productList[0].categoryId) {
      this.loadAttributeByCategory(this.productList[0].categoryId);
    } else {
      this.toastr.warning('Bạn cần chọn sẩn phẩm đẻ so sánh');
    }
  }

  public removeProduct(product: Iproduct) {
    this.compareProduct.removeProductFromCompare(product);
    this.productList = this.compareProduct.getCompareList();
  }

  public loadAttributeByCategory(categoryId: string) {
    this.attributeService.loadAttrubuteByCategory(null, categoryId).subscribe(
      (res) => {
        this.attributeList = res.body?.attributeList as Array<extentIattibute>;
        this.loadAttributeByProduct();
      }
    );
  }

  loadAttributeByProduct() {
    if (this.productList.length > 0) {
      // let indexProduct = 0;
      this.productList.forEach(product => {
        let myAttribute: IproductAttibute[] = [];
        this.attributeService.loadAttributeByProduct(product.id).subscribe(
          (data) => {
            myAttribute = data.body.productAttributeList;

            this.attributeList.forEach(attribute => {
              const myAttributeItem = myAttribute.find(item => item.attributeId === attribute.id);
              // console.log(myAttributeItem);
              if (!attribute.productAttribute) {
                attribute.productAttribute = [];
              }
              attribute.productAttribute.push(myAttributeItem);
            });
          },
        );
        // indexProduct++;
      });
    }
  }
}
