import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { MyOrderService } from './../../../shared/service/my-order.service';
import { IOrder } from './../../../shared/models/order-customer';
import { IMyAddress } from './../../../shared/models/my-address';
import { ICustomer } from 'src/app/shared/models/customer';
import { CustomerService } from './../../../shared/service/customer.service';
import { MyAddressService } from './../../../shared/service/my-address.service';
import { LOCAL_STORAGE } from './../../../shared/contants/location-session-cookie';
import { LocalStorageService } from 'ngx-webstorage';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { REGEX_CUSTOM } from 'src/app/shared/contants/Constants';
import { MyCartService } from 'src/app/shared/service/my-cart.service';
import { ICart } from 'src/app/shared/models/my-cart';
import { ILocation } from 'src/app/shared/models/location';
import { LocationPickerService } from 'src/app/shared/service/location-picker.service';
import { Router } from '@angular/router';
import { IFeeRequest } from '../../../shared/models/FeeRequest';
import { GhnServiceService } from '../../../shared/service/ghn-service.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit {
  constructor(
    private myAddressService: MyAddressService,
    private $localStorage: LocalStorageService,
    private formBuilder: FormBuilder,
    private customerService: CustomerService,
    private myCartService: MyCartService,
    private myOrderService: MyOrderService,
    private toastr: ToastrService,
    private translate: TranslateService,
    private router: Router,
    private ghnService: GhnServiceService
  ) {
    this.initForm();
  }

  retrieve = [];
  infoCustomer?: ICustomer;
  infoOrder?: IOrder;
  customerForm: any;
  shippingConst = 0;
  intoMoneyData?: number = 0;
  totalDiscount?: number = 0;
  numberProducts?: number = 0;
  quantityData?: number = 1;
  listOfMyCart: ICart[];
  dataProvince: ILocation[] = [];
  isVisible = false;
  isOkLoading = false;
  checkedTerms = false;
  checkedPlaceOrder = true;
  addressSelected?: IMyAddress;
  addressId?: number;

  feeShippingRequest: IFeeRequest = {
    from_district_id: 1542,
    height: 30,
    insurance_value: 1000,
    length: 30,
    service_id: 53320,
    service_type_id: null,
    to_district_id: 0,
    to_ward_code: '',
    width: 30,
    weight: 1000,

  };

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  chooseAddress(address: IMyAddress) {
    console.log(address);
    this.addressId = address.id ;
    this.feeShippingRequest.to_district_id = Number(address.districtId);
    this.feeShippingRequest.to_ward_code = address?.wardId + '';

    this.infoCustomer.address =
      address.addressDetail + ', ' + address.ward + ', ' + address.district + ', ' + address.province;
    // console.log('Choose: ', address.addressDetail);
    this.initForm();
    this.isVisible = false;
    this.ghnService.getFee(this.feeShippingRequest).subscribe(
      (data) => {
        console.log(data.body.data.total);
        this.customerForm.get('shippingCost').setValue(data.body.data.total || 0) ;
        this.intoMoney();
        // this.infoOrder.feeShipping = data.fee;
        // this.infoOrder.feeShipping = this.infoOrder.feeShipping.toFixed(0);
        // this.infoOrder.total = this.infoOrder.total - this.infoOrder.feeShipping;
        // this.infoOrder.total = this.infoOrder.total.toFixed(0);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  resolved(captchaResponse: string) {
    // console.log(`Resolved captcha with response: =>> ${captchaResponse}`);
    this.customerForm.get('captchaCode').setValue(captchaResponse);
  }

  // random password generator
  generatePassword() {
    var length = 8,
      charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      retVal = '';
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }

  private initForm(): void {
    this.customerForm = this.formBuilder.group({
      fullName: [this.infoCustomer?.fullName || '', [Validators.required]],
      email: [this.infoCustomer?.email || '', [Validators.required, Validators.pattern(REGEX_CUSTOM.EMAIL)]],
      phoneNumber: [
        this.infoCustomer?.phoneNumber || '',
        [Validators.required, Validators.pattern(REGEX_CUSTOM.PHONE_NUMBER)],
      ],
      address: [
        {
          value: this.infoCustomer?.address || '',
          disabled: true,
        },
        [Validators.required],
      ],
      note: [this.infoOrder?.note || '', [Validators.required]],
      orderDetails: [this.listOfMyCart || [], [Validators.required]],
      captchaCode: [null, [Validators.required]],
      receiver: [this.infoCustomer?.fullName || '', [Validators.required]],
      orderStatus: [this.infoOrder?.orderStatus || 'CREATE', [Validators.required]],
      paymentMethod: [this.infoOrder?.paymentMethod || 'ZALO_PAY', [Validators.required]],
      customerId: [this.infoCustomer?.id || '', [Validators.required]],
      shippingCost: [this.infoOrder?.shippingCost || '', [Validators.required]],
    });
  }

  // Load cart list
  onLoadMyCart(cartIds: string[], noloading?) {
    this.myCartService.getCartByIds(cartIds, noloading).subscribe((data: any) => {
      this.listOfMyCart = data.body?.cartDTOs;
      if (data.body.cartDTOs.length == 0) {
        window.history.back();
      }
      for (let i = 0; i < this.listOfMyCart.length; i++) {
        this.listOfMyCart[i].cartItemId = this.listOfMyCart[i].id;
      }
      this.totalCart();
      this.numberProducts = data.body.totalCartItem;
      // console.log('====================================');
      // console.log('Load cart:', data.body.cartDTOs.length);
      // console.log('====================================');
      this.initForm();
    });
  }

  // Load into money, total discount
  totalCart() {
    this.listOfMyCart?.forEach((item) => {
      this.totalDiscount += item.discount;
      this.intoMoneyData += item.totalPrice;
    });
  }

  // reurn into money
  intoMoney(): number {
    let money = 0;
    this.listOfMyCart?.forEach((cart) => {
      money += cart.quantity * cart.productPrice;
    });
    return (this.intoMoneyData = money);
  }

  onLoadInfoCustomer() {
    this.customerService.getCurrentUserInfo().subscribe((data) => {
      this.infoCustomer = data.body.customerDTO;
      this.initForm();
    });
  }

  onCreateOrder() {

    this.customerForm.markAllAsTouched();
    const order: IOrder = {
      ...this.customerForm.value,
      addressId: this.addressId,
    };
    this.myOrderService.createOrder(order).subscribe(() => {
      this.toastr.success(this.translate.instant('checkout.successOrder'));
      this.$localStorage.clear(LOCAL_STORAGE.ORDER);
      this.router.navigate(['/user/my-list-order']);
    });
  }

  onChangePaymentMethod(event) {
    console.log(event);
    if (event == 'COD') {
      if (this.intoMoney() > 5000000) {
        this.toastr.error("Đơn hàng vượt quá số tiền thu hộ vui lòng chọn phương thức thanh toán khác");
        return;
      }

    }
    this.customerForm.get('paymentMethod').setValue(event );
  }

  ngOnInit(): void {
    this.retrieve = JSON.parse(this.$localStorage.retrieve(LOCAL_STORAGE.ORDER)) as ICart[];
    const cartIds = this.retrieve.map((item) => item.id);
    console.log('Retrieve: ', this.retrieve);
    // console.log(this.retrieve);
    if (this.retrieve) {
      this.onLoadInfoCustomer();
      this.onLoadMyCart(cartIds);
      this.initForm();
    } else {
      window.history.back();
    }
  }
}
