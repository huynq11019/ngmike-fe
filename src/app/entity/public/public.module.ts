import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { CompareProductsComponent } from './compare-products/compare-products.component';
import { TermsOfServiceComponent } from './about-me/terms-of-service/terms-of-service.component';
import { PiracyPolicyComponent } from './about-me/piracy-policy/piracy-policy.component';
import { RefundPolicyComponent } from './about-me/refund-policy/refund-policy.component';
import { IntroduceComponent } from './about-me/introduce/introduce.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { HomeComponent } from './home/home.component';
import { ShoppingPageComponent } from './shopping-page/shopping-page.component';
import { LayoutModule } from '../../layout/layout.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ListProductComponent } from './list-product/list-product.component';
import { PostComponent } from './post/post.component';
import { CartComponent } from './cart/cart.component';
import { UserProfileModule } from './user-profile/user-profile.module';
import { WishListComponent } from './wish-list/wish-list.component';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { AvatarModule } from 'ngx-avatar';
import { TranslateModule } from '@ngx-translate/core';
import { AboutMeComponent } from './about-me/about-me.component';
import { ContactMeComponent } from './contact-me/contact-me.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { RatingProductComponent } from './product-detail/rating-product/rating-product.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzTreeSelectModule } from 'ng-zorro-antd/tree-select';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { RecaptchaModule } from 'ng-recaptcha';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzGridModule } from 'ng-zorro-antd/grid';
import {NzRateModule} from "ng-zorro-antd/rate";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { QRCodeModule } from 'angularx-qrcode';
import {ShareModuleModule} from "../../shared/share-module.module";
import {NzInputNumberModule} from "ng-zorro-antd/input-number";
import { CheckoutSuccessComponent } from './checkout/checkout-success/checkout-success.component';
import { MycartComponent } from './mycart/mycart.component';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import {NzImageModule} from "ng-zorro-antd/image";
@NgModule({
  declarations: [
    HomeComponent,
    ShoppingPageComponent,
    UserProfileComponent,
    ProductDetailComponent,
    ListProductComponent,
    PostComponent,
    CartComponent,
    WishListComponent,
    AboutMeComponent,
    ContactMeComponent,
    CheckoutComponent,
    IntroduceComponent,
    RefundPolicyComponent,
    PiracyPolicyComponent,
    TermsOfServiceComponent,
    CheckoutSuccessComponent,
    MycartComponent,
    CompareProductsComponent,
    PaymentSuccessComponent
  ],
    imports: [
        CommonModule,
        PublicRoutingModule,
        LayoutModule,
        UserProfileModule,
        AvatarModule,
        UserProfileModule,
        NzPaginationModule,
        TranslateModule,
        NzTabsModule,
        NzIconModule,
        NzTableModule,
        NzInputModule,
        NzButtonModule,
        NzBreadCrumbModule,
        FormsModule,
        ReactiveFormsModule,
        NzSelectModule,
        NzModalModule,
        NzStepsModule,
        NzTimelineModule,
        NzTreeSelectModule,
        NzDividerModule,
        RecaptchaModule,
        ReactiveFormsModule,
        NzCheckboxModule,
        NzSliderModule,
        NzGridModule,
        NgbModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDKSDZnbaGIvoR1CxfDQA0PHT6jDki4s2Q',
        }),
        NzRateModule,
        QRCodeModule,
        ShareModuleModule,
        NzInputNumberModule,
        NzLayoutModule,
        NzMenuModule,
        NzImageModule
    ],
})
export class PublicModule {}
