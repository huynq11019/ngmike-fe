import { DateCenterService } from './../../../shared/service/data-service/date-center.service';
import { IOptionProduct } from './../../../shared/models/OptionProduct';

import { ICustomer } from './../../../shared/models/customer';
import { CustomerService } from './../../../shared/service/customer.service';
import { WishlistService } from './../../../shared/service/wishlist.service';
import { IRatting } from '../../../shared/models/ratting';
import { RattingService } from '../../../shared/service/ratting.service';
import { ICart } from 'src/app/shared/models/my-cart';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { MyCartService } from 'src/app/shared/service/my-cart.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Iproduct } from '../../../shared/models/product';
import { ProductService } from '../../../shared/service/product.service';
import { Component, OnInit } from '@angular/core';
import { FileUploadService } from '../../../shared/service/file-upload.service';
import { DEFAULT_QUERY, ENTITY_TYPE } from '../../../shared/contants/Constants';
import { Environment } from '@angular/compiler-cli/src/ngtsc/typecheck/src/environment';
import { AttributeService } from '../../../shared/service/attribute.service';
import { IproductAttibute } from '../../../shared/models/productAttribute';
import { CompareServiceService } from '../../../shared/service/compare-service.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {
  dataOfProductDetail?: Iproduct;
  sameProducts?: Iproduct[];
  infoCustomer?: ICustomer;
  TYPE_PRODUCT = ENTITY_TYPE.PRODUCT;
  FALLBACK =
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg==';


  productId: string;
  public tabIndex = {
    description: 0,
    details: 1,
    review: 2,
  };
  public currentTab;
  ratting?: IRatting[];
  optionSelected?: IOptionProduct;
  checkedFiles?: boolean = false;
  ratingNumber?: number = 0;
  checkedLogin?: boolean = false;
  listAttribute?: IproductAttibute[];
  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
  };

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    public fileService: FileUploadService,
    private myCartService: MyCartService,
    private toastr: ToastrService,
    private translate: TranslateService,
    private router: Router,
    private rattingService: RattingService,
    private wishlistService: WishlistService,
    private customerService: CustomerService,
    private comapreService: CompareServiceService,
    private attributeService: AttributeService,
    private dataCenter : DateCenterService
  ) {
    this.route.params.subscribe((param) => {
      this.productId = param['productid'];
    });
    // this.onLoadCustomerCurrent();
  }

  ngOnInit(): void {
    this.currentTab = this.tabIndex.description;
    this.onLoadData();
    this.changeOptionProduct();
  }

  public onTabChange(tabIndex: number) {
    this.currentTab = tabIndex;
  }

  onLoadCustomerCurrent() {
    // this.customerService.getCurrentUserInfo().subscribe((data) => {
    //   this.infoCustomer = data.body.customerDTO;
    //   this.checkedLogin = true;
    //   this.dataOfProductDetail.customerId = this.infoCustomer.id ? this.infoCustomer.id : '';
    // });
  }

  onLoadData() {
    this.productService.getProductByIdRicher(this.productId).subscribe(
      (data) => {
        // if (data.body.productDTO.files[0] && data.body.productDTO.files[1] && data.body.productDTO.files[2]) {
        //   this.checkedFiles = true;
        // }
        // console.log('products: ', data);
        this.dataOfProductDetail = data?.body?.productDTO;
        this.dataOfProductDetail.quantity = 1;
        this.dataOfProductDetail.productId = this.productId;
        if (!!this.dataOfProductDetail.optionProducts) {
          // this.optionSelected = this.dataOfProductDetail.optionProducts[0];
          //get product with min value in array
          this.optionSelected = this.dataOfProductDetail.optionProducts.sort((a, b) => a.price - b.price)[0];
        }
        this.loadAttributeByProduct();
        this.loadProductSameType();
      },
      (error) => {
        alert('Something wrong!');
        window.history.back();
      }
    );
    this.rattingService.loadRatting(this.productId).subscribe((data) => {
      this.ratting = data?.body.ratingDTOList as Array<IRatting>;
      this.ratingNumber = data?.body.ratingDTOList.length as number;
      // console.log('ratting: ', data?.body.ratingDTOList);
    });
  }

  changeOptionProduct(event?: any) {
    // console.log('event: ', event);
    // this.productService.getProductByIdRicher(this.productId).subscribe((data) => {
    //   console.log('option products: ', event);
    //   this.dataOfProductDetail.avgPrice = event?.price;
    // });
    // this.rattingService.loadRatting(this.productId).subscribe((data) => {
    //   this.ratting = data?.body.ratingDTOList;
    //   // console.log('ratting: ', data?.body.ratingDTOList);
    // });
  }

  increment(cart: Iproduct) {
    cart.quantity += 1;
    // console.log(cart.quantity);
  }

  // Decrement quantity
  decrement(cart: Iproduct): void {
    if (cart.quantity <= 1) {
      return;
    }
    cart.quantity -= 1;
    // console.log(cart.quantity);
  }

  // onChangeQuanity(event: any) {
  //   console.log('quantity: ', event);
  //   this.dataOfProductDetail.quantity = event;
  // }

  onAddToCart(cart?: ICart) {
    cart.productId = this.productId;
    cart.quantity = Number(this.dataOfProductDetail.quantity);
    cart.optionProductId = this.optionSelected?.id;
    this.myCartService.addToCart(cart).subscribe((data) => {
      this.toastr.success(this.translate.instant('cart.message.successCreate'));
    });
  }

  onAddFavorite(favorite: Iproduct) {
    this.wishlistService.createFavorite(favorite).subscribe((data) => {
      this.onloadTotalFav();
      this.toastr.success(this.translate.instant('cart.message.successFavorite'));
    });
  }
  onloadTotalFav() {
    this.wishlistService.loadWishlist(this.querySearch).subscribe((data) => {
      let totalFav = data.body?.totalItem ? data.body?.totalItem : 0;
      this.dataCenter.setTotalFavoriteItem(totalFav);
    });
  }
  /**
   * @author: huynq
   * @since: 12/11/2021 5:36 PM
   * @description:  getnarater qr code
   * @update:
   */
  getQrCode(): string {
    return this.productService.getQr(this.productId);
  }

  /**
   * load sản phẩm cùng loại
   */
  loadProductSameType() {
    // categoryId: this.dataOfProductDetail?.categoryId

    this.productService
      .loadProduct({
        categoryId: this.dataOfProductDetail?.categoryId,
      })
      .subscribe((data) => {
        this.sameProducts = data.body?.listProductDTO as Array<Iproduct>;
      });
  }

  loadAttributeByProduct(): void {
    this.attributeService.loadAttributeByProduct(this.productId).subscribe((data) => {
      console.log(data.body?.productAttributeList as Array<IproductAttibute>);
      this.listAttribute = data.body?.productAttributeList as Array<IproductAttibute>;
    });
  }

  public addToCompare(product: Iproduct): void {
    this.comapreService.addProductToCompare(product);
  }
}
