import { DateCenterService } from './../../../shared/service/data-service/date-center.service';
import { Component, OnInit } from '@angular/core';
import { ICart } from '../../../shared/models/my-cart';
import { MyCartService } from '../../../shared/service/my-cart.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { FileUploadService } from '../../../shared/service/file-upload.service';
import { LocalStorageService } from 'ngx-webstorage';
import { DEFAULT_QUERY, ENTITY_TYPE } from '../../../shared/contants/Constants';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import CommonUtil from '../../../shared/untils/common-utils';
import { LOCAL_STORAGE } from '../../../shared/contants/location-session-cookie';

export interface cartItem extends ICart {
  disabled?: boolean;
}

@Component({
  selector: 'app-mycart',
  templateUrl: './mycart.component.html',
  styleUrls: ['./mycart.component.scss'],
})
export class MycartComponent implements OnInit {
  checked = false;
  loading = false;
  indeterminate = false;
  carts: readonly cartItem[] = [];
  listOfCurrentPageData: readonly cartItem[] = [];

  cartItemSelections: cartItem[] = [];
  setOfCheckedId = new Set<string>();
  TYPE_PRODUCT = ENTITY_TYPE.PRODUCT;
  isChangeQuantity = false;
  isCallFirstRequest = false;
  intoMoneyData?: number = 0;
  queryParams = {
    ...DEFAULT_QUERY,
  };
  ///
  intoMoney = 0;
  intoDiscount = 0;

  constructor(
    private myCartService: MyCartService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private router: Router,
    private translate: TranslateService,
    public fileService: FileUploadService,
    private $localStorage: LocalStorageService,
    private dataCenter : DateCenterService
  ) {}

  updateCheckedSet(id: string, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: readonly cartItem[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
    this.intoMoney = this.carts?.filter((item) => this.setOfCheckedId.has(item.id))
      .reduce((acc, cur) => acc + cur.productPrice * cur.quantity, 0);
    this.intoDiscount = this.carts?.filter((item) => this.setOfCheckedId.has(item.id))
      .reduce((acc, cur) => acc + ((cur.productPrice * cur.quantity) / 100) * cur.quantity, 0);
    console.log(this.intoMoney);
  }

  onItemChecked(cart: ICart, checked: boolean): void {
    this.updateCheckedSet(cart.id, checked);
    this.refreshCheckedStatus();
    console.log(...this.setOfCheckedId.entries());
  }

  onAllChecked(checked: boolean): void {
    this.listOfCurrentPageData
      .filter(({ disabled }) => !disabled)
      .forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  sendRequest(): void {
    this.loading = true;
    const requestData = this.carts.filter((data) => this.setOfCheckedId.has(data?.id));
    console.log(requestData);
    setTimeout(() => {
      this.setOfCheckedId.clear();
      this.refreshCheckedStatus();
      this.onCreate(requestData);
      this.loading = false;
    }, 1000);
  }
  // Into money when the quantity is updated
  total(cart: ICart): number {
    return (this.intoMoneyData = cart.quantity * cart.productPrice);
  }
  ngOnInit(): void {
    this.onLoadMyCart();
  }

  // increment quantity
  increment(cart: ICart) {
    cart.quantity += 1;
    this.onUpdate(cart);
  }

  // Decrement quantity
  decrement(cart: ICart): void {
    if (cart.quantity <= 1) {
      return;
    }
    cart.quantity -= 1;
    this.onUpdate(cart);
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    const { sortBy } = CommonUtil.onQueryParam(params);
    this.queryParams.sort = sortBy;
    console.log(this.queryParams);
  }

  // Load cart lista
  onLoadMyCart(noloading?) {
    this.myCartService.searchCart(noloading).subscribe((data: any) => {
      this.carts = data.body?.cartDTOs as Array<ICart>;
      this.carts?.forEach((item) => {
        item.cartItemId = item.id;
      });
      this.dataCenter.setTotalCartItem(data.body.totalCartItem? data.body.totalCartItem:0)
      console.log("crt: ",data);

      // this.totalCart();
      // this.numberProducts = data.body.totalCartItem;
    });
  }

  onUpdate(cart: ICart) {
    this.isChangeQuantity = true;
    this.myCartService.createOrUpdateCart(cart, true).subscribe((data: any) => {
      // console.log(data);
      this.isChangeQuantity = false;
      this.onLoadMyCart(true);
    });
  }

  // Delete items in cart
  onDeleteCart(cartId?: string) {
    console.log(this.setOfCheckedId.entries());
    const cartIds = cartId ? [cartId] : [...this.setOfCheckedId.values()];

    // console.log('onDeleteCart, cart: ', this.collectIds);
    this.myCartService.deleteCartByIds(cartIds, true).subscribe((data) => {
      this.onLoadMyCart(true);
      this.toastr.success(this.translate.instant('cart.message.successDelete'));
    });
  }

  onCreate(iCarts: ICart[]): void {
    this.$localStorage.clear(LOCAL_STORAGE.ORDER);
    this.$localStorage.store(LOCAL_STORAGE.ORDER, JSON.stringify(iCarts));
    // console.log('this is localStorage: ', this.collectIds);
    this.router.navigate(['/checkout']);
  }
}
