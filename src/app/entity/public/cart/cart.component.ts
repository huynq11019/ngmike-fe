
import { ICart } from './../../../shared/models/my-cart';
import { MyCartService } from './../../../shared/service/my-cart.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder } from '@angular/forms';
import { DEFAULT_QUERY, ENTITY_TYPE } from 'src/app/shared/contants/Constants';
import { TranslateService } from '@ngx-translate/core';
import { stringify } from 'querystring';
import { FileUploadService } from 'src/app/shared/service/file-upload.service';
import { LOCAL_STORAGE } from 'src/app/shared/contants/location-session-cookie';
import { LocalStorageService } from 'ngx-webstorage';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import CommonUtil from '../../../shared/untils/common-utils';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  public TYPE_PRODUCT = ENTITY_TYPE.PRODUCT;

  constructor(
    private myCartService: MyCartService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private router: Router,
    private translate: TranslateService,
    public fileService: FileUploadService,
    private $localStorage: LocalStorageService
  ) {
  }

  ngOnInit(): void {
    this.onLoadMyCart();
  }

  listOfMyCart: ICart[];
  infoCart: ICart;
  intoMoneyData?: number = 0;
  totalDiscount?: number = 0;
  numberProducts?: number = 0;
  quantityData?: number = 1;
  listOfSelection = [
    {
      text: 'Chọn tất cả',
      onSelect: () => {
        this.onAllChecked(true);
      },
    },
    {
      text: 'Chọn hàng lẻ',
      onSelect: () => {
        this.listOfCurrentPageData?.forEach((data, index) => this.updateCheckedSet(data.id, index % 2 !== 0));
        this.refreshCheckedStatus();
      },
    },
    {
      text: 'Chọn hàng chẵn',
      onSelect: () => {
        this.listOfCurrentPageData?.forEach((data, index) => this.updateCheckedSet(data.id, index % 2 === 0));
        this.refreshCheckedStatus();
      },
    },
  ];
  // Check change quantity will be disable button increment or decrement or input
  isChangeQuantity = false;
  checked = false;
  allChecked = false;
  indeterminate = false;
  listOfCurrentPageData: readonly ICart[] = [];
  isCallFirstRequest = true;
  listOfData?: ICart[] = [];
  setOfCheckedId = new Set<string>();
  collectIds = [];
  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
  };
  onChecked():boolean{
    if(this.checked == false && this.indeterminate == true){
      return false;
    }
    if (this.checked == true && this.indeterminate == false) {
      return false;
    }
    if(this.allChecked){
      return false;
    }
      return true;
  }
  updateCheckedSet(id: string, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  // Default select box ant design
  onItemChecked(id: string, checked: boolean, cart: ICart): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
    // console.log("ID: ", id);

    this.collectIds.push(id);
  }
  // Default select box ant design
  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData?.forEach((item) => {
      this.updateCheckedSet(item.cartItemId, value);
    });
    this.refreshCheckedStatus();
    this.allChecked = value;
  }

  // Default select box ant design
  onCurrentPageDataChange($event: readonly ICart[]): void {
    this.listOfCurrentPageData = $event;
    this.refreshCheckedStatus();
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    if (this.isCallFirstRequest) {
      this.isCallFirstRequest = false;
      return;
    }
    const {sortBy} = CommonUtil.onQueryParam(params);
    this.querySearch.sort = sortBy;
    this.onLoadMyCart();

  }

  // Default select box ant design
  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every((item) => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.listOfCurrentPageData.some((item) => this.setOfCheckedId.has(item.id)) && !this.checked;
    // console.log('refreshCheckedStatus, checked: ', this.checked, ', indeterminate: ', this.indeterminate);
  }

  // Load into money, total discount
  totalCart() {
    this.listOfData?.forEach((item) => {
      this.totalDiscount += item.discount;
      this.intoMoneyData += item.totalPrice;
    });
  }

  // Load cart list
  onLoadMyCart(noloading?) {
    this.myCartService.searchCart( noloading).subscribe((data: any) => {
      this.listOfData = data.body?.cartDTOs as Array<ICart>;
      this.listOfData.forEach((item) => {
        item.cartItemId = item.id;
      });
      // this.totalCart();
      this.numberProducts = data.body.totalCartItem;
    });
  }

  // increment quantity
  increment(cart: ICart) {
    cart.quantity += 1;
    this.onUpdate(cart);
  }

  // Decrement quantity
  decrement(cart: ICart): void {
    if (cart.quantity <= 1) return;
    cart.quantity -= 1;
    this.onUpdate(cart);
  }

  // Into money when the quantity is updated
  total(cart: ICart): number {
    return (this.intoMoneyData = cart.quantity * cart.productPrice);
  }

  // reurn into money
  intoMoney(): number {
    let money = 0;
    this.listOfData?.forEach((cart) => {
      money += cart.quantity * cart.productPrice;
    });
    return (this.intoMoneyData = money);
  }

  // Change quantity when input change
  changeSubtotal(cart: ICart) {
    // console.log('cart id: ', cart.id);
    // console.log('quantity: ', cart.quantity);

    if (cart.quantity <= 1) return;
    this.intoMoneyData = cart.quantity * cart.productPrice;
    this.onUpdate(cart);
  }

  // onSubmitForm(cart: ICart): void {
  //     this.onCreate(cart);
  // }
  onCreate(): void {

    this.$localStorage.clear(LOCAL_STORAGE.ORDER);
    this.$localStorage.store(LOCAL_STORAGE.ORDER, JSON.stringify(this.collectIds));
    // console.log('this is localStorage: ', this.collectIds);
    this.router.navigate(['/checkout']);

  }
  onUpdate(cart: ICart) {
    this.isChangeQuantity = true;
    this.myCartService.createOrUpdateCart(cart, true).subscribe((data: any) => {
      // console.log(data);
      this.isChangeQuantity = false;
      this.onLoadMyCart(true);
    });
  }
  // Delete items in cart
  onDeleteCart(cart?: ICart) {
    if (this.checked == true && this.indeterminate == false && cart) {
      this.collectIds.push(cart.id);
    }

    if (this.collectIds) {
      // console.log('onDeleteCart, cart: ', this.collectIds);
      this.myCartService.deleteCartByIds(this.collectIds, true).subscribe((data) => {
        this.onLoadMyCart(true);
        this.toastr.success(this.translate.instant('cart.message.successDelete'));
      });
    }
    this.checked = true;
    this.indeterminate = false;
  }

  // Delete items in cart
  onDeleteCartById() {
    // this.myCartService.deleteCartById(id, true).subscribe((data) => {
    //   this.onLoadMyCart(true);
    //   this.toastr.success(this.translate.instant('cart.message.successDelete'));
    // });
  }

  limitText(text: string, limit = 20) {
    if (text.length > limit) {
      return text.substring(0, limit) + '...';
    }
    return text;
  }
}
