import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyListOrderComponent } from './my-list-order.component';

describe('MyListOrderComponent', () => {
  let component: MyListOrderComponent;
  let fixture: ComponentFixture<MyListOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyListOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyListOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
