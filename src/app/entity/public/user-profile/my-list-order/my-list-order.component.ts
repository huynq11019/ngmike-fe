import {
  CATEGORY_TYPE,
  ORDER_STATUS_STRING,
  ORDER_TYPE,
  ORDER_STATUS_NUMBER,
  ORDER_STATUS,
} from './../../../../shared/contants/Constants';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DEFAULT_QUERY } from 'src/app/shared/contants/Constants';
import { MyOrderService } from 'src/app/shared/service/my-order.service';
import { IOrder } from '../../../../shared/models/order-customer';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-my-list-order',
  templateUrl: './my-list-order.component.html',
  styleUrls: ['./my-list-order.component.scss'],
})
export class MyListOrderComponent implements OnInit {
  constructor(
    private myOrderService: MyOrderService,
    private route: Router,
    private toastr: ToastrService,
    private translate: TranslateService
  ) {}
  WAITING_FOR_PAYMENT = ORDER_STATUS_NUMBER.WAITING_FOR_PAYMENT;
  WAIT_FOR_CONFIRM = ORDER_STATUS_NUMBER.WAIT_FOR_CONFIRM;
  CONFIRMED = ORDER_STATUS_NUMBER.CONFIRMED;
  SHIPPING = ORDER_STATUS_NUMBER.SHIPPING;
  PAID = ORDER_STATUS_NUMBER.PAID;
  CANCELED = ORDER_STATUS_NUMBER.CANCELED;
  SUCCESS = ORDER_STATUS_NUMBER.SUCCESS;
  selectedIndex = 0;
  inforUserOrder?: IOrder;
  listOfOrderDetails: IOrder[];
  orderId: string = '';
  totalOrder?: number = 0;
  deliveryCharges?: number = 0;
  numberProducts?: number = 0;
  searchValue = '';
  sizePage = 10;

  public indexOrderMOI = '1';
  public orderStatus = ORDER_TYPE;
  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
    sort: 'createDate,DESC',
  };
  onChangeStatus(tabsIndex: any) {
    console.log('sttus: ', tabsIndex.index , this.WAITING_FOR_PAYMENT);
    if (tabsIndex.index === this.WAITING_FOR_PAYMENT) {
      this.onLoadOrderStatus('WAITING_FOR_PAYMENT');
    } else if (tabsIndex.index === this.WAIT_FOR_CONFIRM) {
      this.onLoadOrderStatus('WAIT_FOR_CONFIRM');
    } else if (tabsIndex.index === this.CONFIRMED) {
      this.onLoadOrderStatus('CONFIRMED');
    } else if (tabsIndex.index === this.SHIPPING) {
      this.onLoadOrderStatus('SHIPPING');
    } else if (tabsIndex.index === this.SUCCESS) {
      this.onLoadOrderStatus('SUCCESS');
    } else if (tabsIndex.index === this.CANCELED) {
      this.onLoadOrderStatus('CANCELED');
    } else if (tabsIndex.index === this.PAID) {
      this.onLoadOrderStatus('PAID');
    } else {
      this.onLoadCustomerOrder();
    }
  }
  changeOrder(orderId?: string, type?: string) {
    if (orderId) {
      this.indexOrderMOI = orderId;
      // console.log(this.indexOrderMOI);
    }
  }
  onLoadCustomerOrder(param?: any) {
    this.myOrderService.getCurrentCustomerOrder(this.querySearch).subscribe((data) => {
      this.sizePage = data.body?.totalItem;
      this.listOfOrderDetails = data.body.orderList as IOrder[];
    });
  }
  onLoadOrderDetail(orderId: string) {
    this.myOrderService.getOrderDetailByOrderId(orderId).subscribe((data) => {
      // this.listOfOrderDetails = data.body.Order.orderDetails;
      // this.inforUserOrder = data.body.order as IOrder;
      // console.log(' order list: ', data.body);
    });
  }
  onOrderDetail(orderId: string) {
    // console.log("Order Id: ", orderId);
    this.route.navigate(['/order-details:'], {
      queryParams: {
        item: 1,
      },
    });
  }
  onLoadOrderStatus(status: string) {
    this.myOrderService.getOrderbyStatus(status).subscribe((data) => {
      // this.sizePage = data.body.totalItem;
      this.listOfOrderDetails = data.body?.orderList as IOrder[];
    });
  }
  ngOnInit(): void {
    this.onLoadCustomerOrder();
  }

  gotoDetail(id: string) {
    this.route.navigate(['/user/order-detail/' + id]);
  }
}
