import { ICustomer } from './../../../../shared/models/customer';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NzButtonSize } from 'ng-zorro-antd/button';
import { REGEX_CUSTOM } from 'src/app/shared/contants/Constants';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  size: NzButtonSize = 'large';
  validateForm: any;
  inforCustomer?: ICustomer;
  resolved(captchaResponse: string) {
    // console.log(`Resolved captcha with response: =>> ${captchaResponse}`);
    this.validateForm.get('captcha').setValue(captchaResponse);
  }
  constructor(private fb: FormBuilder, private toastr: ToastrService) {}

  ngOnInit(): void {
    this.initForm();
  }
  initForm() {
    this.validateForm = this.fb.group({
      password: [this.inforCustomer?.password || '', [Validators.required]],
      newPassword: [this.inforCustomer?.newPassword || '', [Validators.required]],
      repeatNewPassword: [this.inforCustomer?.repeatNewPassword || '', [Validators.required]],
    });
  }
  onSubmitChangePassword() {
    this.validateForm.markAllAsTouched();
    const changePass = { ...this.validateForm.value };
    // console.log('onSubmit: ', changePass.password);
    if (changePass.newPassword != changePass.repeatNewPassword) {
      this.toastr.error("New password is mismatched!");
      return;
    }
  }
  // random password generator
  generatePassword() {
    var length = 8,
      charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      retVal = '';
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }
}
