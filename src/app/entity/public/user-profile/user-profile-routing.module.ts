import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { MyListOrderComponent } from './my-list-order/my-list-order.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { MyAddressComponent } from './my-address/my-address.component';
import { GuardGuard } from '../../../core/guard/guard.guard';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ProductReturnsComponent } from './product-returns/product-returns.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'myprofile',
    pathMatch: 'full',
  },
  {
    path: 'myprofile',
    component: MyProfileComponent,
    canActivate: [GuardGuard],
    data: {
      authorities: ['CUSTOMER'],
      title: 'partner-contract.title',
    },
  },
  {
    path: 'changepassword',
    component: ChangePasswordComponent,
    canActivate: [GuardGuard],
    data: {
      authorities: ['CUSTOMER'],
      title: 'partner-contract.title',
    },
  },
  {
    path: 'my-list-order',
    component: MyListOrderComponent,
    canActivate: [GuardGuard],
    data: {
      authorities: ['CUSTOMER'],
      title: 'partner-contract.title',
    },
  },
  {
    path: 'order-detail/:orderId',
    component: OrderDetailComponent,
    canActivate: [GuardGuard],
    data: {
      authorities: ['CUSTOMER'],
      title: 'partner-contract.title',
    },
  },
  {
    path: 'myaddress',
    component: MyAddressComponent,
    canActivate: [GuardGuard],
    data: {
      authorities: ['CUSTOMER'],
      title: 'partner-contract.title',
    },
  },
  {
    path: 'order-detail',
    component: OrderDetailComponent,
    canActivate: [GuardGuard],
    data: {
      authorities: ['CUSTOMER'],
      title: 'partner-contract.title',
    },
  },
  {
    path: 'product-returns',
    component: ProductReturnsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserProfileRoutingModule {
}
