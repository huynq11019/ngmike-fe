import { FormBuilder, Validators } from '@angular/forms';
import { ToastrModule, ToastrService } from 'ngx-toastr';

import { CustomerService } from './../../../../shared/service/customer.service';
import { Component, OnInit } from '@angular/core';
import { ICustomer } from 'src/app/shared/models/customer';
import { NzUploadChangeParam, NzUploadFile, NzUploadXHRArgs } from 'ng-zorro-antd/upload';
import { FileUpload } from 'src/app/shared/models/file-upload';
import { FileUploadServiceService } from 'src/app/shared/service/file-upload-service.service';
import { getBase64 } from 'src/app/shared/untils/FunctionUntils';
import { TranslateService } from '@ngx-translate/core';
import { INFO_CUSTOM, REGEX_CUSTOM } from 'src/app/shared/contants/Constants';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss'],
})
export class MyProfileComponent implements OnInit {
  fileList = []; // file của ant design
  productFile?: File[] = []; // fiel khi upload
  fileDefault?: File[] = []; // file mặc định
  infoCustomer?: ICustomer;
  profileForm: any;
  previewImage: string | undefined = '';
  previewVisible = false;
  profileId: string;
  isVisible = false;
  constructor(
    private customerService: CustomerService,
    private formBuilder: FormBuilder,
    private fileUploadService: FileUploadServiceService,
    private translate: TranslateService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.onLoadProfile();
  }
  onLoadProfile() {
    this.customerService.getCurrentUserInfo().subscribe((data: any) => {
      this.infoCustomer = data.body?.customerDTO;
      this.previewImage = data.body?.customerDTO['avatar'];
      this.profileId = data.body?.customerDTO['id'];
      console.log('info customer: ', data.body?.customerDTO);
      this.initForm();
    });
  }
  private initForm(): void {
    // console.log('info customer: ', this.infoCustomer);
    this.profileForm = this.formBuilder.group({
      fullName: [this.infoCustomer?.fullName || '', [Validators.required]],
      email: [{
        value: this.infoCustomer?.email || '',
        disabled: true
      }, [Validators.required, Validators.pattern(REGEX_CUSTOM.EMAIL)]],
      phoneNumber: [
        this.infoCustomer?.phoneNumber || '',
        [Validators.required, Validators.pattern(REGEX_CUSTOM.PHONE_NUMBER)],
      ],
      dob: [this.infoCustomer?.dob || ''],
      gender: [this.infoCustomer?.gender || INFO_CUSTOM.GENDER_DEFAULT],
      status: [this.infoCustomer?.status || INFO_CUSTOM.STATUS_DEFAULT],
    });
  }
  onSubmitForm(): void {
    this.profileForm.markAllAsTouched();
    const profile = { ...this.profileForm.value };
    // console.log(this.profileForm);
    if (this.profileForm.invalid) {
      this.toastr.error(this.translate.instant('cart.message.invalidForm'));
      return;
    }
    // console.log('onSubmit, customerId: ', this.profileId);
    this.customerService.udpateUser(profile).subscribe((data: any) => {
      // console.log(data);
      this.toastr.success(this.translate.instant('home.profile.address.successUpdate'));
    });
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }
}
