import { ICart } from './../../../../shared/models/my-cart';
import { IOrder } from './../../../../shared/models/order-customer';
import { MyOrderService } from './../../../../shared/service/my-order.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ENTITY_TYPE, ORDER_STATUS_STRING } from 'src/app/shared/contants/Constants';
import { FileUploadService } from 'src/app/shared/service/file-upload.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss'],
})
export class OrderDetailComponent implements OnInit {
  CREATE = ORDER_STATUS_STRING.CREATE;
  WAITING_FOR_PAYMENT = ORDER_STATUS_STRING.WAITING_FOR_PAYMENT;
  WAITING_FOR_CONFIRM = ORDER_STATUS_STRING.WAITING_FOR_CONFIRM;
  CONFIRMED = ORDER_STATUS_STRING.CONFIRMED;
  SHIPPING = ORDER_STATUS_STRING.SHIPPING;
  PAID = ORDER_STATUS_STRING.PAID;
  CANCELED = ORDER_STATUS_STRING.CANCELED;

  constructor(
    private myOrderService: MyOrderService,
    private route: ActivatedRoute,
    public fileUpload: FileUploadService
  ) {
    this.route.params.subscribe((param) => {
      this.orderId = param['orderId'];
    });
  }
  public PRODUCT_TYPE = ENTITY_TYPE.PRODUCT;
  inforUserOrder?: IOrder[];
  listOfOrder?: IOrder;
  listOfOrderDetails: ICart[];
  orderId: string = '';
  intoMoneyData?: number = 0;
  deliveryCharges?: number = 0;
  numberProducts?: number = 0;

  totalCart() {
    this.listOfOrder?.orderDetails.forEach((item) => {
      let total = item.quantity * item.productPrice;
      this.intoMoneyData += total;
    });
  }
  onLoadOrderDetail() {
    this.myOrderService.getOrderDetailByOrderId(this.orderId).subscribe((data) => {
      this.listOfOrderDetails = data.body.order?.orderDetails;
      this.listOfOrder = data.body.order as IOrder;
      this.deliveryCharges = this.listOfOrder?.shippingCost;
      this.checkStep(this.listOfOrder.orderStatus);
      this.totalCart();
      console.log(' order detail: ', this.listOfOrder);
    });
  }
  ngOnInit() {
    this.onLoadOrderDetail();
  }
  index = 0;
  checkStep(orderStatus: string) {
    if (orderStatus === this.CREATE) {
      this.index = 0;
    } else if (orderStatus === this.WAITING_FOR_PAYMENT) {
      this.index = 1;
    } else if (orderStatus === this.WAITING_FOR_CONFIRM) {
      this.index = 2;
    } else if (orderStatus === this.CONFIRMED) {
      this.index = 3;
    } else if (orderStatus === this.SHIPPING) {
      this.index = 4;
    } else if (orderStatus === this.PAID) {
      this.index = 5;
    } else if (orderStatus === this.CANCELED) {
      this.index = 6;
    }else{
      this.index = 7;
    }
  }
  disable = false;
  onIndexChange(index: number): void {
    this.index = index;
  }
}
