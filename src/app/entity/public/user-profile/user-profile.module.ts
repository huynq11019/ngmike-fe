import { ChangePasswordComponent } from './change-password/change-password.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserProfileRoutingModule } from './user-profile-routing.module';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { MyAddressComponent } from './my-address/my-address.component';
import { MyListOrderComponent } from './my-list-order/my-list-order.component';
import { AvatarModule } from 'ngx-avatar';
import { TranslateModule } from '@ngx-translate/core';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { RecaptchaModule } from 'ng-recaptcha';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzButtonModule } from 'ng-zorro-antd/button';

import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { ProductReturnsComponent } from './product-returns/product-returns.component';
import {LayoutModule} from "../../../layout/layout.module";
import { NzTabsModule } from 'ng-zorro-antd/tabs';

@NgModule({
  declarations: [
    OrderDetailComponent,
    MyProfileComponent,
    MyAddressComponent,
    MyListOrderComponent,
    ChangePasswordComponent ,
    OrderDetailComponent,
    ProductReturnsComponent
  ],
  imports: [
    CommonModule,
    UserProfileRoutingModule,
    AvatarModule,
    TranslateModule,
    NzRadioModule,
    FormsModule,
    NzUploadModule,
    NzIconModule,
    NzSelectModule,
    ReactiveFormsModule,
    NzModalModule,
    NzFormModule,
    NzInputModule,
    RecaptchaModule,
    NzStepsModule,
    NzTimelineModule,
    NzBadgeModule,
    NzTableModule,
    LayoutModule,
    NzTabsModule,
    NzButtonModule
  ]
})
export class UserProfileModule { }
