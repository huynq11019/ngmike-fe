import { ICustomer } from './../../../../shared/models/customer';
import { User } from './../../../../shared/models/User';
import { LOCAL_STORAGE } from './../../../../shared/contants/location-session-cookie';
import { LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import { MyAddressService } from './../../../../shared/service/my-address.service';
import { IMyAddress } from './../../../../shared/models/my-address';
import { LocationPickerService } from './../../../../shared/service/location-picker.service';
import { Component, OnInit } from '@angular/core';
import { ILocation } from 'src/app/shared/models/location';
import { FormBuilder, Validators } from '@angular/forms';
import { DEFAULT_QUERY, REGEX_CUSTOM } from 'src/app/shared/contants/Constants';
import { TranslateService } from '@ngx-translate/core';
import { CustomerService } from 'src/app/shared/service/customer.service';
import { markIgnoreDiagnostics } from '@angular/compiler-cli/src/ngtsc/typecheck/src/comments';
interface addressCustom {
  address: string;
  provinceName: string;
  provinceId: string;
  districtId: string;
  districtName: string;
  wardId: string;
  wardName: string;
  street: string;
  fullAddress: string;
}
@Component({
  selector: 'app-my-address',
  templateUrl: './my-address.component.html',
  styleUrls: ['./my-address.component.scss'],
})
export class MyAddressComponent implements OnInit {
  addressCheck = false;
  checkData = true;
  isVisible = false;
  selectedValue: string;
  dataProvince: ILocation[] = [];
  addressForm: any;
  addressInfo?: IMyAddress;
  infoCustomer: ICustomer;
  listOfMyAddress?: IMyAddress[] = [];
  listOfProvinces?: [{ ProvinceName: string; ProvinceID: number }];
  listOfDistricts?: [{ DistrictName: string; DistrictID: number }];
  listOfWards?: [{ WardName: string; WardCode: number }];

  listOfProvincesGlobal?: [{ ProvinceName: string; ProvinceID: number }];
  listOfDistrictsGlobal?: [{ DistrictName: string; DistrictID: number }];
  listOfWardsGlobal?: [{ WardName: string; WardCode: number }];
  ///////////

  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
  };
  constructor(
    private locationPickerService: LocationPickerService,
    private formBuilder: FormBuilder,
    private addressService: MyAddressService,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private $localStorage: LocalStorageService,
    private customerService: CustomerService
  ) {
    this.onLoadProvinces();
    // this.onLoadDistricts();
    // this.onLoadWards();
  }

  ngOnInit(): void {
    //get info customer and get address by customer id

    // this.onLoadProvince();
    this.onLoadProfile();

    this.onLoadProvincesGHN();
    // this.onLoadDistrictsGHN();
    // this.onLoadWardsGHN();
  }
  private initForm(): void {
    this.addressForm = this.formBuilder.group({
      nameOfRecipient: [this.addressInfo?.nameOfRecipient || '', [Validators.required]],
      phoneNumber: [
        this.addressInfo?.phoneNumber || '',
        [Validators.required, Validators.pattern(REGEX_CUSTOM.PHONE_NUMBER)],
      ],
      addressDetail: [this.addressInfo?.addressDetail || '', [Validators.required]],
      province: [this.addressInfo?.province || '', [Validators.required]],
      provinceId: [this.addressInfo?.provinceId || '', [Validators.required]],
      district: [{ value: this.addressInfo?.district || '', disabled: true }, [Validators.required]],
      districtId: [this.addressInfo?.districtId || '', [Validators.required]],
      ward: [
        {
          value: this.addressInfo?.ward || 0,
          disabled: true,
        },
        [Validators.required],
      ],
      wardId: [this.addressInfo?.wardId || '', [Validators.required]],
      addressType: [this.addressInfo?.addressType || 'home'],
      id: [this.addressInfo?.id || ''],
    });
  }

  onLoadProfile() {
    this.customerService.getCurrentUserInfo().subscribe((data: any) => {
      this.infoCustomer = data?.body.customerDTO;
      this.getMyAddressByIds(this.infoCustomer.id);
    });
  }
  private getMyAddressByIds(userId: any) {
    this.addressService.getAddressByIds(userId).subscribe((data: any) => {
      this.listOfMyAddress = data?.body.addressDTOS;
      this.initForm();
    });
  }

  private getMyAddressById(user: any) {
    // console.log('getMyAddress: ', user.id);
    this.addressService.getAddressById(user.id, true).subscribe((data: any) => {
      this.addressInfo = data?.body.addressDTO;
      this.addressForm.patchValue({
        nameOfRecipient: this.addressInfo?.nameOfRecipient,
        phoneNumber: this.addressInfo?.phoneNumber,
        addressDetail: this.addressInfo?.addressDetail,
        province: this.addressInfo?.province,
        district: this.addressInfo?.district,
        ward: this.addressInfo?.ward,
        addressType: this.addressInfo?.addressType,
        id: this.addressInfo?.id,
      });
      if (!!this.addressInfo.provinceId) {
        // console.log(this.addressInfo.provinceId);
        this.onLoadDistrict(this.addressInfo?.provinceId);
      }
    });
  }
  showModal(address?: any): void {
    this.onLoadProvinces();
    // this.onLoadDistricts();
    // this.onLoadWards();
    this.isVisible = true;
    if (address) {
      this.addressCheck = true;
      this.getMyAddressById(address);
    }
    if (!address) {
      this.addressForm.reset();
    }
  }

  handleOk(): void {
    this.addressForm.markAllAsTouched();
    const address = { ...this.addressForm.value };

    console.log(address);
    // console.log(this.addressForm);

    if (this.addressForm.invalid) {
      this.toastr.error(this.translateService.instant('home.profile.address.invalidForm'));
      return;
    }
    if (address.id) {
      this.onUpdate(address);
    } else {
      // this.addressForm.get('id').setValue(Math.floor(Math.random() * 6 + 1) + Math.floor(Math.random() * 6 + 1));
      // const addressCreate = { ...this.addressForm.value };
      this.onCreate(address);
    }
  }

  onCreate(address: IMyAddress) {
    this.addressService.createAddress(address).subscribe((res) => {
      this.onLoadProfile();
      this.toastr.success(this.translateService.instant('home.profile.address.successCreate'));
    });
  }

  handleCancel(): void {
    this.addressForm.reset();
    this.isVisible = false;
  }

  onLoadDistrict(codeDistrict: number): void {
    console.log('codeDistrict: ', codeDistrict);
    const provinceMatch = this.listOfProvincesGlobal?.find((item) => item.ProvinceID === codeDistrict);
    console.log('ProvinceName: ', provinceMatch?.ProvinceName);
    this.addressForm.get('province').setValue(provinceMatch?.ProvinceName);
    if (!!codeDistrict && typeof codeDistrict === 'number') {
      this.locationPickerService.loadLocationByDistrictGHN(codeDistrict).subscribe((data: any) => {
        this.listOfDistricts = data?.body.data;

        console.log('onChange district: ', data.body.data);

        if (this.listOfProvincesGlobal.length > 0) {
          this.addressForm.controls.district.enable();
        }
      });
    }
  }

  onLoadWard(codeDistrict: number): void {
    console.log('codeDistrict: ', codeDistrict);

    const districtMatch = this.listOfDistricts?.find((item) => item.DistrictID === codeDistrict);
    console.log('DistrictName: ', districtMatch?.DistrictName);
    this.addressForm.get('district').setValue(districtMatch?.DistrictName);
    if (!!codeDistrict && typeof codeDistrict !== 'string') {
      this.locationPickerService.loadLocationByWardGHN(codeDistrict).subscribe((data: any) => {
        this.listOfWards = data?.body.data;
        if (this.listOfWards.length > 0) {
          this.addressForm.controls.ward.enable();
        }
      });
    }
  }

  onSelectWar(event: number): void {
    console.log('wardCode: ', event);
    const ward = this.listOfWards?.find((item) => item.WardCode === event);
    this.addressForm.get('ward').setValue(ward?.WardName);
    console.log('wardName: ', ward?.WardName);
  }

  //----------------------------------------------------------------
  onLoadProvinces() {
    this.locationPickerService.loadLocationByProvinceGHN().subscribe((data: any) => {
      this.listOfProvincesGlobal = data?.body.data;
    });
  }

  onLoadProvincesGHN() {
    this.locationPickerService.loadLocationByProvinceGHN().subscribe((data: any) => {
      this.listOfProvincesGlobal = data.body.data;
      // console.log('GHN provinces: ', data.body.data);
    });
  }
  onLoadDistrictsGHN() {
    this.locationPickerService.loadLocationByDistrictGHN(201).subscribe((data: any) => {
      this.listOfDistrictsGlobal = data.body.data;
      console.log('GHN District: ', data.body.data);
    });
  }
  onLoadWardsGHN() {
    this.locationPickerService.loadLocationByWardGHN(3440).subscribe((data: any) => {
      console.log('GHN Ward: ', data.body.data);
    });
  }

  onDelete(addressId: number) {
    this.addressService.deleteById(addressId).subscribe((data) => {
      this.toastr.success(this.translateService.instant('home.profile.address.succcessDelete'));
      this.onLoadProfile();
    });
  }
  onUpdate(myAddress: IMyAddress) {
    this.addressService.updateMyAddress(myAddress).subscribe((data) => {
      this.toastr.success(this.translateService.instant('home.profile.address.successUpdate'));
      this.onLoadProfile();
    });
  }
  onSetAddressDefault(addressId?: number) {
    this.addressService.setAddressDefault(addressId).subscribe((data) => {
      this.onLoadProfile();
    });
  }
}
