import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ICustomer } from 'src/app/shared/models/customer';
import { CustomerService } from 'src/app/shared/service/customer.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {
  constructor(private customerService: CustomerService) {}
  infoCustomer?: ICustomer;
  
  ngOnInit(): void {
    this.onLoadProfile();
  }

  onLoadProfile() {
    this.customerService.getCurrentUserInfo().subscribe((data: any) => {
      this.infoCustomer = data?.body?.customerDTO;
      // console.log('info user profile: ', data.body.customerDTO);
    });
  }
}
