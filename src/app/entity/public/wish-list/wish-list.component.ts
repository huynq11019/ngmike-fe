import { DateCenterService } from './../../../shared/service/data-service/date-center.service';
import { IAddToCart } from './../../../shared/models/addToCart';
import { WishlistService } from './../../../shared/service/wishlist.service';
import { Component, OnInit } from '@angular/core';
import { DEFAULT_QUERY, ENTITY_TYPE } from 'src/app/shared/contants/Constants';
import { Wishlist } from 'src/app/shared/models/wishlist';
import { FileUploadService } from '../../../shared/service/file-upload.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { MyCartService } from 'src/app/shared/service/my-cart.service';
import { ICart } from 'src/app/shared/models/my-cart';
import { CompareServiceService } from '../../../shared/service/compare-service.service';

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.scss'],
})
export class WishListComponent implements OnInit {
  public PRODUCT_TYPE = ENTITY_TYPE.PRODUCT;
  public totalItem = 0;
  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
  };
  listOfProfile?: Wishlist[] = [];
  stars: number[] = [1, 2, 3, 4, 5];
  selectedValue: number;
  addToCart?: IAddToCart;
  constructor(
    private wishlistService: WishlistService,
    public fileUpload: FileUploadService,
    private myCartService: MyCartService,
    private toastr: ToastrService,
    private translate: TranslateService,
    private compareService: CompareServiceService,
    private dataCenter : DateCenterService
  ) {}

  ngOnInit(): void {
    this.onLoadWishlist();
  }
  onLoadWishlist() {
    this.wishlistService.loadWishlist(this.querySearch).subscribe((data) => {
      this.listOfProfile = data?.body.favouritesDTOS as Array<Wishlist>;
      this.dataCenter.setTotalFavoriteItem(data.body.totalItem ? data.body.totalItem : 0);
      this.totalItem = data.body?.totalItem;
    });
  }
  pageChange(type: string, event: number): void {
    // console.log(type, event);
    if (type === 'page') {
      this.querySearch[type] = event - 1;
    } else {
      this.querySearch[type] = event;
    }
    this.onLoadWishlist();
  }
  countStar(star: number) {
    this.selectedValue = star;
    // console.log('Value of star', star);
  }

  onAddToCart(cart: ICart) {
    cart.quantity = 1;
    cart.optionProductId = null;
    this.myCartService.addToCart(cart).subscribe((data) => {
      this.toastr.success(this.translate.instant('cart.message.successCreate'));
    });
  }

  onDeleteFavorite(favoriteId: string) {
    this.wishlistService.deleteFavorite(favoriteId).subscribe((data) => {
      this.onLoadWishlist();
      this.toastr.success(this.translate.instant('cart.message.successDeleteFavorite'));
    });
  }
  //
  // addCompare(product: string) {
  //   this.compareService.addCompare(productId);
  // }
}
