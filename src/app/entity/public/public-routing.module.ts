import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { CompareProductsComponent } from './compare-products/compare-products.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ListProductComponent } from './list-product/list-product.component';
import { CartComponent } from './cart/cart.component';
import { WishListComponent } from './wish-list/wish-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ShoppingPageComponent } from './shopping-page/shopping-page.component';
import { PostComponent } from './post/post.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { ContactMeComponent } from './contact-me/contact-me.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { GuardGuard } from '../../core/guard/guard.guard';
import { MycartComponent } from './mycart/mycart.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'user',
    component: UserProfileComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./user-profile/user-profile.module').then((m) => m.UserProfileModule),
      },
    ],
  },
  {
    path: 'propage',
    component: ListProductComponent,
  },

  {
    path: 'cart',
    component: MycartComponent,
    data: {
      authorities: ['CUSTOMER'],
      title: 'cart.title',
    },
  },
  {
    path: 'wishlist',
    component: WishListComponent,
    canActivate: [GuardGuard],
    data: {
      authorities: ['CUSTOMER'],
      title: 'partner-contract.title',
    },
  },
  {
    path: 'product/:productid',
    component: ProductDetailComponent,
  },
  {
    path: 'product',
    component: ShoppingPageComponent,
  },
  {
    path: 'blog',
    component: PostComponent,
  },
  {
    path: 'about',
    component: AboutMeComponent,
  },
  {
    path: 'contact',
    component: ContactMeComponent,
  },
  {
    path: 'payment-success',
    component: PaymentSuccessComponent,
  },
  {
    path: 'checkout',
    component: CheckoutComponent,
    canActivate: [GuardGuard],
    data: {
      authorities: ['CUSTOMER'],
      title: 'checkout.title',
    },
  },
  {
    path: 'compare-products',
    component: CompareProductsComponent,
  },
  // {
  //   path: 'order-management',
  //   component: OrderCustomerComponent,
  //   canActivate: [GuardGuard],
  //   data: {
  //     authorities: ['CUSTOMER'],
  //     title: 'checkout.title',
  //   },
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule {
}
