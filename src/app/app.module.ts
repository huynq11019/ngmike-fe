import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// Import your AvatarModule
import { AvatarModule } from 'ngx-avatar';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N, vi_VN } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import vi from '@angular/common/locales/vi';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxWebstorageModule } from 'ngx-webstorage';
// import {DashboardOutline, FormOutline, MenuFoldOutline, MenuUnfoldOutline} from '@ant-design/icons-angular/icons';
import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';
import { IconDefinition } from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';
import { PublicLayoutComponent } from './layout/app-layout/public-layout/public-layout.component';
import { MainLayoutComponent } from './layout/app-layout/main-layout/main-layout.component';
import { LayoutModule } from './layout/layout.module';
import { ConfirmRequestComponent } from './shared/component/confirm-request/confirm-request.component';
import { NzPipesModule } from 'ng-zorro-antd/pipes';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { ErrorInterceptor } from './core/inteceptor/error.interceptor';
import { JwtInterceptor } from './core/inteceptor/jwt.interceptor';
import { LoadingInterceptor } from './core/inteceptor/loading.interceptor';
import { RecaptchaModule } from 'ng-recaptcha';
import { ModalModule } from 'ngb-modal';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AgmCoreModule } from '@agm/core';
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
// import {MomentModule} from 'ngx-moment';
registerLocaleData(vi);
// const icons = [MenuFoldOutline, MenuUnfoldOutline, DashboardOutline, FormOutline];
const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map((key) => antDesignIcons[key]);
// const quillToolbar = [
//     ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
//     ['blockquote', 'code-block'],
//     [{ 'header': 1 }, { 'header': 2 }],               // custom button values
//     [{ 'list': 'ordered'}, { 'list': 'bullet' }],
//     [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
//     [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
//     [{ 'direction': 'rtl' }],                         // text direction
//     [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
//     [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
//     [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
//     [{ 'font': [] }],
//     [{ 'align': [] }],
//     ['clean'],                                         // remove formatting button
//     ['link', 'image', 'video']                         // link and image, video
//   ];
@NgModule({
  declarations: [AppComponent, PublicLayoutComponent, MainLayoutComponent, ConfirmRequestComponent],
    imports: [
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        ToastrModule.forRoot({
            timeOut: 10000,
            positionClass: 'toast-top-right',
            preventDuplicates: false,
        }),
        BrowserAnimationsModule,
        NgxSpinnerModule,
        HttpClientModule,
        NzIconModule,
        NzLayoutModule,
        NzMenuModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient],
            },
            useDefaultLang: true,
        }),
        NgxWebstorageModule.forRoot({prefix: 'Playmore-portal', separator: '-'}),
        LayoutModule,
        NzPipesModule,
        NzPaginationModule,
        ReactiveFormsModule,
        AngularFireStorageModule,
        AvatarModule,
        // QuillModule.forRoot({
        //   modules: {
        //     syntax: true,
        //     toolbar: quillToolbar
        //   }
        // }),
        AngularFireModule.initializeApp(environment.firebaseConfig, 'cloud'),
        RecaptchaModule,
        ModalModule,
        NgbModule,
        NzToolTipModule,
        // AgmCoreModule.forRoot({
        //   apiKey: 'AIzaSyDKSDZnbaGIvoR1CxfDQA0PHT6jDki4s2Q',
        // }),
    ],
  providers: [
    { provide: NZ_I18N, useValue: vi_VN },
    { provide: NZ_ICONS, useValue: icons },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true },
    {
      provide: 'CanActivateGuard',
      useValue: () => {
        return true;
      },
    },
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}

// tslint:disable-next-line:typedef
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, `./assets/i18n/`, '.json');
}
