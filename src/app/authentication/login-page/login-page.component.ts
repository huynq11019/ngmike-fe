import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../shared/service/auth.service';

const re = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  public isLoginAd = window.location.href.includes('login/admin');

  constructor(private fb: FormBuilder,
              private toasrtService: ToastrService,
              private spiner: NgxSpinnerService,
              private route: Router,
              private router: ActivatedRoute,
              private translateService: TranslateService,
              private authService: AuthService) {
  }

// , Validators.pattern('/^[a-zA-Z0-9]+([a-zA-Z0-9](_|-| )[a-zA-Z0-9])*[a-zA-Z0-9]+$/')
  public form: FormGroup = this.fb.group({
    userName: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(60), Validators.email]],
    passWord: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(60)]],
    rm: [false]
  });

  ngOnInit(): void {
    this.router.queryParams.subscribe(
      (params) => {
        console.log(params);
        if (params.tokendeep) {
          this.authService.oauthLogin(params.tokendeep, false);
          this.route.navigate(['/home']);

        }
      }
    );
    this.checklogin();
  }

  // chekc usser login
  private checklogin() {
    this.authService.getCurrentUser();

    if (this.isLoginAd) {
      if (this.authService.hasAnyAuthority(['ROLE_ADMIN'])) {
        this.route.navigate(['/admin']);
      }
    } else {
      if (this.authService.authenticated && this.authService.hasAnyAuthority(['"CUSTOMER"'])) {
        this.route.navigate(['/home']);
      }
    }
  }

  public onLogin() {
    if (!this.form.valid) {
      this.toasrtService.error('yêu cầu nhập thông tin tài khaorn mật khẩu');
      return;
    } else {
      console.log("login với tài khoản nào", this.isLoginAd);
      // await this.spiner.show();
      const loginType = this.isLoginAd ? 'admin_' : 'user_';
      this.authService.doLogin(loginType + this.form.get('userName').value, this.form.get('passWord').value, this.form.get('rm').value)
      .subscribe(res => {

          console.log(res);
          if (!!res) {
            if (this.isLoginAd) {
              this.route.navigate(['/admin']);
            } else {
              this.route.navigate(['/home']);
            }
            this.toasrtService.success("đăng nhập thành công");
          }
        },
        error => {
          console.error(error);
        });
      // await this.spiner.hide();

    }

    // alert(JSON.stringify(this.form.value));

  }

  imageToBase64(url) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = () => {
        const reader = new FileReader();
        reader.onloadend = () => {
          resolve(reader.result);
        };
        reader.readAsDataURL(xhr.response);
      };
      xhr.open('GET', url);
      xhr.send();
    });
  }
}
