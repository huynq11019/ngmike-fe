import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Page404Component } from './page404/page404.component';
import { RegisterComponent } from './register/register.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { Page403Component } from './page403/page403.component';

const routes: Routes = [
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'login',
    component: LoginPageComponent,
  },
  {
    path: 'login/admin',
    component: LoginPageComponent,
  },
  {
    path: 'forgot-password',
    component: ForgotpasswordComponent,
  },
  {
    path: 'page403',
    component: Page403Component,
  },
  {
    path: 'page404',
    component: Page404Component,
  },
  { path: '', pathMatch: 'full', redirectTo: 'page404' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule {
}
