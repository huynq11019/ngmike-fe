import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { Page404Component } from './page404/page404.component';
import { RegisterComponent } from './register/register.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { RecaptchaModule } from 'ng-recaptcha';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { TranslateModule } from '@ngx-translate/core';
import { Page403Component } from './page403/page403.component';
import {NzResultModule} from "ng-zorro-antd/result";

@NgModule({
  declarations: [
    Page404Component,
    LoginPageComponent,
    RegisterComponent,
    ForgotpasswordComponent,
    Page403Component,
  ],
    imports: [
        CommonModule,
        AuthenticationRoutingModule,
        ReactiveFormsModule,
        NzFormModule,
        NzInputModule,
        NzSelectModule,
        NzCheckboxModule,
        RecaptchaModule,
        NzButtonModule,
        TranslateModule,
        NzResultModule
    ]
})
export class AuthenticationModule {
}
