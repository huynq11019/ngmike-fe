import { REGEX_CUSTOM } from './../../shared/contants/Constants';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NzButtonSize } from 'ng-zorro-antd/button';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss'],
})
export class ForgotpasswordComponent implements OnInit {
  size: NzButtonSize = 'large';
  validateForm: any;
  email?: string;
  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: =>> ${captchaResponse}`);
    this.validateForm.get('captcha').setValue(captchaResponse);
  }
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [this.email || '', [Validators.pattern(REGEX_CUSTOM.EMAIL), Validators.required]],
      captcha: [null, [Validators.required]],
      agree: [false],
    });
  }
  // random password generator
  generatePassword() {
    var length = 8,
      charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      retVal = '';
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }
}
