import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import { LoadingService } from '../../shared/service/loading.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { CustomerService } from '../../shared/service/customer.service';
// import { ICustomer } from '../../shared/models/Customer';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { ICustomer } from '../../shared/models/customer';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  validateForm!: FormGroup;
  captchaTooltipIcon: NzFormTooltipIcon = {
    type: 'info-circle',
    theme: 'twotone'
  };
  customerInfor?: ICustomer = {};

  constructor(private fb: FormBuilder,
              private spiner: LoadingService,
              private toastr: ToastrService,
              private translate: TranslateService,
              private customerService: CustomerService,
              private route: Router,
              private router: ActivatedRoute) {
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if (this.validateForm.valid) {
      console.log(this.validateForm.value);
      this.customerService.register(this.validateForm.value).subscribe(
        res => {
          console.log(res);
          this.toastr.success("Đăng ký thành công");
            this.route.navigate(['/login']);
        }, error => {
          this.toastr.error(error.error?.message);
        }
      );
    } else {
      this.toastr.warning("Data in valid");
    }
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() => this.validateForm.controls.checkPassword.updateValueAndValidity());
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return {required: true};
    } else if (control.value !== this.validateForm.controls.passwordHash.value) {
      return {confirm: true, error: true};
    }
    return {};
  };

  getCaptcha(e: MouseEvent): void {
    e.preventDefault();
  }

  ngOnInit(): void {
    this.checklogin();
    this.validateForm = this.fb.group({
      email: [this.customerInfor?.email || '', [Validators.required, Validators.email]],
      passwordHash: [null, [Validators.required]],
      checkPassword: [null, [Validators.required, this.confirmationValidator]],
      fullName: [null, [Validators.required]],
      phoneNumberPrefix: ['+84'],
      phoneNumber: [null, [Validators.required, Validators.pattern('^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$')]],
      captcha: [null, [Validators.required]],
      agree: [false],
    });
  }
  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: =>> ${captchaResponse}`);
    this.validateForm.get('captcha').setValue(captchaResponse);
  }

  private checklogin() {
    this.router.queryParams.subscribe(params => {
      console.log(params);
      if (!!params.err && params.err === 'usernotfound') {
        this.customerInfor.email = params?.email;
        this.toastr.error('Bạn chưa đăng ký tài khoản');
      }
    });
    // this.route.navigate(['/login']);
  }
}
