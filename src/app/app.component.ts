import { AfterViewChecked, Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewChecked {
  ngAfterViewChecked(): void {}

  public isCollapsed = false;

  constructor(public router: Router, private translate: TranslateService, private $localStorage: LocalStorageService) {
    const lang = this.$localStorage.retrieve('lang') || 'vi';

    this.translate.setDefaultLang(lang);
    this.translate.use(lang);
  }

  lat = 51.678418;
  lng = 7.809007;
}
