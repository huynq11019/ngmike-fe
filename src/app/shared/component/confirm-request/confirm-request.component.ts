import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-confirm-request',
  templateUrl: './confirm-request.component.html',
  styleUrls: ['./confirm-request.component.scss']
})
export class ConfirmRequestComponent implements OnInit {
  @Input() title: string;
  @Input() body: string;
  @Input() confirmButton: string;
  @Input() cancel: string;
  closeButton: string;

  constructor(
    public activeModal: NgbActiveModal,
    protected translateService: TranslateService,
  ) {
    this.closeButton = translateService.instant('common.action.close');
  }
  ngOnInit(): void {
  }

}
