import { IMyAddress } from './my-address';
export interface ICustomer {
  id?: string;
  address?: string;
  avatar?: string;
  dob?: string;
  email?: string;
  fullName?: string;
  gender?: number;
  role?: number;
  userName?: string;
  authorities?: Array<string>;
  capcha?: string;
  phoneNumber?: number;
  status?: number;
  rewardPoint?: number;
  addressDTOS?: IMyAddress[];
  password?: string;
  newPassword?: string;
  repeatNewPassword?: string;
}
