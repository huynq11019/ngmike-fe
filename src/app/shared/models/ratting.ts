export interface IRatting {
  id: string;
  orderId: string;
  point: number;
  productId: string;
  rateContent: string;
  status: number;
  userRating: string;
  createDate: string;
}
