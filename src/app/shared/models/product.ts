import { IfileUpload } from './file-upload';
import { BaseModels } from './baseModels';
import { IOptionProduct } from './OptionProduct';

export interface Iproduct extends BaseModels {
  description?: string;
  discount?: number;
  id?: string;
  isOrder?: boolean;
  metaData?: string;
  price?: number;
  productName?: string;
  subCateName?: string;
  supplierId?: number;
  userId?: number;
  quantity?: number;
  serial?: number;
  serialName?: string;
  categoryId?: string;
  categoryName?: string;
  estimateTime?: number;
  optionProducts?: IOptionProduct[];
  files?: IfileUpload[];
  ratingPoint?: number;
  avgPrice?: number;
  minPrice?: number;
  maxPrice?: number;
  customerId?: string;
  productId?: string;
  maxDiscount?: number;
}

export class Product {
  description?: string;
  discount?: number;
  id?: string;
  isOrder?: boolean;
  metaData?: string;
  price?: number;
  productName?: string;
  subCateName?: string;
  supplierId?: number;
  userId?: number;
  quantity?: number;
  serial?: number;
  serialName?: string;
  categoryId?: string;
  categoryName?: string;
  estimateTime?: number;
  optionProducts?: IOptionProduct[];
  files?: IfileUpload[];
  createDate?: string;
  ratingPoint?: number;
  minPrice?: number;
  maxPrice?: number;
  maxDiscount?: number;


  constructor(product?: Iproduct) {
    if (product) {
      this.description = product.description;
      this.discount = product.discount;
      this.id = product.id;
      this.isOrder = product.isOrder;
      this.metaData = product.metaData;
      this.price = product.price;
      this.productName = product.productName;
      this.subCateName = product.subCateName;
      this.supplierId = product.supplierId;
      this.userId = product.userId;
      this.quantity = product.quantity;
      this.serial = product.serial;
      this.serialName = product.serialName;
      this.categoryId = product.categoryId;
      this.categoryName = product.categoryName;
      this.estimateTime = product.estimateTime;
      this.optionProducts = product.optionProducts;
      this.files = product.files;
      this.createDate = product.createDate;
      this.ratingPoint = product.ratingPoint;
      this.minPrice = product.minPrice;
      this.maxPrice = product.maxPrice;
      this.maxDiscount = product.maxDiscount;
    }
  }
}
