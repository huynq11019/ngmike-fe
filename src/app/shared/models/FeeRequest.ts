export interface IFeeRequest {
  from_district_id?: 1542;
  to_district_id?: number;
  to_ward_code?: string;
  service_id?: 53320;
  height?: number;
  length?: number;
  width?: number;
  weight: number;
  service_type_id: null;
  insurance_value?: number;
}

// "from_district_id":1454,
//   "service_id":53320,
//   "service_type_id":null,
//   "to_district_id":1452,
//   "to_ward_code":"21012",
//   "height":50,
//   "length":20,
//   "weight":200,
//   "width":20,
//   "insurance_value":10000,
//   "coupon": null
export interface feeReqesponse {
  code: number;
  message: string;
  data: {
    total: number;
    service_fee: number
    insurance_fee: number
    pick_station_fee: number
    coupon_value: number
    r2s_fee: number
  };
}
