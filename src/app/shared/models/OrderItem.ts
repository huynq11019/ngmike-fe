export interface IOrderItem {
  id?: string;
  productId?: string;
  cartItemId?: string;
  productName?: string;
  optionId?: string;
  optionName?: string;
  quantity?: number; // số lượng sẳn phẩm mua
  status?: number;
  discount?: number;
  price?: number;
  orderId: string;
  productPrice?: number;
}
export class OrderItem implements IOrderItem {
  cartItemId: string;
  discount: number;
  id: string;
  optionId: string;
  orderId: string;
  price: number;
  optionName?: string;
  productId: string;
  productName: string;
  quantity: number;
  status: number;
  constructor(data?: IOrderItem) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>data)[property];
        }
      }
    }
  }
}
