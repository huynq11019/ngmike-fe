import { BaseModels } from './baseModels';

export interface Iattibute extends BaseModels{
    id?:string;
    categoryId?:string;
    name?:string;
    type?:string;
    value?:string;
}
