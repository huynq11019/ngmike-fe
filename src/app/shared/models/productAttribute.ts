import { BaseModels } from './baseModels';

export interface IproductAttibute extends BaseModels{
  id?:string;
  attributeId?:string;
  attributeName?:string;
  productId?:string;
  value?:string;
}
