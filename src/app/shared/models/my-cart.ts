export interface ICart {
  optionProductId?: string;
  customerId?: string;
  productId?: string;
  productName?: string;
  productPrice?: number;
  quantity?: number;
  discount?: number;
  cartItemId?: string;
  id?: string;
  totalPrice?: number;
  optionProductName?: string;
  optionName?:string;

}
