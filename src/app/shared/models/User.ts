export class User {
  address?: string;
  avatar?: string;
  dob?: string;
  email?: string;
  fullName?: string;
  gender?: number;
  userId?: string;
  role?: number;
  userName?: string; // lưu địa chỉ email
  authorities?: Array<string>;
  sub: any;
  haskey: string;

}
