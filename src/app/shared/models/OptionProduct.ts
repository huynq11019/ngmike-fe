
export interface IOptionProduct {
  id?: string;
  name?: string;
  price?: any;
  stockQuantity?: number;
  productId?: string;
  discount?: number;
  image?: string;
  description?: string;
}

export class OptionProduct implements IOptionProduct {
  constructor(
    public id?: string,
    public name?: string,
    public price?: number,
    public stockQuantity?: number,
    public productId?: string,
    public discount?: number,
    public image?: string,
    public description?: string
  ) {
    this.id = id ? id : null;
    this.name = name ? name : null;
    this.price = price ? price : null;
    this.stockQuantity = stockQuantity ? stockQuantity : null;
    this.productId = productId ? productId : null;
    this.discount = discount ? discount : null;
    this.image = image ? image : null;
    this.description = description ? description : null;
  }
}
