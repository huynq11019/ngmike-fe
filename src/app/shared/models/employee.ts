export interface IEmployee {
  id?: string;
  address?: string;
  avatar?: string;
  dob?: string;
  email?: string;
  fullName?: string;
  gender?: number;
  role?: number;
  userName?: string;
  authorities?: Array<string>;
  status?: number;
  password?: string;
  phoneNumber?: string;
}
