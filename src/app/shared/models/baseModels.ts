export interface BaseModels {

  createDate?: string;

  createBy?: number;

  lastUpdate?: string;

  updateBy?: number;
// status -1 là nháp, 0 xóa, 1 active
  status ?: number;
}
