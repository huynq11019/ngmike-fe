export interface IMyAddress {
  addressDetail?: string;
  addressType?: string;
  customerId?: string;
  province: string;
  provinceId?: number;
  district: string;
  districtId?: number;
  ward: string;
  wardId?: string;
  id?: number;
  latitude?: string;
  nameOfRecipient?: string;
  status?: number;
  phoneNumber?: string;
  fullName?: string;
  isDefault?: boolean;
}
