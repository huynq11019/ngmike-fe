import { BaseModels } from './baseModels';

export interface IattrubuteByCate extends BaseModels{
    id:string;
    categoryId:string;
    productId:string;
    name:string;
    type:string;
    value:string;
}
