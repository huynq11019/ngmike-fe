export interface IPaymentRequest {
  id?: string;
  amount?: number;
  apptime?: number; // date miniseconds
  appuser?: 'demo';
  bankcode?: '38';
  currency?: 'VND';
  description?: string;
  embeddata?: string; // data call back bắt buộc
  item?: string; // item của đơnhàng do ứng dụng tự định nghĩa
  mac?: "20a8e0c1c64e9922f23fce3f72d8dfab365eaaf6422f33e6450c027262abed97"; // tự tính
  phone?: string;
  orderid?: string;
  zlppaymentid?: string;
}
