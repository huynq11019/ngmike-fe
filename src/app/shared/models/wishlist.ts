export interface Wishlist {
  id?: string;
  productName?: string;
  numberFavourites: number;
  price?: number;
  productId?: string;
  productThumnail?: string;
  ratingPoint?: number;
  optionProductId?: string;
  avatar?: string;
  customerId?: string;
}
