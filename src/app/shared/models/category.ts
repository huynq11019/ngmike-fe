import { BaseModels } from './baseModels';

export interface ICategory extends BaseModels{
  id?:string;
  categoryName?:string;
  icon?:string;
  description?:string;
  sortOder?:number;
  child?: ICategory[];
  countProduct?: number;
  parentCategory?: string;
}
