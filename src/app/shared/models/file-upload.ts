export interface IfileUpload {
  createdBy?: number;
  createdDate?: string;
  file?: string;
  filePath?: string;
  dowloadUrl?: string;
  fileSize?: number;
  fileType?: string;
  id?: string;
  originalName?: string;
  ownerId?: string;
  ownerType?: string;
  status?: number;
  updateBy?: number;
  updatedDate?: string;

  viewUrl?: string;
  downloadUrl?: string;
}

export class FileUpload implements IfileUpload {
  constructor(
    public createdBy?: number,
    public createdDate?: string,
    public file?: string,
    public filePath?: string,
    public fileSize?: number,
    public fileType?: string,
    public id?: string,
    public dowloadUrl?: string,
    public originalName?: string,
    public ownerId?: string,
    public ownerType?: string,
    public status?: number,
    public updateBy?: number,
    public updatedDate?: string,
  ) {
    this.createdBy = createdBy;
    this.createdDate = createdDate;
    this.file = file;
    this.filePath = filePath;
    this.fileSize = fileSize;
    this.fileType = fileType;
    this.dowloadUrl = dowloadUrl;
    this.id = id;
    this.originalName = originalName;
    this.ownerId = ownerId;
    this.ownerType = ownerType;
    this.status = status;
    this.updateBy = updateBy;
    this.updatedDate = updatedDate;
  }
}
