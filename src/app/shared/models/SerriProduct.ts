export interface IserriProduct {
  id?: number,
  serriName: string,
  createDate?: string,
  createBy?: string,
  lastUpdate?: string,
  updateBy?: string,
  status?: number,
  serriDescroption?: string,
}
