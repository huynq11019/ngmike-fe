import { ICart } from 'src/app/shared/models/my-cart';
import { IOrderItem } from './OrderItem';
export interface IOrder {
  note: string;
  optionId: string;
  productId: string;
  totalPrice: number;
  totalQuantity: number;
  userId: string;
  userName: string;
  captchaCode: string;
  address: string;
  phoneNumber: string;
  receiver: string;
  orderStatus: string;
  paymentMethod: string;
  customerId: string;
  orderDetails: ICart[];
  id?: string;
  shippingCost: number;
  items: IOrderItem[];
  paymentId?: string;
  discount?: number;
  signature?: string;
  createDate?: string;
  createBy?: string;
  lastUpdate?: string;
  updateBy?: string;
  status?: number;
  addressId?: number;
}
