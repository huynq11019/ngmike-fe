import { Directive, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { fromEvent, Subject, timer } from 'rxjs';
import { debounce, distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[appKeyUptime]'
})
export class KeyUptimeDirective {
  @Input() delayTime = 400;
  @Output() keyupDelay = new EventEmitter<Event>();

  private destroy = new Subject<void>();

  constructor(private elementRef: ElementRef<HTMLInputElement>) {
  }
  ngOnInit() {
    fromEvent(this.elementRef.nativeElement, "keyup")
    .pipe(
      debounce(() => timer(this.delayTime)),
      distinctUntilChanged(null, (event: Event) => (event.target as HTMLInputElement).value),
      takeUntil(this.destroy)
    )
    .subscribe((e) => this.keyupDelay.emit(e));
  }

  ngOnDestroy() {
    this.destroy.next();
  }
}
