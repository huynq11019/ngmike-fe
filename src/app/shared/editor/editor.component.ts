import {AfterViewChecked, AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as ClassicEditor from 'src/app/shared/lib/ckeditor5';
import { AuthService } from '../service/auth.service';
import { API_STORAGE } from '../contants/Constants';
// import {API_STORAGE} from "../../constants/base.constant";
// import {AuthService} from '../../service/auth.service';

// https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/basic-api.html

@Component({
  selector: 'dk-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['editor.component.scss']
})
export class EditorComponent implements OnInit, AfterViewInit {

  public Editor = ClassicEditor;
  public editor: any;
  TOKEN = '';
  fileName = '';
  isLoadCKeditor = false;
  @Input() placeholder = 'Nội dung';
  @Input() value?: string = '';
  @Output() data: EventEmitter<any> = new EventEmitter();

  constructor(
    private authService: AuthService,
  ) {
    this.TOKEN = this.authService.getToken();
  }

  ngOnInit(): void {

  }

   ngAfterViewInit(): void {
      this.afterView();
     // setTimeout(() => {
     //   this.editor.setData(this.value);
     // }, 1000);
   }

  // async ngAfterViewChecked(): Promise<void> {
  //   // this.editor.setData('<p>Some text.</p>');
  // }

  afterView(): void {
    const self = this;
    if (document.querySelector("#editor") && !this.isLoadCKeditor) {
      this.isLoadCKeditor = true;
      ClassicEditor.builtinPlugins.map((plugin: any) => {
        // console.log(plugin.pluginName)
      });
      ClassicEditor.create(document.querySelector("#editor"), {
        toolbar: {
          items: [
            "heading",
            "bold",
            "italic",
            "underline",
            "alignment",
            "fontFamily",
            "fontColor",
            "fontSize",
            "highlight",
            "link",
            "strikethrough",
            "bulletedList",
            "numberedList",
            "blockQuote",
            "outdent",
            "indent",
            "code",
            "insertTable",
            // "paragraph",
            // "uploadImage",
          ],
        },
        image: {
          toolbar: ["imageStyle:full", "imageStyle:side"],
        },
        placeholder: this.placeholder,
        extraPlugins: [MyCustomUploadAdapterPlugin]
      })
        .then((editor: any) => {
          this.editor = editor;
          editor.model.document.on("change:data", (evt: any, data: any) => {
            this.data.emit(editor.getData());
            // this.editor.setData(this.value);
          });
          this.editor.setData(this.value);
        })
        .catch((error: any) => {
          console.log("There was a problem initializing the editor.", error);
        });
    }

    function MyCustomUploadAdapterPlugin(editor: any) {
      editor.plugins.get('FileRepository').createUploadAdapter = (loader: any) => {
        // Configure the URL to the upload script in your back-end here!
        return new MyUploadAdapter(loader);
      };

      class MyUploadAdapter {
        loader: any;
        xhr = new XMLHttpRequest();

        constructor(loader: any) {
          // The file loader instance to use during the upload.
          this.loader = loader;
        }

        upload() {
          return this.loader.file
            .then((file: any) => new Promise((resolve, reject) => {
              this._initRequest();
              this._initListeners(resolve, reject, file);
              this._sendRequest(file);
              self.fileName = file.name;
            }));
        }

        abort() {
          if (this.xhr) {
            this.xhr.abort();
          }
        }

        _initRequest() {
          const xhr = this.xhr = new XMLHttpRequest();
          // xhr.open('POST', API_STORAGE + '/file/...', true);
          xhr.open('POST', API_STORAGE , true);
          xhr.responseType = 'json';
          xhr.setRequestHeader('Authorization', 'Bearer ' + self.TOKEN);
        }

        _initListeners(resolve: any, reject: any, file: any) {
          const xhr = this.xhr;
          const loader = this.loader;
          const genericErrorText = `Couldn't upload file: ${file.name}.`;

          xhr.addEventListener('error', () => reject(genericErrorText));
          xhr.addEventListener('abort', () => reject());
          xhr.addEventListener('load', () => {
            const response = xhr.response;
            if (!response || response.error) {
              return reject(response && response.error ? response.error.message : genericErrorText);
            }
            resolve({
              default: response.url
            })
            ;
          });
          if (xhr.upload) {
            xhr.upload.addEventListener('progress', evt => {
              if (evt.lengthComputable) {
                loader.uploadTotal = evt.total;
                loader.uploaded = evt.loaded;
              }
            });
          }
        }

        _sendRequest(file: any) {
          // Prepare the form data.
          const data = new FormData();

          data.append('file', file);
          this.xhr.send(data);
        }

      }
    }

  }

}
