import { FormGroup } from "@angular/forms";
import * as _ from "lodash";
import { NzTableQueryParams } from 'ng-zorro-antd/table';

export default class CommonUtil {
  /**
   * Xóa tham số là null || undefined trong Object
   *
   * @param obj
   * @returns
   */
  static cleanObjectParams = (obj?: any): any => {
    if (obj) {
      Object.keys(obj).forEach((k) => (obj[k] === null || obj[k] === undefined || obj[k] === '') && delete obj[k]);
    }
    return obj;
  }

  /**
   * Convert object to form data | sẽ update thêm
   *
   * @param object
   * @returns
   */
  static convertObjectToFormData = (object): any => {
    const formData = new FormData();
    const data = CommonUtil.cleanObjectParams(object);
    if (data) {
      Object.keys(data).forEach((key) => {
        if (key !== "files" && key !== "optionProducts") {
          formData.append(key, data[key])
        }
      });
    }
    return formData;
  }

  static converseObjectToFormData = (object): any => {
    const formData = new FormData();
    const data = CommonUtil.cleanObjectParams(object);
    if (data) {
      Object.keys(data).forEach((key) => formData.append(key, data[key]));
    }
    return formData;
  }

  /**
   * Set touched event cho control trong form, dùng khi validate form để hiển thị message lỗi
   *
   * @param form
   */
  static markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach((control) => {
      control.markAsTouched();
      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  static removeComma = (data): any => {
    Object.keys(data).map((k) => (data[k] = typeof data[k] === "string" ? data[k].replace(/,/gi, "") : data[k]));
    return data;
  }

  /**
   * Trả về list năm gồm năm hiện tại và 5 năm gần nhất trong quá khứ
   *
   * @returns number[]
   */
  static fiveYearsAgo = (): number[] => {
    const years = [];
    const currentYear = new Date().getFullYear() - 5;
    for (let i = 0; i <= 5; i++) {
      years.push(currentYear + i);
    }
    return years;
  }

  /**
   * Trả về list năm gồm năm hiện tại và 5 năm gần nhất trong quá khứ và 4 năm gần nhất trong tương lai
   *
   * @returns number[]
   */
  static listYearInAboutFive = (): number[] => {
    const years = [];
    const currentYear = new Date().getFullYear() - 5;
    for (let i = 0; i < 10; i++) {
      years.push(currentYear + i);
    }
    return years;
  }

  /* check t12 chuyen nam moi*/
  static setMonthYear = () => {
    let fiscalYear = new Date().getFullYear();
    let period = (new Date().getMonth() + 2);
    if ((new Date().getMonth() + 1) === 12) {
      fiscalYear = fiscalYear + 1;
      period = 1;
    }
    return {
      fiscalYear: fiscalYear.toString(),
      period: period.toString()
    };
  }

  /* trả về nếu char không thuộc kí tự đặc biệt, các ký tự chấp nhận: a-z, A-Z,0-9, dấu chấm và phẩy và không có dấu cách*/
  static omitSpecialChar(event)
  {
    let k;
    k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return( ((k > 64 && k < 91) || (k > 96 && k < 123) || k === 8 || k === 32
      || (k >= 48 && k <= 57)) || k === 46 || k === 44) && k !== 32 ;
  }

  /**
   * Tính tỷ lệ A trên B
   *
   * @param A
   * @param B
   * @returns number
   */
  static calculatePercentage = (A: number, B: number): number => {
    if (!A || !B) { return 0; }
    const percent = (A * 100) / B;
    return percent <= 0 ? 0 : _.round(percent, 2);
  }

  /**
   * Chỉ cho nhập số không âm(>=0)
   * @param event
   */
  static positiveNumbers(event)
  {
    let k;
    k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return(k >= 48 && k <= 57) || k === 46 || k === 44;
  }

  static getValidOmitSpecial(value: string) {
    const str = value.trim();
    if (str.length > 0) {
      return /^([a-zA-Z0-9]{1,100})$/.test(str); // true => valid
    }
    return false;
  }
  static  removeAccents(str) {
    const AccentsMap = [
      "aàảãáạăằẳẵắặâầẩẫấậ",
      "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
      "dđ", "DĐ",
      "eèẻẽéẹêềểễếệ",
      "EÈẺẼÉẸÊỀỂỄẾỆ",
      "iìỉĩíị",
      "IÌỈĨÍỊ",
      "oòỏõóọôồổỗốộơờởỡớợ",
      "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
      "uùủũúụưừửữứự",
      "UÙỦŨÚỤƯỪỬỮỨỰ",
      "yỳỷỹýỵ",
      "YỲỶỸÝỴ"
    ];
    for (let i = 0; i < AccentsMap.length; i++) {
      const re = new RegExp('[' + AccentsMap[i].substr(1) + ']', 'g');
      const char = AccentsMap[i][0];
      str = str?.replace(re, char);
    }
    return str;
  }

  // static download(lob: Blob, fileName?: string) {
  //   const blob = new Blob([lob]);
  //   saveAs(blob, fileName);
  // };
  static getLimitLength(str: string, length = 20): string {
    return !!str ? (str.length <= length ? str : (str.slice(0, length) + '...')) : '';
  };

  static splitDate(str: string): string {
    let date = str.split('/');
    return date[1] + '/' + date[0] + '/' + date[2];
  };

  static newDate(date: string): Date {
    return new Date(this.splitDate(date));
  };

  /* Query param table */
  static onQueryParam(params: NzTableQueryParams): { pageIndex: number; pageSize: number, sortBy: string } {
    const {pageIndex, pageSize, sort, filter} = params;

    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    let sortBy = '';
    if (sortField && sortOrder) {
      sortBy = `${sortField}.${sortOrder === 'ascend' ? 'asc' : 'desc'}`;
    }
    return {pageIndex, pageSize, sortBy};
  };

  static formatToNumber(value: any): number {
    return Number(CommonUtil.numberFormatter(value));
  }

  static numberFormatter(value: string): string {
    return value.toString().replace(/\./g, '');
  }
}
