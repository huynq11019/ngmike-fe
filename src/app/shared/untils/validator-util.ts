import * as moment from "moment";

export default class ValidatorUtil {
    static isNotNull(value, columnName) {
        if (value && value.toString().trim()) {
            return;
        }
        return {columnName, content: ' không được bỏ trống' };
    }

    static isDate(value, columnName) {
        if (!value) {
            return;
        }
        if (!moment(value, 'dd/MM/yyyy').isValid()) {
            return { columnName, content: ' không đúng định dạng dd/MM/yyyy' };
        }
    }

    static isNumeric(value, columnName) {
        if (!value) {
            return;
        }
        if (isNaN(Number(value))) {
            return { columnName, content: ' không đúng dữ liệu kiểu số' };
        }
        if (Number(value) < 0) {
            return { columnName, content: ' số nhập phải lớn hơn bằng 0' };
        }
    }
}
