import { environment } from '../../../environments/environment';
export const SERVER_API_URL = environment.backend+ '/api';
export const LANGUAGE_VI = 'vi';
export const LANGUAGE_EN = 'en';
