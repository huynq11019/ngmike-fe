export const LOCAL_STORAGE = {
  JWT_TOKEN: 'ducky-portal-jwt-token',
  JWT_TOKEN_AUTH: 'ducky-portal-jwt-token-auth',
  URL_AUTHORIZATION_CODE: 'url-code',
  CHECK_LOGOUT_MS: 'check-logout-ms',
  URL: 'url',
  CODE: 'code',
  USER_NAME: 'UUNAME',
  LANGUAGE : 'LANG',
  ORDER : 'addOrder'
};
export const SESSION_STORAGE = {
  JWT_TOKEN: 'ducky-portal-jwt-token',
  JWT_TOKEN_AUTH: 'ducky-portal-jwt-token-auth',
  URL_AUTHORIZATION_CODE: 'url-code'
};
export const COOKIES = {
  REFRESH_TOKEN: 'ducky-portal-refresh-token'
};
