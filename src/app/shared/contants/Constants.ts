import { environment } from '../../../environments/environment';

export const PRODUCT_STATUS = {
  // all: {label: 'productManagement.status.all', color: 'green', value: 0},
  active: {label: 'productManagement.status.active', color: 'green', value: 1},
  stopSelling: {label: 'productManagement.status.stopSelling', color: 'red', value: 2},
  outofstock: {label: 'productManagement.status.outOfStock', color: 'yellow', value: 3},
  event: {label: 'productManagement.status.event', color: 'blue', value: 4},
  remove: {label: 'productManagement.status.removed', color: 'blue', value: -1},
  draft: {label: 'productManagement.status.draft', color: 'blue', value: 0}
};

export const DEFAULT_QUERY = {
  keyword: '',
  page: 0,
  size: 10,
  sort: '',
};

export const UserType = {
  admin: {label: "EMPLOYEE", color: 'green', value: 1},
  user: {label: "CUSTOMER", color: 'blue', value: 2},
  guest: {label: 'GUEST', color: 'red', value: 3}
};

export const ENTITY_TYPE = {
  PRODUCT: "PRO"
};
export const REGEX_CUSTOM = {
  EMAIL: '[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}',
  PHONE_NUMBER: '^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$',
  USER_NAME: '^[a-zA-Z0-9_]{5,20}$',
  PASSWORD: '^[a-zA-Z0-9_]{5,20}$',
};
export const API_STORAGE = environment.backend + "/api/file/uploadFile";

export const INFO_CUSTOM = {
  GENDER_DEFAULT: 1,
  STATUS_DEFAULT: 1,
};

export const SORT_PRODUCT_BY = [
  {label: "Tất cả", value: ""},
  {label: "Giá tăng dần", value: "price,ASC"},
  {label: "Giá giảm dần", value: "price,DESC"},
  {label: "Tên A-Z", value: "productName,ASC"},
  {label: "Tên Z-A", value: "productName,DESC"},
  {label: "Sản phẩm mới nhất", value: "createDate,DESC"},
  {label: "Sản phẩm cũ nhất", value: "createDate,ASC"},
]

export const SHOW_RECORD = [
  {label: "9", value: 9},
  {label: "18", value: 18},
  {label: "36", value: 36},
  {label: "72", value: 72},
]

export const CATEGORY_FIXER = {
  newProductType: 1,
  topSellingType: 2,
  laptopCategoryId: '5',
  smartphonesCategoryId: '6',
  camerasCategoryId: '7',
  accessoriesCategory: '8',
}

export const CATEGORY_TYPE = [
  {
    label: 'home.keycap',
    id: '1',
  },
  {
    label: 'home.keyBoard',
    id: '2',
  },
  {
    label: 'home.mice',
    id: '3',
  }, {
    label: "home.headphone",
    id: "4"
  },
  {
    label: 'home.periphetals',
    id: '5',
  }
]

export const ORDER_TYPE = [
  {
    label: 'order.orderStatus.all',
    id: '1',
  },
  {
    label: 'order.waiting',
    id: '2',
  },
  {
    label: 'order.shipping',
    id: '3',
  },
  {
    label: 'order.orderStatus.success',
    id: '4',
  },
  {
    label: 'order.orderStatus.cancel',
    id: '5',
  },
];

export const ORDER_STATUS_NUMBER = {
  WAITING_FOR_PAYMENT: 1,
  WAIT_FOR_CONFIRM: 2,
  CONFIRMED: 3,
  SHIPPING: 4,
  SUCCESS: 5,
  CANCELED: 6,
  PAID: 7,
};
export const ORDER_STATUS_STRING = {
  CREATE: 'CREATE',
  WAITING_FOR_PAYMENT: 'WAITING_FOR_PAYMENT',
  WAITING_FOR_CONFIRM: 'WAIT_FOR_CONFIRM',
  CONFIRMED: 'CONFIRMED',
  SHIPPING: 'SHIPPING',
  PAID: 'PAID',
  CANCELED: 'CANCELED'
};

export const ORDER_STATUS = {
  CREATE: {
    value: 1,
    label: 'CREATE',
  },
  WAITING_FOR_PAYMENT: {
    value: 2,
    label: 'WAITING_FOR_PAYMENT',
  },
  PAID: {
    value: 6,
    label: 'PAID',
  },
  WAITING_FOR_CONFIRM: {
    value: 3,
    label: 'WAIT_FOR_CONFIRM',
  },
  CONFIRMED: {
    value: 4,
    label: 'CONFIRMED',
  },
  SHIPPING: {
    value: 5,
    label: 'SHIPPING',
  },
  SUCCESS: {
    value: 200,
    label: 'SUCCESS'
  },
  CANCELED: {
    value: 7,
    label: 'CANCELED'
  }
};
