
export const CUSTOMER = {
    ACTIVE : 1,
    VERIFY : 0,
    BLOCKED : -1,
}
// convert date to milliseconds
export function dateToMs(date: Date): number {
    return date.getTime();
}