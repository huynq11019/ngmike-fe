import { IOrder } from './../models/order-customer';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { createRequestOption } from '../untils/Request-untils';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root',
})
export class MyOrderService {
  constructor(private http: HttpClient) {
  }

  createOrder(order: IOrder): Observable<httpRespose> {
    return this.http.post<any>(`${api}/me/orders/create`, order, {observe: 'response'});
  }

  getOrderDetailByUserId(param: string): Observable<httpRespose> {
    return this.http.get(`${api}/orders/${param}`, {observe: 'response'});
  }

  getCurrentCustomerOrder(param?: any): Observable<httpRespose> {
    const params = createRequestOption(param);
    return this.http.get(`${api}/orders/my-order`, {observe: 'response', params: params});
  }

  getAllOrder(option?: any): Observable<httpRespose> {
    const params = createRequestOption(option);
    return this.http.get(`${api}/orders`, {observe: 'response', params});
  }

  getOrderDetailByOrderId(orderId: string): Observable<httpRespose> {
    return this.http.get(`${api}/orders/${orderId}`, {observe: 'response'});
  }

  getOrderbyStatus(statusId: string): Observable<httpRespose> {
    return this.http.get(`${api}/orders?orderStatus=${statusId}`, {observe: 'response'});
  }
  changeOrderStatus(orderId: string, orderStatus: string): Observable<httpRespose> {
    return this.http.post(`${api}/orders/${orderId}/payment/status?orderStatus=${orderStatus}`, orderStatus, {
      observe: 'response',
    });
  }
  getListOrderByUserId(param: string): Observable<httpRespose> {
    return this.http.get(`${api}/orders/customer/${param}`, { observe: 'response' });
  }
}
