import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { createRequestOption } from '../untils/Request-untils';
import { ICategory } from '../models/category';


type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root'
})

export class CategoryService {

  constructor(private http: HttpClient) {
  }

  /**
   * @author: NamTH
   * @since: 11/11/2021  10:12 PM
   * @description:  lấy tất cả danh sách category
   * @update:
   */
  loadAllCategory(req): Observable<httpRespose> {
    const param = createRequestOption(req);
    return this.http.get(`${api}/category/search`, {observe: 'response', params: param});
  }

  /**
   * @author: NamTH
   * @since: 11/11/2021  10:13 PM
   * @description:  tạo category
   * @update:
   */
  createCategory(category: ICategory): Observable<httpRespose> {
    return this.http.post<any>(`${api}/category/create`, category);
  }


  /**
   * @author: NamTH
   * @since: 11/11/2021  10:14 PM
   * @description:  cập nhật category
   * @update:
   */
  updateProduct(category: ICategory): Observable<httpRespose> {
    return this.http.put<any>(`${api}/category/${category.id}`, category);
  }


  loadCategoryTree(req?:any): Observable<httpRespose> {
    const param = createRequestOption(req);
    return this.http.get(`${api}/category/search-tree`, {observe: 'response', params: param});
  }

  // change(productId: string, status: number, noloading = false): Observable<httpRespose> {
  //   return this.http.delete(`${api}/product/${productId}/${status}/changestatus`, {
  //     observe: "response",
  //     headers: {noLoading: noloading ? "true" : ""}
  //   });
  // }
}
