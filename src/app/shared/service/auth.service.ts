import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map, tap } from 'rxjs/operators';
import jwtDecode, { JwtPayload } from "jwt-decode";

import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { COOKIES, LOCAL_STORAGE, SESSION_STORAGE } from '../contants/location-session-cookie';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { User } from '../models/User';
import { throwError } from 'rxjs';

const api = environment.backend + '/api';
const userFomat = {
  userName: '',
  userId: '',
  authorites: [],
  userType: ''
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public userIdentity: any; // thông tin người dùng
  public authenticated = false; // trạng thái đăng nhập
  public userType : string; // kiểu người dùng
  constructor(private http: HttpClient,
              private toasrService: ToastrService,
              private cookieService: CookieService,
              private $localStorage: LocalStorageService,
              private $sessionStorage: SessionStorageService,
  ) {
  }

// {
//   "userName" :"admin_vosong9201@gmail.com",
//   "passWord":"123"
// }
  doLogin(userName: string, passWord: string, rm: boolean) {
    return this.http.post<any>(`${api}/authenticate`,
      {userName: userName, passWord: passWord}, {observe: 'response'})
    .pipe(map(this.authenticationSuccess.bind(this, rm)));
  }

  private authenticationSuccess(rememberMe, respon) {
    // const auth = respon.body?.auth;
    const refreshToken = respon.body?.refreshToken;
    const accessToken = respon.body?.accessToken;
    // console.log(auth, accessToken);
    console.log(refreshToken, accessToken);
    // check trạng thái đăng nhâp
    if (respon.body.errorCode != 'ERR_00000') {
      this.toasrService.error("Đăng nhập không thành công");
    } else {

      // check refresh token
      if (!!refreshToken.token) {
        // lưu refresh token vào cookie
        this.cookieService.set(COOKIES.REFRESH_TOKEN, refreshToken?.token, new Date(refreshToken.tokenExpDate), '/');
      }
      // lưu thông tin người dùng vào bộ nhớ tạm
      const userFormToken = this.getUserTokenFromJwt(accessToken.token);
      // lưu tên người dùng vào local store
      this.$localStorage.store(LOCAL_STORAGE.USER_NAME, JSON.stringify(userFormToken));
      // kiểm tra nếu tồn tại access token thì lưu thon tin nười dùng vào hệ thóng
      if (accessToken.token) {
        this.storeAuthenticationToken(accessToken.token, rememberMe);
        return accessToken.token;
      } else {
        this.userIdentity = null;
        this.authenticated = false;
      }
      this.toasrService.success('Đã thực hiện đăng nhập');
    }
  }

  /**
   * @author: huynq
   * @since: 11/25/2021 9:47 PM
   * @description:
   * @update:
   */
  public oauthLogin(tokenAccessToken: string, rm: boolean) {

    // kiểm tra nếu tồn tại access token thì lưu thon tin nười dùng vào hệ thóng
    if (tokenAccessToken) {
      this.storeAuthenticationToken(tokenAccessToken);
      // valid token
      const userFormToken = this.getUserTokenFromJwt(tokenAccessToken);
      console.log(userFormToken);
      // lưu tên người dùng vào local store
      this.$localStorage.store(LOCAL_STORAGE.USER_NAME, JSON.stringify(userFormToken));
      this.toasrService.success('Đã thực hiện đăng nhập');
    } else {
      this.userIdentity = null;
      this.authenticated = false;
    }
  }

  /**
   * @description lưu thông tin đăng nhập vào bộ nhớ tạm
   * @param jwt
   * @param jwtAuth
   * @param rememberMe
   */
  storeAuthenticationToken(jwt?: string, jwtAuth?: string, rememberMe = false) {
    if (rememberMe) { // nếu người dùng chọn nhớ mật khẩu thì thực hiện lưu vào localstore
      this.$localStorage.store(LOCAL_STORAGE.JWT_TOKEN, jwt); //access token
      // this.$localStorage.store(LOCAL_STORAGE.JWT_TOKEN_AUTH, jwtAuth); // authentication token
    } else {
      this.$sessionStorage.store(SESSION_STORAGE.JWT_TOKEN, jwt);
      // this.$sessionStorage.store(LOCAL_STORAGE.JWT_TOKEN_AUTH, jwtAuth);
    }
  }

  /**
   * description: lấy thông tin người dùng từ jwt
   * @param
   * @return
   * update:
   */
  getUserTokenFromJwt(token): any {
    if (token) {
      const tokenObject = jwtDecode<JwtPayload>(token);
      // const userId = {userId: JSON.parse(tokenObject.sub)};
      const userId = tokenObject.sub;
      let user = userFomat;
      if (token) {
        // const authorities = jwtDecode<JwtPayload>(token);
        // console.log('authrority', authorities);
        user = {
          userName: tokenObject['email'] || '',
          userId: userId,
          authorites: JSON.parse(tokenObject['authority']) || [],
          userType: tokenObject['userType'] || "CUSTOMER"
        };
      }
      user.authorites.push('GUEST');
      // console.log("thông tin user sau khi parse ", user);
      this.setAuthenticate(user);
      return user;
    }
    return null;
  }

  private setAuthenticate(identity) {
    this.userIdentity = identity;
    this.authenticated = identity !== null;
    this.userType = identity?.userType;
  }

  hasAnyAuthority(authorities: string[]): boolean {
    this.userIdentity = this.getCurrentUser();
    // console.log(authorities, this.getCurrentUser());
    let userAuthorities = [];
    if (this.userIdentity) {
      userAuthorities = this.userIdentity.authorites;
    }
    // userAuthorities.push("APPROVAL_DASHBOARD");
    userAuthorities.push("GUEST");

    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < userAuthorities?.length; i++) {
      if (userAuthorities.includes(authorities[i])) {
        return true;
      }
    }
    return false;
  }

  getCurrentUser(): User {
    const jwt = this.$localStorage.retrieve(LOCAL_STORAGE.JWT_TOKEN) || this.$sessionStorage.retrieve(SESSION_STORAGE.JWT_TOKEN);
    const jwtAuth = this.$sessionStorage.retrieve(SESSION_STORAGE.JWT_TOKEN_AUTH) || this.$localStorage.retrieve(LOCAL_STORAGE.JWT_TOKEN_AUTH);
    const currentName = this.$localStorage.retrieve(LOCAL_STORAGE.USER_NAME);
    // tham số trueyefn vào là access token  và authentication token
    return this.getUserTokenFromJwt(jwt) || null;
  }

  getToken(): string {
    return this.$localStorage.retrieve(LOCAL_STORAGE.JWT_TOKEN) || this.$sessionStorage.retrieve(SESSION_STORAGE.JWT_TOKEN);
  }
  refreshToken(refreshToken, userName) {
    return this.http.post<any>(`${environment.backend}/getNewAccessToken`, {userName, refreshToken}).pipe(
      tap(
        (res) => {
          this.storeAuthenticationToken(res?.accessToken?.token, res?.authorities?.token, true);
        },
        (err) => throwError(err)
      )
    );
  }

  doLogout() {
    this.$localStorage.clear(LOCAL_STORAGE.JWT_TOKEN);
    this.$localStorage.clear(LOCAL_STORAGE.JWT_TOKEN_AUTH);
    this.$localStorage.clear(LOCAL_STORAGE.USER_NAME);
    this.$sessionStorage.clear(SESSION_STORAGE.JWT_TOKEN);
    this.$sessionStorage.clear(SESSION_STORAGE.JWT_TOKEN_AUTH);
    this.cookieService.delete(COOKIES.REFRESH_TOKEN);
    this.authenticated = false;
    this.userIdentity = null;
    // this.toasrService.success('Đã thực hiện đăng xuất');
  }
}
