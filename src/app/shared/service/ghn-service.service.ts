import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IFeeRequest } from '../models/FeeRequest';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GhnServiceService {
  private urlAPI = 'https://dev-online-gateway.ghn.vn/';

  constructor(protected http: HttpClient,) {
  }

  public getFee(request: IFeeRequest): Observable<any> {
    return this.http.post(`${this.urlAPI}shiip/public-api/v2/shipping-order/fee`, request, {
      observe: 'response',
      headers: {
        'Token': '339ee9f5-5b37-11ec-ac64-422c37c6de1b',
        'ShopId': '84307',
      }
    });
  }

  public getTrackingOrder(request: IFeeRequest): Observable<any> {
    return this.http.post(`${this.urlAPI}shiip/public-api/v2/shipping-order/detail'`, request, {
      observe: 'response',
      headers: {
        'Token': '339ee9f5-5b37-11ec-ac64-422c37c6de1b',
        'ShopId': '84307',
      }
    });
  }

  public getToken(): Observable<any> {
    return this.http.get(`${this.urlAPI}shiip/public-api/v2/a5/gen-token`, {
      observe: 'response',
      headers: {
        'Token': '339ee9f5-5b37-11ec-ac64-422c37c6de1b',
        'ShopId': '84307',
      }
    });
  }

  public getHoaDon(request: IFeeRequest): Observable<any> {
    return this.http.post(`${this.urlAPI}a5/public-api/printA5?token=ABC`, request, {
      observe: 'response',
      headers: {
        'Token': '339ee9f5-5b37-11ec-ac64-422c37c6de1b',
        'ShopId': '84307',
      }
    });
  }

  public createShipping(request: IFeeRequest): Observable<any> {
    return this.http.post(`${this.urlAPI}shiip/public-api/v2/shipping-order/create`, request, {
      observe: 'response',
      headers: {
        'Token': '339ee9f5-5b37-11ec-ac64-422c37c6de1b',
        'ShopId': '84307',
      }
    });
  }

}
