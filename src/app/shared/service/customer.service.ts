import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

import { createRequestOption } from '../untils/Request-untils';
import { ICustomer } from '../models/customer';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  constructor(private http: HttpClient) {}

  /**
   * description: Customer đăng ký tài khoản
   * @param customerInfor thông tin của Customer
   * @return
   * update:
   */
  register(customerInfor: ICustomer): Observable<httpRespose> {
    return this.http.post<any>(`${api}/customer/register`, customerInfor, { observe: 'response' });
  }

  /**
   * description: Khách hàng hoặc quản lý thực hiện cập nhật thông tin
   * @param: customerInfor Thông tin của người dùng
   * @return:
   * update:
   */
  udpateUser(customerInfor: ICustomer): Observable<httpRespose> {
    return this.http.put<any>(`${api}/customer/account/update-profile`, customerInfor, { observe: 'response' });
  }

  /**
   * description: Tìm kiếm Customer trong hệ thống
   * @param: option query param
   * @return:
   * update:
   */
  searchCustomer(option): Observable<httpRespose> {
    const params = createRequestOption(option);
    return this.http.get<any>(`${api}/customer/search`, { observe: 'response', params });
  }

  /**
   * description: Lấy thông tin người dùng theo user Id
   * @param customerId id của customer
   * @return
   * update:
   */
  getInforCustomer(customerId): Observable<httpRespose> {
    return this.http.get<any>(`${api}/customer/` + customerId);
  }

  getCurrentUserInfo(customerId?: string, noloading = false): Observable<httpRespose> {
    return this.http.get<any>(`${api}/account/my-profile`, {
      observe: 'response',
      headers: { noLoading: noloading ? 'true' : '' },
    });
  }

  /**
   * description:  customer quên mật khẩu
   * @param  cusomterEmail email của khách hàng đã đăng ký trong hệ thống
   * @return
   * update:
   */
  forgotPasswordCustomer(customerEmail: string): Observable<httpRespose> {
    return null;
  }

  /**
   * description: Customer đổi mật khẩu
   * @param
   * @return
   * update:
   */
  changePassWord(customerId, oldPassWord, newPassWord): Observable<httpRespose> {
    return null;
  }

  /**
   * description: Người dùng thực hiện xác thực  tài khoản
   * @param:
   * @return
   * update:
   */
  verifyUser(email, activeToken: string): Observable<httpRespose> {
    const param = new HttpParams().set('email', email).set('activeToken', activeToken);
    return this.http.get<any>(`${api}/customer/verify`, { observe: 'response', params: param });
  }

  /**
   * description: quản lý thực hiện khóa tài khaorn khách hàng
   * @param customerInfor id của khách hàng
   * @param status trạng thái của customer
   * @return:
   * update:
   */
  lockUser(customerId: string, status: number): Observable<httpRespose> {
    return this.http.get<any>(`${api}/customer/${customerId}/${status}`, { observe: 'response' });
  }
}
