import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { createRequestOption } from '../untils/Request-untils';



type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root',
})
export class WishlistService {
  constructor(private http: HttpClient) {}

  loadWishlist(req?): Observable<httpRespose> {
    const param = createRequestOption(req);
    return this.http.get(`${api}/favourites/account/search`, { observe: 'response', params: param });
  }
  getFavoriteByUserId(userId: string): Observable<httpRespose> {
    return this.http.get(`${api}/favourites/user/${userId}`, { observe: 'response' });
  }
  createFavorite(param: any): Observable<httpRespose> {
    return this.http.post(`${api}/favourites/account/create`, param, { observe: 'response' });
  }

  deleteFavorite(favoriteId: string): Observable<httpRespose> {
    return this.http.delete(`${api}/me/favourites/${favoriteId}`, { observe: 'response' });
  }
  public addToWishlist(productId: string): Observable<httpRespose> {
    return this.http.post(`${api}/favourites/account/create`, { productId }, { observe: 'response' });
  }
}
