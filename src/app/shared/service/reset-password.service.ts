import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root',
})
export class ResetPasswordService {
  constructor(private http: HttpClient) {}

  resetPassword(employeeId: string): Observable<httpRespose> {
    return this.http.get(`${api}/employee/${employeeId}/rsp`, {
      observe: 'response',
    });
  }
}
