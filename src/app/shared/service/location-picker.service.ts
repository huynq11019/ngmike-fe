import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';

type httpRespose = HttpResponse<any>;
const api = environment.location + '/api';
const apiGHN = environment.locationGHN;
@Injectable({
  providedIn: 'root',
})
export class LocationPickerService {
  constructor(private http: HttpClient) {}

  loadLocationByProvince(province?: number): Observable<httpRespose> {
    return this.http.get(`${api}/p/${province}`, {
      observe: 'response',
    });
  }

  loadLocationByDistrict(codeProvince: number, noloading = true): Observable<httpRespose> {
    return this.http.get(`${api}/p/${codeProvince}?depth=2`, {
      observe: 'response',
    });
  }

  loadLocationByWard(codeDistrict: number): Observable<httpRespose> {
    return this.http.get(`${api}/d/${codeDistrict}?depth=2`, { observe: 'response' });
  }

  loadLocationByProvinces(): Observable<httpRespose> {
    return this.http.get(`${api}/p/`, { observe: 'response' });
  }

  loadLocationByDistricts(): Observable<httpRespose> {
    return this.http.get(`${api}/d/`, {
      observe: 'response',
    });
  }

  loadLocationByWards(): Observable<httpRespose> {
    return this.http.get(`${api}/w/`, { observe: 'response' });
  }

  loadLocationByProvinceGHN(province_id?: number): Observable<httpRespose> {
    return this.http.get(`${apiGHN}/province`, {
      observe: 'response',
      headers: {
        token: '339ee9f5-5b37-11ec-ac64-422c37c6de1b',
      },
    });
  }

  loadLocationByProvinceGHNById(province_id?: number): Observable<httpRespose> {
    return this.http.post(`${apiGHN}/province`, province_id, {
      observe: 'response',
      headers: {
        token: '339ee9f5-5b37-11ec-ac64-422c37c6de1b',
      },
    });
  }
  loadLocationByDistrictGHN(provinceId: number, noloading = true): Observable<httpRespose> {
    return this.http.post(`${apiGHN}/district`, {province_id: provinceId}, {
      observe: 'response',
      headers: {
        token: '339ee9f5-5b37-11ec-ac64-422c37c6de1b',
      },
    });
  }

  loadLocationByWardGHN(province_id: number): Observable<httpRespose> {
    return this.http.get(`${apiGHN}/ward?district_id=${province_id}`, {
      observe: 'response',
      headers: {
        token: '339ee9f5-5b37-11ec-ac64-422c37c6de1b',
      },
    });
  }
}
