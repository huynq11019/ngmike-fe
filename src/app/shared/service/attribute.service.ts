import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { createRequestOption } from '../untils/Request-untils';
import { ICategory } from '../models/category';
import { Iattibute } from '../models/attribute';
import { IproductAttibute } from '../models/productAttribute';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root'
})
export class AttributeService {

  constructor(private http: HttpClient) { }

  loadAttrubuteByCategory(req, categoryId:string): Observable<httpRespose>{
    const param = createRequestOption(req);
    return this.http.get(`${api}/categories/${categoryId}/attributes`, {observe: 'response', params: param});
  }

  loadProductAttrubuteByCategory(req, categoryId: string): Observable<httpRespose> {
    const param = createRequestOption(req);
    return this.http.get(`${api}/products/${categoryId}/product-attributes`, {observe: 'response', params: param});
  }

  createAttributeByCategory(attribute: Iattibute) {
    return this.http.post<any>(`${api}/categories/${attribute.categoryId}/attributes`, attribute);
  }

  updateAttrubuteByCategory(attribute: Iattibute, attributeId: string): Observable<httpRespose> {
    return this.http.post<any>(`${api}/categories/${attribute.categoryId}/attributes/${attributeId}`, attribute);
  }

  saveAttributeByProduct(productId: string, request: IproductAttibute[]): Observable<httpRespose> {
    return this.http.post(`${api}/products/${productId}/product-attributes`, {productAttributes: request}, {observe: 'response'});
  }

  loadAttributeByProduct(productId: string): Observable<httpRespose> {
    return this.http.get(`${api}/products/${productId}/product-attributes`, {observe: 'response'});
  }
}
