import { TestBed } from '@angular/core/testing';

import { GhnServiceService } from './ghn-service.service';

describe('GhnServiceService', () => {
  let service: GhnServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GhnServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
