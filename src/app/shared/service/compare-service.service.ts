import { Injectable } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { Iproduct } from '../models/product';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class CompareServiceService {
  public compareList: Iproduct[] = [];

  constructor(private $localStorage: LocalStorageService,
              private toastr: ToastrService) { }

  addProductToCompare(product: Iproduct) {
    let products = this.$localStorage.retrieve('compareProducts');
    // check list compare không rỗng
    if (products) {
      // check product đã tồn tại trong list compare chưa
      if (products.findIndex(x => x.id === product.id) === -1) {
        this.toastr.success('Sản phẩm đã được thêm vào danh sách so sánh');
        // chekc khác danh mục thì xóa hết list compare
        if (products.findIndex(x => x.categoryId === product.categoryId) === -1) {
          products = [];
        }
        products = [...products, product];
      }else{
        this.toastr.warning('Sản phẩm đã tôn tại trong danh sách só sánh');
      }
      this.compareList = products;
      this.$localStorage.store('compareProducts', products);
    } else {
      products = [];
      products.push(product);
      this.$localStorage.store('compareProducts', products);
    }
  }

  removeProductFromCompare(product: Iproduct) {
    let products = this.$localStorage.retrieve('compareProducts');
    if (products) {
      products.forEach((item, index) => {
        if (item.id === product.id) {
          products.splice(index, 1);
        }
      });
      this.$localStorage.store('compareProducts', products);
    }
  }

  getCompareProducts() { // trả về một list thông tin của các sản phẩm đang được so sánh
    return this.$localStorage.retrieve('compareProducts');
  }

  clearCompareList() {
    this.$localStorage.clear('compareProducts');
    this.compareList = [];
  }

  checkIsEmpty() {
    this.compareList = this.$localStorage.retrieve('compareProducts');
    if (!this.compareList){
      return false;
    }
    if (this.compareList.length === 0) {
      return false;
    } else {
      return true;
    }
    // return this.compareList.length === 0;
  }

  getCompareList(): Array<Iproduct> {
    this.compareList = this.$localStorage.retrieve('compareProducts');
    return this.compareList;
  }

  getCompareListLength() {
    this.compareList = this.$localStorage.retrieve('compareProducts');
    return this.compareList.length;
  }

  isCompare(product: Iproduct) {
    let products = this.$localStorage.retrieve('compareProducts');
    if (products) {
      return products.findIndex(x => x.id === product.id) !== -1;
    }
    return false;
  }
}
