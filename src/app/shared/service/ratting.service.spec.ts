/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RattingService } from './ratting.service';

describe('Service: Ratting', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RattingService]
    });
  });

  it('should ...', inject([RattingService], (service: RattingService) => {
    expect(service).toBeTruthy();
  }));
});
