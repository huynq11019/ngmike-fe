import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  public resourceUrl = environment.backend + '/api';

  constructor(protected http: HttpClient,
              private $localStorage: LocalStorageService,
              private $sessionStorage: SessionStorageService,
  ) {
  }

  getListFile(options?: any): Observable<any> {
    return this.http.get<any>(this.resourceUrl + '/getListFile', {params: options, observe: 'response'});
  }
  /**
   * @author: huynq
   * @since: 11/14/2021 6:19 AM
   * @description:  Lấy thông tin file theo id
   * @update:
   */
  getFileById(fileId: string): Observable<any> {
    return this.http.get<any>(this.resourceUrl + '/file/getFileByid/' + fileId);
  }
  /**
   * @author: huynq
   * @since: 11/14/2021 6:08 AM
   * @description:  Tải file theo fileId
   * @update:
   */
  downloadFileById(fileId: string) {
    return `${this.resourceUrl}/download-file/${fileId}}`;
  }
  /**
   * @author: huynq
   * @since: 11/14/2021 6:08 AM
   * @description:  Tải file theo đường đẫn
   * @update:
   */
  downloadFileByPath(filePath: string) {
    return `${this.resourceUrl}/download-file-by-path/${filePath}}`;
  }
  /**
   * @author: huynq
   * @since: 11/14/2021 6:08 AM
   * @description:  view FIle image
   * @update:
   */
  viewFileImage(filePath: string) {
    return `${this.resourceUrl}/file/view-image/${filePath}}`;
  }
  /**
   * @author: huynq
   * @since: 11/18/2021 1:44 PM
   * @description:  get image byte by owner id và owner Type
   * @param ownerId : ownerId
   * @param ownerType : ownerType
   * @update:
   */
  viewImageByOwnerIdAndOwnerType(ownerId: string, ownerType: string): string {
    return `${this.resourceUrl}/file/view-frist/${ownerId}/PRO`;
  }
  /**
   * Xóa file theo id
   *
   * @author huynq1808
   * @date 2021-09-08
   * @param {number} fileId
   * @returns {Observable<any>}
   * @memberof FileUploadService
   */
  removeFileById(fileId: string): Observable<any> {
    return this.http.delete<any>(this.resourceUrl + `/file/delete/${fileId}`, {observe: 'response'});
  }
}
