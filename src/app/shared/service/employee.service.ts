import { IEmployee } from './../models/employee';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { createRequestOption } from '../untils/Request-untils';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor(private http: HttpClient) {}

  /**
   * description: Khách hàng hoặc quản lý thực hiện cập nhật thông tin
   * @param: employeeInfor Thông tin của người dùng
   * @return:
   * update:
   */
  udpateUser(employeeId, employeeInfor: IEmployee): Observable<httpRespose> {
    return this.http.put<any>(`${api}/employee/update`, employeeInfor, { observe: 'response' });
  }

  /**
   * description: Tìm kiếm employee trong hệ thống
   * @param: option query param
   * @return:
   * update:
   */
  searchEmployee(option): Observable<httpRespose> {
    const params = createRequestOption(option);
    return this.http.get<any>(`${api}/employee/search`, { observe: 'response', params });
  }

  /**
   * description: Lấy thông tin người dùng theo user Id
   * @param employeeId id của employee
   * @return
   * update:
   */
  getInforEmployee(employeeId): Observable<httpRespose> {
    return this.http.get<any>(`${api}/employee/` + employeeId);
  }

  /**
   * description:  employee quên mật khẩu
   * @param  cusomterEmail email của khách hàng đã đăng ký trong hệ thống
   * @return
   * update:
   */
  forgotPasswordEmployee(cusomterEmail: string) {
    return null;
  }

  /**
   * description: employee đổi mật khẩu
   * @param
   * @return
   * update:
   */
  changePassWord(employeeId, oldPassWord, newPassWord): Observable<httpRespose> {
    return null;
  }

  /**
   * description: Người dùng thực hiện xác thực  tài khoản
   * @param:
   * @return
   * update:
   */
  verifyUser(email, activeToken: string): Observable<httpRespose> {
    const param = new HttpParams().set('email', email).set('activeToken', activeToken);
    return this.http.get<any>(`${api}/employee/verify`, { observe: 'response', params: param });
  }

  /**
   * description: quản lý thực hiện khóa tài khaorn khách hàng
   * @param employeeInfor id của khách hàng
   * @param status trạng thái của employee
   * @return:
   * update:
   */
  lockUser(employeeId: string ): Observable<httpRespose> {
    return this.http.get<any>(`${api}/employee/${employeeId}/lock`, { observe: 'response' });
  }

  createEmployee(employee: IEmployee): Observable<httpRespose> {
    return this.http.post<any>(`${api}/employee/create`, employee, { observe: 'response' });
  }
  updateEmployee(employeeId: string, employee: IEmployee): Observable<httpRespose> {
    return this.http.put<any>(`${api}/employee/${employeeId}`, employee, { observe: 'response' });
  }
}
