import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { IPaymentRequest } from '../models/PaymentRequest';
import { environment } from '../../../environments/environment';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';
@Injectable({
  providedIn: 'root'
})
export class ZalopayService {

  constructor(private http: HttpClient) {
  }

  peymentZalo(request: IPaymentRequest) {
    return this.http.post(`${api}/order/zalo-pay/checkout`, request, { observe: 'response' });
  }
}
