import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { createRequestOption } from '../untils/Request-untils';
import { ICart } from '../models/my-cart';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root',
})
export class MyCartService {
  private totalItems: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  getCartItems() {
    return this.totalItems.asObservable();
  }

  updateCartItems(items: number) {
    this.totalItems.next(items);
  }
  constructor(private http: HttpClient) {}

  addToCart(cart: ICart, noloading = true) {
    return this.http.post<any>(`${api}/cart/me/add`, cart, {
      observe: 'response',
      headers: { noLoading: noloading ? 'true' : '' },
    });
  }
  // get data
  searchCart(option: any, noloading = true): Observable<httpRespose> {
    const options = createRequestOption(option);
    return this.http.get<any>(`${api}/cart/me/search`, {
      observe: 'response',
      params: options,
      headers: { noLoading: noloading ? 'true' : '' },
    });
  }

  getInforCart(cartId: any): Observable<httpRespose> {
    return this.http.get<any>(`${api}/cart/` + cartId);
  }

  createOrUpdateCart(cart: ICart, noloading = true): Observable<httpRespose> {
    return this.http.post<any>(`${api}/cart/me/update`, cart, {
      observe: 'response',
      headers: { noLoading: noloading ? 'true' : '' },
    });
  }

  deleteCartByIds(data: string[], noloading = false): Observable<httpRespose> {
    return this.http.put<any>(
      `${api}/cart/me/delete`,
      { deletedProductIds: data },
      {
        observe: 'response',
        headers: { noLoading: noloading ? 'true' : '' },
      }
    );
  }
  getCartByIds(data: string[], noloading = true): Observable<httpRespose> {
    return this.http.post<any>(
      `${api}/cart/cart/find-by-ids`,
      { ids: data },
      {
        observe: 'response',
        headers: { noLoading: noloading ? 'true' : '' },
      }
    );
  }
  getListCartByUserId(customerId : string): Observable<httpRespose> {
    return this.http.get<any>(`${api}/cart/search/${customerId}`, {
      observe: 'response',
    });
  }
}
