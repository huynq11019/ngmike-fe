import { TestBed } from '@angular/core/testing';

import { DateCenterService } from './date-center.service';

describe('DateCenterService', () => {
  let service: DateCenterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DateCenterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
