import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateCenterService {
  public wishList: any[] = [];
  public cartList: any[] = [];
  public totalItemCart: number = 0;
  public totalItemFavorite: number = 0;
  constructor() {}

  getTotalCartItem() {
    return this.totalItemCart;
  }
  getTotalFavoriteItem() {
    return this.totalItemFavorite;
  }
  setTotalCartItem(newMessage) {
    this.totalItemCart = newMessage;
  }
  setTotalFavoriteItem(newMessage) {
    this.totalItemFavorite = newMessage;
  }
}
