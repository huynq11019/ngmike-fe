import { TestBed } from '@angular/core/testing';

import { ZalopayService } from './zalopay.service';

describe('ZalopayService', () => {
  let service: ZalopayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ZalopayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
