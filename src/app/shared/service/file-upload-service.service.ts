import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root'
})
export class FileUploadServiceService {

  constructor(private http: HttpClient) {
  }

  fakeUploadFile(noloading = false) {
    return this.http.post(`${api}/file/fakeUpload`, {}, {
      observe: 'response',
      headers: {noLoading: noloading ? "true" : ""}
    });
  }

  deleteImage(imageName: string, noloading = false) {
    return this.http.delete(`${api}/file/delete/${imageName}`, {
      observe: 'response',
      headers: {noLoading: noloading ? "true" : ""}
    });
  }

}
