import { Injectable } from '@angular/core';

import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import CommonUtil from '../untils/common-utils';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';
@Injectable({
  providedIn: 'root',
})
export class RattingService {
  constructor(private http: HttpClient) {}

  loadRatting(productId: string): Observable<httpRespose> {
    return this.http.get(`${api}/rating/product/${productId}`, { observe: 'response' });
  }

  createRating(productId: string, param: any): Observable<httpRespose> {
    return this.http.post(`${api}/rating/product/${productId}`, param, { observe: 'response' });
  }
}
