import { IMyAddress } from './../models/my-address';
import { Observable } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { createRequestOption } from '../untils/Request-untils';


type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';
@Injectable({
  providedIn: 'root',
})
export class MyAddressService {
  constructor(private http: HttpClient) {}

  loadMyAddress(req): Observable<httpRespose> {
    const param = createRequestOption(req);
    return this.http.get(`${api}/address/search`, { observe: 'response', params: param });
  }

  updateMyAddress(address: IMyAddress): Observable<httpRespose> {
    return this.http.put<any>(`${api}/address/updateAddress`, address);
  }
  /**
   * @author:
   * @since:
   * @description:
   */
  // Lấy nhiều thông tin address
  getAddressByIds(addressId: string): Observable<httpRespose> {
    return this.http.get(`${api}/address/customer/${addressId}`, { observe: 'response' });
  }
  //lấy 1 thông tin địa chỉ dùng cho sửa
  getAddressById(addressId: string, noloading?: true): Observable<httpRespose> {
    return this.http.get(`${api}/address/${addressId}`, {
      observe: 'response',
      headers: { noLoading: noloading ? 'true' : '' },
    });
  }
  createAddress(address: IMyAddress): Observable<httpRespose> {
    return this.http.post<any>(`${api}/address/create`, address, { observe: 'response' });
  }
  findByCustomerId(customerId: string) {
    return this.http.get<any>(`${api}/address/customer/${customerId}`, { observe: 'response' });
  }
  deleteById(addressId: number): Observable<httpRespose> {
    return this.http.delete(`${api}/address/me/${addressId}/delete`, { observe: 'response' });
  }
  setAddressDefault(addressId: number): Observable<httpRespose> {
    return this.http.put<any>(`${api}/address/me/${addressId}/default`, null);
  }
}
