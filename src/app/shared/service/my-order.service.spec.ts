/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MyOrderService } from './my-order.service';

describe('Service: MyOrder', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyOrderService]
    });
  });

  it('should ...', inject([MyOrderService], (service: MyOrderService) => {
    expect(service).toBeTruthy();
  }));
});
