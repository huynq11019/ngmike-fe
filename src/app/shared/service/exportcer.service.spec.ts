import { TestBed } from '@angular/core/testing';

import { ExportcerService } from './exportcer.service';

describe('ExportcerService', () => {
  let service: ExportcerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExportcerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
