import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { createRequestOption } from '../untils/Request-untils';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { IserriProduct } from '../models/SerriProduct';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root'
})
export class SerriProductService {

  constructor(private http: HttpClient) {
  }

  /**
   * @author: huynq
   * @since: 11/14/2021 3:41 AM
   * @description:  search Danh sách sản phẩm
   * @update:
   */
  search(req, noloading = true): Observable<httpRespose> {
    const option = createRequestOption(req);
    return this.http.get<any>(`${api}/serri`, {
      observe: 'response', params: option, headers: {noLoading: noloading ? "true" : ""}
    });
  }

  /**
   * @author: huynq
   * @since: 11/14/2021 3:41 AM
   * @description:  Tạo những serri của sản  phẩm
   * @update:
   */
  createUpdate(serri: IserriProduct) {
    return this.http.post(`${api}/serri/create-update`, serri, {observe: 'response', headers:
        {noLoading: "true"}});
  }
}
