import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { createRequestOption } from '../untils/Request-untils';
import { Iproduct } from '../models/product';
import CommonUtil from '../untils/common-utils';

type httpRespose = HttpResponse<any>;
const api = environment.backend + '/api';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  /**
   * @author: huynq
   * @since: 10/31/2021 11:47 AM
   * @description:  tìm kiếm danh sách sản phẩm
   * @update:
   */
  loadProduct(req): Observable<httpRespose> {
    const param = createRequestOption(req);
    return this.http.get(`${api}/product/search`, { observe: 'response', params: param });
  }

  /**
   * @author: hunghv
   * @since: 11/29/2021 11:2 PM
   * @description:  custom tham số tìm kiếm product
   * @update:
   */
  loadProductCustom(param): Observable<httpRespose> {
    const params = createRequestOption(param);
    return this.http.get(`${api}/product/search${param}`, { observe: 'response' });
  }

  /**
   * @author: huynq
   * @since: 10/31/2021 11:46 AM
   * @description:  nhân viên tạo sản phẩm
   * @update:
   */
  createProduct(product: Iproduct, files: any[]): Observable<httpRespose> {
    const optioProduct = JSON.stringify(product.optionProducts);
    product.optionProducts = null;
    const formData = CommonUtil.convertObjectToFormData(product);
    formData.append('optionProducts', optioProduct);
    files?.forEach((f) => {
      formData.append('files', f.originFileObj);
    });
    return this.http.post<any>(`${api}/product/create`, formData, { observe: 'response' });
  }

  /**
   * @author: huynq
   * @since: 10/31/2021 11:47 AM
   * @description:  ngừng bán sản phẩm
   * @update:
   */
  stopSelling(productId, status): Observable<httpRespose> {
    return this.http.put<any>(`${api}/product/${productId}`, { status }, { observe: 'response' });
  }

  /**
   * @author: huynq
   * @since: 10/31/2021 11:47 AM
   * @description:  Cập nhật sản phẩm
   * @update:
   */
  updateProduct(productId: string, product: Iproduct, files: any[]): Observable<httpRespose> {
    const optioProduct = JSON.stringify(product.optionProducts);
    console.log(optioProduct);
    product.optionProducts = null;
    const formData = CommonUtil.convertObjectToFormData(product);
    formData.append('optionProducts', optioProduct);
    if (files.length > 0) {
      files?.forEach((f) => {
        formData.append('files', f.originFileObj);
      });
    }
    return this.http.post<any>(`${api}/product/updateproduct/${productId}`, formData, { observe: 'response' });
  }

  getProductById(productId: string): Observable<httpRespose> {
    return this.http.get(`${api}/product/${productId}`, { observe: 'response' });
  }

  getProductByIdRicher(productId: string): Observable<httpRespose> {
    return this.http.get(`${api}/product/${productId}/richer`, { observe: 'response' });
  }

  /**
   * @author: huynq
   * @since: 10/31/2021 4:39 PM
   * @description:  Thực hiện xóa sản phẩm
   * @update:
   */
  changeStatusProduct(productId: string, status: number, noloading = false): Observable<httpRespose> {
    return this.http.delete(`${api}/product/${productId}/${status}/changestatus`, {
      observe: 'response',
      headers: { noLoading: noloading ? 'true' : '' },
    });
  }
  getQr(productId: string): string {
    return `${environment.domain}/product/${productId}`;
  }
}
