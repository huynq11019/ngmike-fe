import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  public isLoading = new BehaviorSubject<boolean>(false);

  constructor(private  spiner: NgxSpinnerService) { }

  show() {
    this.isLoading.next(true);
    this.spiner.show();
  }

  hide() {
    this.isLoading.next(false);
    this.spiner.hide();
  }
}
