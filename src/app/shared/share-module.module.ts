import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KeyUptimeDirective } from './directive/key-uptime.directive';
import { HasRolesDirectiveDirective } from './directive/has-roles-directive.directive';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { EditorComponent } from './editor/editor.component';
import { DecimalNumberDirective } from './directive/decimal-number.directive';
import { NumberFormatterDirective } from './directive/number-formatter.directive';



@NgModule({
  declarations: [
    KeyUptimeDirective,
    HasRolesDirectiveDirective,
    EditorComponent,
    DecimalNumberDirective,
    NumberFormatterDirective,
  ],
    exports: [
      HasRolesDirectiveDirective,
      KeyUptimeDirective,
      CKEditorModule,
      EditorComponent,
      DecimalNumberDirective,
      NumberFormatterDirective
    ],
  imports: [
    CommonModule,
    CKEditorModule,
  ]
})
export class ShareModuleModule { }
