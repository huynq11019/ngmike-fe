import { Injectable, isDevMode } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../shared/service/auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {
  constructor(private authService: AuthService,
              private router: Router,
              private tostr: ToastrService) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log(route.data, state.url);
    return this.checkRole(route?.data?.['authorities'], state.url);
  }

  private checkRole(authorities: string[], url: string): boolean {
    if (!authorities || authorities.length === 0) {
      return true;
    }

    if (this.authService.getCurrentUser()) {
      // nếu có quyền truy cập vào url
      const hasAnyAuthority = this.authService.hasAnyAuthority(authorities);
      if (hasAnyAuthority) {
        return true;
      }
      if (isDevMode()) {
        console.error('User has not any of required authorities: ', authorities);
      }
      this.tostr.error('Bạn không có quyền truy cập vào trang này', '403');
      // bắn về lỗi không có quyên truy cập
      this.router.navigate(['/page403']);
      return false;
    }
    this.tostr.error('Bạn cần Đăng nhập để sử dụng chức năng này', '401');
    this.router.navigate(['/login']);
    return false;
  }
}
