import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { catchError, filter, switchMap, take } from 'rxjs/operators';
import { COOKIES, LOCAL_STORAGE, SESSION_STORAGE } from '../../shared/contants/location-session-cookie';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  private isTokenRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  private pathName = window.location.href;

  constructor(
    private router: Router,
    private authenticationService: AuthService,
    private toastService: ToastrService,
    private cookieService: CookieService,
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService,
    private translate: TranslateService
  ) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((err: any) => {
        // debugger

        if (err.status === 401) {
          if (this.pathName.indexOf('/login') != -1) {
            this.authenticationService.doLogout();
            this.toastService.error(this.translate.instant(err.error.message));
            // this.router.navigate(['/login']);
          } else { // refresh token
            const refreshToken = this.cookieService.get(COOKIES.REFRESH_TOKEN);
            const userName = this.authenticationService?.userIdentity?.userName;
            if (!!refreshToken) {
              return this.handle401Error(request, next, refreshToken, userName);
            } else { // nếu không có refresh token thì đăng xuất
              this.router.navigate(['/login']);
              this.authenticationService.doLogout();
            }
          }

        } else if (err.status === 403) {
          this.toastService.error(this.translate.instant(err?.error?.message), '403');
        } else if (err.status === 500) {
          this.toastService.error(this.translate.instant(err?.error?.message), '500');
        } else {
          if (err?.error?.message) {
            this.toastService.error(this.translate.instant(err.error.message));
          } else {
            this.toastService.error(this.translate.instant('Dữ liệu không hợp lệ'));
          }
        }
        return throwError(err);
      })
    );
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler, refreshToken: string, userName: string) {
    if (!this.isTokenRefreshing) {
      this.isTokenRefreshing = true;
      this.refreshTokenSubject.next(null);

      // call refresh token
      return this.authenticationService.refreshToken(userName, refreshToken).pipe(
        switchMap((res: any) => {
          console.log('accesstoken ', res);
          this.isTokenRefreshing = false;
          this.refreshTokenSubject.next(res.accessToken.token);
          return next.handle(this.addTokenToHeader(request, res.accessToken.token));
        }),
        catchError((err, osb) => {
          this.clearTokenRefLogout();
          return throwError(err);
        })
      );
    } else {
      return this.refreshTokenSubject.pipe(
        filter((token) => token != null),
        take(1),
        switchMap((jwt) => {
          return next.handle(this.addTokenToHeader(request, jwt.toString()));
        })
      );
    }
  }

  private addTokenToHeader(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`,
      },
    });
  }

  private clearTokenRefLogout() {
    this.localStorage.clear(SESSION_STORAGE.JWT_TOKEN);
    this.localStorage.clear(SESSION_STORAGE.JWT_TOKEN_AUTH);
    this.sessionStorage.clear(LOCAL_STORAGE.JWT_TOKEN);
    this.sessionStorage.clear(LOCAL_STORAGE.JWT_TOKEN_AUTH);
    this.cookieService.delete(COOKIES.REFRESH_TOKEN);
    this.router.navigate(['/login']);
  }
}
