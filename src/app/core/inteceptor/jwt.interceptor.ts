import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { ActivatedRoute, Router } from '@angular/router';
import { LOCAL_STORAGE } from '../../shared/contants/location-session-cookie';
import { finalize } from 'rxjs/operators';

declare var require: any;
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  pathName = window.location.href;

  constructor(
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      const stepCommentRq = {
        stepId: params.stepId,
        content: params.content,
        id: params.id,
        stepIdNext: params.stepIdNext,
        approvalId: params.approvalId,
        approvalStatus: params.approvalStatus,
      };
      if (params.stepId !== undefined) {
        this.localStorage.store(LOCAL_STORAGE.URL, stepCommentRq);
      }
    });
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    request.headers.delete("noJwt");

    if (request.url.includes('provinces.open-api.vn')) {
      return next.handle(request);
    }

    const queryString = require("query-string");
    // const approvalId = parsedHash.query.id;
    if (window.location.href.includes(LOCAL_STORAGE.CODE)) {
      this.localStorage.store(LOCAL_STORAGE.URL_AUTHORIZATION_CODE, this.pathName);
    }

    const token =
      this.localStorage.retrieve(LOCAL_STORAGE.JWT_TOKEN) || this.sessionStorage.retrieve(LOCAL_STORAGE.JWT_TOKEN);
    if (!!token) {
      // console.log(token);
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
        },
      });
    } else {
      if (!this.pathName.includes("stepId")) {
        // this.router.navigate(["/page/login"], {queryParams: {id: approvalId}});
      }
    }

    return next.handle(request);
  }
}
