import { Component, Input, OnInit } from '@angular/core';
import { SetingService } from '../../shared/service/seting.service';
import { AuthService } from '../../shared/service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title?: string;
  currentUser: any;

  constructor(public settingService: SetingService,
              public authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.currentUser = this.authService.getCurrentUser();
  }

  onCollapse() {

    console.log(this.settingService.hideSidebar);
    this.settingService.hideSidebar = !this.settingService.hideSidebar;
  }

  public logout(): void {
    this.router
    .navigate(['/login/admin'], {relativeTo: this.route});
    this.authService.doLogout();
  }
}
