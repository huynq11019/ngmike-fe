import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LayoutRoutingModule} from './layout-routing.module';
import {HeaderComponent} from './header/header.component';
import {PageLoaderComponent} from './page-loader/page-loader.component';
import {SideBarComponent} from './side-bar/side-bar.component';
import {NzLayoutModule} from 'ng-zorro-antd/layout';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import { PublicHeaderComponent } from './publicComponent/public-header/public-header.component';
import { AvatarModule } from 'ngx-avatar';
import { CartItemComponent } from './publicComponent/cart-item/cart-item.component';
import { ItemSlickComponent } from './publicComponent/item-slick/item-slick.component';
import { CategoryComponentComponent } from './publicComponent/category-component/category-component.component';
import { FullLayoutComponent } from './app-layout/full-layout/full-layout.component';
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import { ProductItemComponent } from './publicComponent/product-item/product-item.component';
import { ShareModuleModule } from '../shared/share-module.module';
import {NzTreeSelectModule} from "ng-zorro-antd/tree-select";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NzCascaderModule } from 'ng-zorro-antd/cascader';
import {NzRateModule} from "ng-zorro-antd/rate";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import { SliderComponent } from './publicComponent/slider/slider.component';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { OrderBoxComponent } from './publicComponent/order-box/order-box.component';


@NgModule({
  declarations: [
    SideBarComponent,
    HeaderComponent,
    PageLoaderComponent,
    PublicHeaderComponent,
    CartItemComponent,
    ItemSlickComponent,
    CategoryComponentComponent,
    FullLayoutComponent,
    ProductItemComponent,
    SliderComponent,
    OrderBoxComponent,

  ],
    exports: [
        SideBarComponent,
        HeaderComponent,
        PublicHeaderComponent,
        CategoryComponentComponent,
        ItemSlickComponent,
        SliderComponent,
        OrderBoxComponent
    ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    NzLayoutModule,
    NzMenuModule,
    RouterModule,
    TranslateModule,
    AvatarModule,
    NzDropDownModule,
    ShareModuleModule,
    NzTreeSelectModule,
    FormsModule,
    NzCascaderModule,
    ReactiveFormsModule,
    NzRateModule,
    NzToolTipModule,
    NzCarouselModule

  ]
})
export class LayoutModule {
}
