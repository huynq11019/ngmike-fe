import { DateCenterService } from 'src/app/shared/service/data-service/date-center.service';
import { Component, Input, OnInit } from '@angular/core';
import { Iproduct } from '../../../shared/models/product';
import { FileUploadService } from '../../../shared/service/file-upload.service';
import { DEFAULT_QUERY, ENTITY_TYPE } from '../../../shared/contants/Constants';
import { ToastrService } from 'ngx-toastr';
import { WishlistService } from '../../../shared/service/wishlist.service';
import { Router } from '@angular/router';
import { MyCartService } from '../../../shared/service/my-cart.service';
import { ICart } from '../../../shared/models/my-cart';
import { AuthService } from '../../../shared/service/auth.service';
import { CompareServiceService } from '../../../shared/service/compare-service.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss'],
})
export class ProductItemComponent implements OnInit {
  @Input() productItem: Iproduct;
  public TYPE_PRODUCT = ENTITY_TYPE.PRODUCT;
  public querySearch = {
    ...DEFAULT_QUERY,
    size: 9,
  };

  constructor(
    public fileService: FileUploadService,
    private toastrService: ToastrService,
    private favorite: WishlistService,
    private router: Router,
    private cartService: MyCartService,
    private authService: AuthService,
    private compareProduct: CompareServiceService,
    private dataCenter: DateCenterService
  ) {}

  ngOnInit(): void {}

  checkIsNewProduct(createDate: string): boolean {
    let dateSent = new Date(createDate);
    let currentDate = new Date();
    let diff = Math.floor(
      (Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) -
        Date.UTC(dateSent.getFullYear(), dateSent.getMonth(), dateSent.getDate())) /
        (1000 * 60 * 60 * 24)
    );
    return diff >= 15 ? false : true;
  }

  public likeProduct(product: Iproduct): void {
    // if (!this.authService.getCurrentUser()){
    //   this.toastrService.error('Bạn cần đăng nhập để thực hiện chức năng này');
    //   return;
    // }
    this.favorite.addToWishlist(product.id).subscribe((response) => {
      // console.log(response);
      this.onloadTotalFav();
      this.toastrService.success('Sản phẩm đã được thêm vào danh sách yêu thích', 'Success');
    });
  }
  onloadTotalFav() {
    this.favorite.loadWishlist(this.querySearch).subscribe((data) => {
      let totalFav = data.body?.totalItem ? data.body?.totalItem : 0;
      this.dataCenter.setTotalFavoriteItem(totalFav);
    });
  }

  addCart(id: string) {
    console.log('id: ' + id);
    const cartItem: ICart = {
      productId: id,
      quantity: 1,
      optionProductId: '',
    };
  }

  goToDetail(id: string) {
    this.router.navigate(['/product/' + id]);
  }

  public addToCompare(product: Iproduct): void {
    this.compareProduct.addProductToCompare(product);
  }

  public checkIsCompare(): boolean {
    return this.compareProduct.isCompare(this.productItem);
  }
}
