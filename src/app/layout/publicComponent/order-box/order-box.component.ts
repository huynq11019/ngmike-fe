import { ORDER_STATUS, ORDER_STATUS_STRING } from './../../../shared/contants/Constants';
import { IOrderItem } from './../../../shared/models/OrderItem';
import { Component, Input, OnInit } from '@angular/core';
import { FileUploadService } from '../../../shared/service/file-upload.service';
import { ENTITY_TYPE } from '../../../shared/contants/Constants';
import { MyOrderService } from 'src/app/shared/service/my-order.service';
import { IOrder } from '../../../shared/models/order-customer';
import { Router } from '@angular/router';
import { ZalopayService } from '../../../shared/service/zalopay.service';
import { IPaymentRequest } from '../../../shared/models/PaymentRequest';

@Component({
  selector: 'app-order-box',
  templateUrl: './order-box.component.html',
  styleUrls: ['./order-box.component.scss'],
})
export class OrderBoxComponent implements OnInit {
  @Input('order') order: IOrder;
  totalMoney: number = 0;
  TYPE_PRODUCT = ENTITY_TYPE.PRODUCT;
  CREATE = ORDER_STATUS.CREATE.label;
  WAITING_FOR_PAYMENT = ORDER_STATUS.WAITING_FOR_PAYMENT.label;
  WAITING_FOR_CONFIRM = ORDER_STATUS.WAITING_FOR_CONFIRM.label;
  CONFIRMED = ORDER_STATUS.CONFIRMED.label;
  SHIPPING = ORDER_STATUS.SHIPPING.label;
  PAID = ORDER_STATUS.PAID.label;
  SUCCESS = ORDER_STATUS.SUCCESS.label;

  constructor(public fileService: FileUploadService,
              private myOrderService: MyOrderService,
              private router: Router,
              private zPayService: ZalopayService,
  ) {
  }

  ngOnInit(): void {
    this.onLoadOrderDetail(this.order.id);
  }

  onLoadOrderDetail(orderId: string) {
    // for(let orders of this.ord)
    this.myOrderService.getOrderDetailByOrderId(orderId).subscribe((data) => {
      this.order.items = data.body.order.orderDetails as IOrderItem[];
      this.order?.items.forEach((element) => {
        this.totalMoney += element.quantity * element.productPrice;
      });
      this.totalMoney += this.order.shippingCost;
    });
  }
  coutnTotalMoney() {
    this.totalMoney = 0;
    if (this.order.items) {
      this.order?.items.forEach((element) => {
        this.totalMoney += element.quantity * element.price;
      });
    }
  }

  limitText(text: string, limit: number): string {
    if (text.length > limit) {
      return text.substr(0, limit) + '...';
    }
    return text;
  }

  public payment() {
    const request: IPaymentRequest = {
      description: "Thanh toán đơn hàng",
      amount: this.totalMoney,
      apptime: new Date().getTime(),
      phone: '0925573154',
      item: JSON.stringify(this.order),
      orderid: this.order.id,

    };
    this.zPayService.peymentZalo(request).subscribe((data: any) => {
      console.log('data: ', data.body);
      if (!!data.body?.orderurl) {
        window.open(data.body?.orderurl, '_blank');
      }
    });
  }

  gotoDetail(id: string) {
    this.router.navigate(['/user/order-detail/' + id]);
  }
}
