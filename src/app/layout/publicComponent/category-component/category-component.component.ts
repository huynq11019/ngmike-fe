import { CATEGORY_TYPE } from './../../../shared/contants/Constants';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-category-component',
  templateUrl: './category-component.component.html',
  styleUrls: ['./category-component.component.scss']
})
export class CategoryComponentComponent implements OnInit {

  constructor() { }
  ngOnInit(): void {
  }

}
