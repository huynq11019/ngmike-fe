import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemSlickComponent } from './item-slick.component';

describe('ItemSlickComponent', () => {
  let component: ItemSlickComponent;
  let fixture: ComponentFixture<ItemSlickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemSlickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSlickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
