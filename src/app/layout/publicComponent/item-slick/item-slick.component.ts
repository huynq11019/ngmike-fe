import { Component, Input, OnInit } from '@angular/core';
import { Iproduct } from '../../../shared/models/product';

@Component({
  selector: 'app-item-slick',
  templateUrl: './item-slick.component.html',
  styleUrls: ['./item-slick.component.scss']
})
export class ItemSlickComponent implements OnInit {
 @Input('listProducts') listProducts?: Iproduct[];
  constructor() { }

  ngOnInit(): void {
    console.table(this.listProducts);
  }

}
