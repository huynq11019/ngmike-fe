import { WishlistService } from './../../../shared/service/wishlist.service';
import { MyCartService } from './../../../shared/service/my-cart.service';
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AuthService } from '../../../shared/service/auth.service';
import { DateCenterService } from '../../../shared/service/data-service/date-center.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LANGUAGE_EN, LANGUAGE_VI } from '../../../shared/contants/appBase';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE } from '../../../shared/contants/location-session-cookie';
import { CategoryService } from '../../../shared/service/category.service';
import { NzCascaderOption } from 'ng-zorro-antd/cascader';
import { FormBuilder } from '@angular/forms';
import { ICategory } from '../../../shared/models/category';
import { DEFAULT_QUERY } from 'src/app/shared/contants/Constants';

@Component({
  selector: 'app-public-header',
  templateUrl: './public-header.component.html',
  styleUrls: ['./public-header.component.scss'],
})
export class PublicHeaderComponent implements OnInit, OnChanges {
  currentUri = window.location.href;
  currentLanguage = '';
  VI = LANGUAGE_VI;
  EN = LANGUAGE_EN;
  visible: boolean = false;
  public categoryNote?: any;
  expandKeys = ['100', '1001'];
  selectCate?: string;
  totalCartItem: number;
  public querySearch = {
    ...DEFAULT_QUERY,
    status: '',
  };
  // options =
  nzOptions: NzCascaderOption[] = [
    {
      value: 'zhejiang',
      label: 'Zhejiang',
      children: [
        {
          value: 'hangzhou',
          label: 'Hangzhou',
          children: [
            {
              value: 'xihu',
              label: 'West Lake',
              isLeaf: true,
            },
          ],
        },
        {
          value: 'ningbo',
          label: 'Ningbo',
          isLeaf: true,
        },
      ],
    },
    {
      value: 'jiangsu',
      label: 'Jiangsu',
      children: [
        {
          value: 'nanjing',
          label: 'Nanjing',
          children: [
            {
              value: 'zhonghuamen',
              label: 'Zhong Hua Men',
              isLeaf: true,
            },
          ],
        },
      ],
    },
  ];

  values: string[] = ['zhejiang', 'hangzhou', 'xihu'];
  keyword: string = '';
  public form: any;
  onChange($event: string): void {
    console.log($event);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.currentUri;
  }

  constructor(
    public authService: AuthService,
    public dataCenter: DateCenterService,
    private router: Router,
    private route: ActivatedRoute,
    private $localStorage: LocalStorageService,
    private categoryService: CategoryService,
    private fb: FormBuilder,
    private myCartService: MyCartService,
    private wishlistService: WishlistService
  ) {}

  isShowNavbar: boolean = window.location.href.includes('/user/');
  opencart: boolean;

  ngOnInit(): void {
    this.authService.getCurrentUser();
    this.initForm();
    this.totalCartItem = this.dataCenter.getTotalCartItem();
    this.totalCartItem = this.dataCenter.getTotalFavoriteItem();
    if(this.authService.getCurrentUser()){
      this.initTotal();
    }
    // console.log(this.authService.userIdentity);
  }
  initForm() {
    this.form = this.fb.group({
      category: [''],
    });
  }
  dologOut() {
    this.authService.doLogout();
    this.router.navigate(['/login'], { relativeTo: this.route });
  }

  showCartList() {
    console.log('thay đổi gì đó');
    this.opencart = !this.opencart;
  }

  onChangeLanguage(language: string) {
    if (this.currentLanguage !== language) {
      this.$localStorage.store(LOCAL_STORAGE.LANGUAGE, language);
      location.reload();
    } else {
      this.visible = false;
    }
  }

  private loadCategory() {
    this.categoryService.loadAllCategory({ page: 0, size: 10 }).subscribe((res) => {
      console.dir(res.body.categoryDTOS);
      // this.nzOptions = this.inputDataToTree(res.body.categoryDTOS);
      console.log(this.nzOptions);
    });
  }
  initTotal(noloading?) {
    this.myCartService.searchCart(noloading).subscribe((data) => {
      this.dataCenter.setTotalCartItem(data.body?.totalCartItem);
    });
    this.wishlistService.loadWishlist(this.querySearch).subscribe((data) => {
      let totalFav = data.body?.totalItem ? data.body?.totalItem : 0;
      this.dataCenter.setTotalFavoriteItem(totalFav);
    });
  }
  onChanges(values: string[]): void {
    console.log(values, this.values);
  }

  public inputDataToTree(list: ICategory[]): any[] {
    const categoryList = [];
    list?.forEach((category) => {
      // @ts-ignore
      const obj = {
        label: category.categoryName,
        value: category.id,
        children: category.child == null ? null : this.inputDataToTree(category.child),
      };
      categoryList.push(obj);
    });

    return categoryList;
  }

  goToSearch() {
    this.router.navigate(['/product'], { queryParams: { keyword: this.keyword, category: this.selectCate } });
  }
}
