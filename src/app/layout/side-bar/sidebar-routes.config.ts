export const ROUTES = [
  { path: '/admin/dashboard', title: 'Dashboard', icon: 'dashboard', children: null },
  { path: '/admin/product', title: 'User Profile', icon: 'person', children: null },
  { path: '/admin/category', title: 'Table List', icon: 'content_paste', children: null },
  { path: '/admin/billing', id: 'component', title: 'Component', icon: 'apps', children: [
      {path: 'components/order', title: 'Price Table', icon: 'PT'},
      {path: 'components/bill', title: 'Panels', icon: 'P'},
      {path: 'components/wizard', title: 'Wizard', icon: 'W'},
    ]},
  { path: '/admin/customer', title: 'cusomter', icon: 'person', children: null },
  { path: '/admin/employee', title: 'employee', icon: 'warning', children: null },
  { path: '/admin/settings', title: 'Settings', icon: 'settings', children: null },
];
