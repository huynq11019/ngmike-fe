import { Component, OnInit } from '@angular/core';
import { CompareServiceService } from '../../../shared/service/compare-service.service';

@Component({
  selector: 'app-public-layout',
  templateUrl: './public-layout.component.html',
  styleUrls: ['./public-layout.component.scss']
})
export class PublicLayoutComponent implements OnInit {
  isCollapsed = false;

  constructor(public compare: CompareServiceService) { }

  ngOnInit(): void {
  }

  clearCompare() {
    this.compare.clearCompareList();
  }

}
