import { Component, OnInit } from '@angular/core';
import { SetingService } from '../../../shared/service/seting.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {
  isCollapsed = false;

  constructor(public setingService: SetingService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

  onCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }

}
