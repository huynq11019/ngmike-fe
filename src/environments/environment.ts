// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  backend: 'http://localhost:8099',
  // backend: 'http://duckyvn.site:8099',
  location: 'https://provinces.open-api.vn',
  locationGHN: 'https://dev-online-gateway.ghn.vn/shiip/public-api/master-data',
  domain: 'http://localhost:4200',
  firebaseConfig: {
    apiKey: 'AIzaSyDvabDlt7zkhOZrSGJso4enQ0qEvS5BOaY',
    authDomain: 'prodeptrai-14282.firebaseapp.com',
    databaseURL: 'https://prodeptrai-14282-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'prodeptrai-14282',
    storageBucket: 'prodeptrai-14282.appspot.com',
    messagingSenderId: '932744185407',
    appId: '1:932744185407:web:6c5af721dc7a5a2f055a8a',
    measurementId: 'G-PEQH4QGPH0',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
